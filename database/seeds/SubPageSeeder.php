<?php

use Illuminate\Database\Seeder;

class SubPageSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        factory(App\SubPage::class,5)->create();
    }
}
