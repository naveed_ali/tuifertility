<?php

use Illuminate\Database\Seeder;

class ClinicFormPageTextSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        factory(App\ClinicFormPageText::class,1)->create();
    }
}
