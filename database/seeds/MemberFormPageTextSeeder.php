<?php

use Illuminate\Database\Seeder;

class MemberFormPageTextSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        factory(App\MemberFormPageText::class,1)->create();
    }
}
