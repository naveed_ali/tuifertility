<?php

use Illuminate\Database\Seeder;

class DatabaseSeeder extends Seeder
{
    /**
     * Seed the application's database.
     *
     * @return void
     */
    public function run()
    {
        // $this->call(UsersTableSeeder::class);
        // $this->call(MemberFormPageTextSeeder::class);
        $this->call(ClinicFormPageTextSeeder::class);
        // $this->call(MenuSeeder::class);
        // $this->call(PageSeeder::class);
        // $this->call(SubPageSeeder::class);
    }
}
