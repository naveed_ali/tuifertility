<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateClinicFormPageTextsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('clinic_form_page_texts', function (Blueprint $table) {
            $table->id();
            $table->string('title')->nullable();
            $table->string('note')->nullable();
            $table->string('clinic_name')->nullable();
            $table->string('name')->nullable();
            $table->string('email')->nullable();
            $table->string('address')->nullable();
            $table->string('website')->nullable();
            $table->string('country')->nullable();
            $table->string('phone')->nullable();
            $table->string('provide_services_for')->nullable();
            $table->text('provide_services_for_data')->nullable();
            $table->string('treatments')->nullable();
            $table->text('treatments_data')->nullable();
            $table->string('willing_to_assist')->nullable();
            $table->text('willing_to_assist_data')->nullable();
            $table->string('willing_to_assist_countries')->nullable();
            // $table->text('willing_to_assist_countries_data')->nullable();
            $table->string('providing_service')->nullable();
            $table->text('providing_service_data')->nullable();
            $table->string('describe_clinic')->nullable();
            $table->string('video_url')->nullable();
            $table->string('video_url_hint')->nullable();
            $table->string('pictures')->nullable();
            $table->string('add_file')->nullable();
            $table->string('complete_signup')->nullable();
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('clinic_form_page_texts');
    }
}
