<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateFertilityClinicMembersTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('fertility_clinic_members', function (Blueprint $table) {
            $table->id();
            $table->string('clinic_name');
            $table->string('name');
            $table->string('email');
            $table->string('address')->nullable();
            $table->string('website')->nullable();
            $table->string('country');
            $table->string('phone')->nullable();
            $table->string('provide_services_for');
            $table->string('treatments');
            $table->string('willing_to_assist');
            $table->string('willing_to_assist_countries')->nullable();
            $table->string('providing_service');
            $table->string('describe_clinic');
            $table->string('video_url')->nullable();
            $table->string('pictures')->nullable();
            $table->boolean('status')->default(0);
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('fertility_clinic_members');
    }
}
