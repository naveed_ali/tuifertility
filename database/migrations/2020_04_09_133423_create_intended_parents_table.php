<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateIntendedParentsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('intended_parents', function (Blueprint $table) {
            $table->id();
            $table->string('name');
            $table->string('email');
            $table->date('birthday')->nullable();
            $table->string('country')->nullable();
            $table->text('address')->nullable();
            $table->string('phone')->nullable();
            $table->string('nationality')->nullable();
            $table->string('race')->nullable();
            $table->string('religion')->nullable();
            $table->string('education')->nullable();
            $table->string('spoken_language')->nullable();
            $table->string('willing_to_travel')->nullable();
            $table->string('smoker')->nullable();
            $table->string('your_couple_type')->nullable();
            $table->string('marital_status')->nullable();
            $table->string('number_of_children')->nullable();
            $table->string('number_of_surrogate_children')->nullable();
            $table->string('had_surrogate_before')->nullable();
            $table->string('budget')->nullable();
            $table->string('looking_to_connect_with')->nullable();
            $table->string('prefer_surrogate_age')->nullable();
            $table->string('prefer_surrogate_country')->nullable();
            $table->string('prefer_surrogate_nationality')->nullable();
            $table->string('describe_yourself')->nullable();
            $table->string('reason_for_wanting_surrogate')->nullable();
            $table->string('communication_letter')->nullable();
            $table->string('picture')->nullable();
            $table->string('category');
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('intended_parents');
    }
}
