<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateBecomeMembersTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('become_members', function (Blueprint $table) {
            $table->id();
            $table->string('name');
            $table->string('email');
            $table->date('birthday');
            $table->string('country');
            $table->text('address')->nullable();
            $table->string('phone')->nullable();
            $table->string('race');
            $table->string('religion')->nullable();
            $table->string('education')->nullable();
            $table->string('health_status')->nullable();
            $table->string('smoker')->nullable();
            $table->string('eye_color');
            $table->string('hair_color');
            $table->string('height');
            $table->string('weight');
            $table->string('blood_type')->nullable();
            $table->string('have_passport')->nullable();
            $table->string('willing_to_travel')->nullable();
            $table->string('number_of_children')->nullable();
            $table->string('number_of_surrogate_children')->nullable();
            $table->string('surrogate_type')->nullable();
            $table->string('given_service_before')->nullable();
            $table->string('marital_status')->nullable();
            $table->string('spoken_language')->nullable();
            $table->string('willing_to_assist');
            $table->string('describe_yourself')->nullable();
            $table->string('why_become_member');
            $table->string('communication_letter');
            $table->string('video_url');
            $table->string('picture');
            $table->boolean('has_photo');
            $table->string('category');
            $table->boolean('status')->default(0);
            $table->timestamps();
        });
        return view('front-end.thank-you');
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('become_members');
    }
}
