<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateMemberFormPageTextsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('member_form_page_texts', function (Blueprint $table) {
            $table->id();
            $table->string('title')->nullable();
            $table->string('note')->nullable();
            $table->string('name')->nullable();
            $table->string('email')->nullable();
            $table->string('birthday')->nullable();
            $table->string('country')->nullable();
            $table->text('address')->nullable();
            $table->string('phone')->nullable();
            $table->string('race')->nullable();
            $table->text('race_data')->nullable();
            $table->string('religion')->nullable();
            $table->text('religion_data')->nullable();
            $table->string('education')->nullable();
            $table->text('education_data')->nullable();
            $table->string('health_status')->nullable();
            $table->text('health_status_data')->nullable();
            $table->string('smoker')->nullable();
            $table->text('smoker_data')->nullable();
            $table->string('eye_color')->nullable();
            $table->text('eye_color_data')->nullable();
            $table->string('hair_color')->nullable();
            $table->text('hair_color_data')->nullable();
            $table->string('height')->nullable();
            $table->text('height_data')->nullable();
            $table->string('weight')->nullable();
            $table->text('weight_data')->nullable();
            $table->string('blood_type')->nullable();
            $table->text('blood_type_data')->nullable();
            $table->string('have_passport')->nullable();
            $table->text('have_passport_data')->nullable();
            $table->string('willing_to_travel')->nullable();
            $table->text('willing_to_travel_data')->nullable();
            $table->string('number_of_children')->nullable();
            $table->text('number_of_children_data')->nullable();
            $table->string('number_of_surrogate_children')->nullable();
            $table->text('number_of_surrogate_children_data')->nullable();
            $table->string('surrogate_type')->nullable();
            $table->text('surrogate_type_data')->nullable();
            $table->string('given_service_before')->nullable();
            $table->text('given_service_before_data')->nullable();
            $table->string('marital_status')->nullable();
            $table->text('marital_status_data')->nullable();
            $table->string('spoken_language')->nullable();
            $table->text('spoken_language_data')->nullable();
            $table->string('willing_to_assist')->nullable();
            $table->text('willing_to_assist_data')->nullable();
            $table->string('describe_yourself')->nullable();
            $table->string('why_become_member')->nullable();
            $table->string('communication_letter')->nullable();
            $table->string('video_url')->nullable();
            $table->string('video_url_hint')->nullable();
            $table->string('add_photo')->nullable();
            $table->string('add_file')->nullable();
            $table->string('first_photo')->nullable();
            $table->string('category')->nullable();
            $table->string('complete_signup')->nullable();
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('member_form_page_texts');
    }
}
