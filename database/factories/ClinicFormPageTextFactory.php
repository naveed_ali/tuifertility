<?php

/** @var \Illuminate\Database\Eloquent\Factory $factory */

use App\ClinicFormPageText;
use Faker\Generator as Faker;

$factory->define(ClinicFormPageText::class, function (Faker $faker) {
    return [
        'title' => 'Fertility Clinic Registration',
        'note' => "Your information stays confidential ( photos, name, contacts won't be displayed in search). For this program, you can gain USD 80 - 150",
        'clinic_name' => 'Name of your Clinic*',
        'name' => 'Full Name*',
        'email' => 'Email*',
        'address' => 'Address',
        'website' => 'Website/Blog',
        'country' => 'Country',
        'phone' => 'Phone/WhatsApp/Viber/Telegram',
        'provide_services_for' => 'Select the groups that you provide services for:*',
        'provide_services_for_data' => 'Surrogate Mothers,Sperm Donors,Egg Donors,Intended Parents',
        'treatments' => 'Treatments Offered:*',
        'treatments_data' => 'AEC (autologous endometrial coculture),Egg Donation,Egg Freezing,Embryo Adoption,Embryo Donation,Embryo Freezing,Frozen Embryo Transfer Cycle,Gender Selection,ICSI (intracytoplasmic sperm injection),IMSI,IUI (intrauterine insemination),IVF (in vitro fertilization),IVM (in vitro maturation),Microdissection TESE/TESA,MicroSort (gender selection),Mini IVF,Natural Cycle IVF,PGD (pre-implantation genetic diagnosis),Pregnancy Care,Sperm Donation,Sperm Freezing,Surrogacy,Tandem IVF Cycle,TESA,Vasectomy Reversal',

        'willing_to_assist' => 'Select the couple types that you assist:*',
        'willing_to_assist_data' => 'Gay Couple,Heterosexual Couple,Lesbian Couple,Single Man,Single Woman',
        'providing_service' => 'Select the services that you provide:*',
        'providing_service_data' => 'Crisis Support,Egg Donor Coordinators,Egg Donor Matching,Fertility Doctors,Financial Assistance,Journey Coordinators,Legal Support/Lawyers,Medical Insurance Assistance,Midwives/Birthing Team,Nursing Staff,Psychologists Evaluations,Psychotherapists Evaluations,Screening & Matching Team,Social Workers,Sperm Donor Matching,Surrogacy Applications and Contracts,Surrogate Care Assistance,Surrogate Mother Matching',

        'willing_to_assist_countries' => 'Willing to assist intended parents/surrogates/donors from the following countries:',
        'describe_clinic' => 'Please describe your practise/clinic in detail:*',
        'video_url' => 'Add Link of the video',
        'video_url_hint' => 'Video Link should be from Youtube or Vimeo',
        'pictures' => 'Add Photos',
        'add_file' => 'Add Files',
        'complete_signup' => 'Complete Sign Up!', 

    ];
});
