<?php

/** @var \Illuminate\Database\Eloquent\Factory $factory */

use App\Page;
use Faker\Generator as Faker;
use App\Menu;
$factory->define(Page::class, function (Faker $faker) {
	$menu_ids = Menu::pluck('id')->toArray();
    return [
        'menu_id' => $faker->randomElement($menu_ids),
        'name' => $faker->name,
        'slug' => $faker->slug,
        'title' => $faker->tld,
        'description' => $faker->text(),
        'status' => $faker->boolean
    ];
});
