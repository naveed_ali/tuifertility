<?php

/** @var \Illuminate\Database\Eloquent\Factory $factory */

use App\Scout;
use Faker\Generator as Faker;

$factory->define(Scout::class, function (Faker $faker) {
    return [
        'name' => $faker->name,
        'phone' => $faker->phone,
        'messenger'=> $faker->link,
        'viber'=> $faker->link,
        'telegram'=> $faker->link,
        'whatsapp'=> $faker->link,

    ];
});
