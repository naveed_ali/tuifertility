<?php

/** @var \Illuminate\Database\Eloquent\Factory $factory */

use App\SubPage;
use Faker\Generator as Faker;
use App\Page;
$factory->define(SubPage::class, function (Faker $faker) {
        $page_ids = Page::pluck('id')->toArray();
    return [
        'page_id' => $faker->randomElement($page_ids),
        'name' => $faker->name,
        'slug' => $faker->slug,
        'title' => $faker->tld,
        'description' => $faker->text(),
        'status' => $faker->boolean
    ];
});
