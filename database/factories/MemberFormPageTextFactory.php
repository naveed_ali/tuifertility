<?php

/** @var \Illuminate\Database\Eloquent\Factory $factory */

use App\MemberFormPageText;
use Faker\Generator as Faker;

$factory->define(MemberFormPageText::class, function (Faker $faker) {
    return [
        'title' => 'Surrogate Mother Registration',
        'note' => "Your information stays confidential ( photos, name, contacts won't be displayed in search). For this program, you can gain USD 80 - 150",
        'email' => 'Your Email*',
        'name' => 'Full Name',
        'birthday' => 'Birthday (mo/day/yr)*',
        'country' => 'Country',
        'address' => 'Address',
        'phone' => 'Phone/WhatsApp/Viber/Telegram',
        'race' => 'Race*',
        'race_data' => 'African,American-Indian,Asian (Chinese),Asian (Japanese),Asian (other),Caucasiann,Hispanic,Middle Eastern,Pacific Islander,Other',
        'religion' => 'Religion',
        'religion_data' => "African Traditional &amp; Diasporic,Agnostic,Atheist,Baha'i,Buddhism,Cao Dai,Chinese traditional religion,Christianity,Hinduism,Islam,Jainism,Juche,Judaism,Neo-Paganism,Nonreligious,Other,primal-indigenous,Rastafarianism,Secular,Shinto,Sikhism,Spiritism,Tenrikyo,Unitarian-Universalism,Zoroastrianism,",
        'education' => 'Education',
        'education_data' => 'Never finished High School, Completed High School, Completed College or University, Completed Master Degree',
        'health_status' => 'Health Status',
        'health_status_data' => 'Excellent,Fair,Good,Poor',
        'smoker' => 'Smoker',
        'smoker_data' => 'Yes,No',
        'eye_color' => 'Eye Color*',
        'eye_color_data' => 'Amber,Blue,Brown,Green,Grey,Hazel',
        'hair_color' => 'Hair Color*',
        'hair_color_data' => 'Auburn,Black,Blonde,Brown,Ginger/Red,Grey,Light Brown,White',
        'height' => 'Height*',
        'height_data' => "4' 10\" or 147 CM,4' 11\" or 150 CM,5' or 152 CM,5' 1\" or 155 CM,5' 2\" or 157 CM,5' 3\" or 160 CM,5' 4\" or 163 CM,5' 5\" or 165 CM,5' 6\" or 168 CM,5' 7\" or 170 CM,5' 8\" or 173 CM,5' 9\" or 175 CM,5' 10\" or 178 CM,5' 11\" or 180 CM,6' or 183 CM,6' 1\" or 185 CM,6' 2\" or 188 CM,6' 3\" or 191 CM,6' 4\" or 193 CM",
        'weight' => 'Weight*',
        'weight_data' => '80 Lbs or 36 Kgs,85 Lbs or 39 Kgs,90 Lbs or 41 Kgs,95 Lbs or 43 Kgs,100 Lbs or 45 Kgs,105 Lbs or 48 Kgs,110 Lbs or 50 Kgs,115 Lbs or 52 Kgs,120 Lbs or 55 Kgs,125 Lbs or 57 Kgs,130 Lbs or 59 Kgs,135 Lbs or 61 Kgs,140 Lbs or 64 Kgs,145 Lbs or 66 Kgs,150 Lbs or 68 Kgs,155 Lbs or 70 Kgs,160 Lbs or 73 Kgs,165 Lbs or 75 Kgs,170 Lbs or 77 Kgs,175 Lbs or 80 Kgs,180 Lbs or 82 Kgs,185 Lbs or 84 Kgs,190 Lbs or 86 Kgs,195 Lbs or 89 Kgs ,200 Lbs or 91 Kgs,205 Lbs or 93 Kgs,210 Lbs or 95 Kgs,215 Lbs or 98 Kgs,220 Lbs or 100 Kgs,225 Lbs or more',
        'blood_type' => 'Blood Type',
        'blood_type_data' => 'A+,A-,B+,B-,AB+,AB-,O+,O-',
        'have_passport' => 'Has Passport',
        'have_passport_data' => 'Yes, No',
        'willing_to_travel' => 'Willing To Travel',
        'willing_to_travel_data' => 'Yes, No',
        'number_of_children' => 'Number of Children(non-donor)',
        'number_of_children_data' => '0,1,2,3,4 or more',
        'number_of_surrogate_children' => 'Number of Surrogate Children',
        'number_of_surrogate_children_data' => '0,1,2,3,4 or more',
        'surrogate_type' => 'Surrogate Mother Types That You Agree To:*',
        'surrogate_type_data' => 'Gestational,Traditional',
        'given_service_before' => 'Have you been a surrogate mother before?',
        'given_service_before_data' => 'Yes, No',
        'marital_status' => 'Marital Status',
        'marital_status_data' => 'Divorced,Married,Separated,Sinlge(With no partner),Single(With partner)',
        'spoken_language' => 'Spoken Languages',
        'spoken_language_data' => 'Abkhaz,Afrikaans,Albanian
 ,Amharic
,Arabic
,Armenian
,Assamese
,Aymara,Bajan
,Basque
,Belarusian
 ,Bengali
  ,Bhojpuri
 ,Bislama
 ,Bosnian
   ,Bulgarian
 ,Burmese
 ,Catalan
 ,Chinese
  ,Croatian,Czech
,Danish
 ,Dhivehi,Dutch
  ,Dzongkha
 ,English
  ,Estonian
,Fijian
  ,Filipino
 ,Finnish
,French
  ,Georgian
,German,Greek
 ,Guarani
  ,Gujarati
,Haitian Creole
,Hebrew,Hindi
   ,Hiri Motu
   ,Hungarian
   ,Icelandic
    ,Indonesian
   ,Inuktitut
 ,Italian
  ,Japanese
 ,Kannada
  ,Kashmiri
,Kazakh,Khmer
,Korean
 ,Kurdi,Lao
 ,Latvian
    ,Lithuanian
       ,Luxembourgish
    ,Macedonian
  ,Malagasy,Malay
   ,Malayalam
 ,Maltese
     ,Manx Gaelic,Maori
 ,Marathi,Mayan
   ,Mongolian
,Nepali
   ,Norwegian
 ,Occitan,Oriya
  ,Ossetian
    ,Papiamento
,Pashto
 ,Persian
,Polish
    ,Portuguese
 ,Punjabi
 ,Quechua
,Rhaeto-Romansh
  ,Romanian
 ,Russian
  ,Sanskrit
 ,Serbian
,Sindhi
 ,Sinhala
,Slovak
 ,Slovene
,Somali,Sotho
 ,Spanish
 ,Swahili
 ,Swedish,Tajik,Tamil
,Telug,Thai
,Tswana
 ,Turkish
 ,Turkmen
   ,Ukrainia,Urdu,Uzbek,Venda
    ,Vietnamese,Welsh
 ,Yiddis,Zulu',
        'willing_to_assist' => 'Willing To Assist Couple Types:*',
        'willing_to_assist_data' => 'Gay Couple,Heterosexual Couple,Lesbian Couple,Single Man,Single Woman',
        'describe_yourself' => 'How would you best describe yourself?',
        'why_become_member' => 'Why do you want to be a surrogate mother ?*',
        'communication_letter' => 'Use this space to communicate a letter to potential intended parents.',
        'video_url' => 'Add Link of the video',
        'video_url_hint' => 'Video Link should be from Youtube or Vimeo',
        'add_photo' => 'Add Photos',
        'add_file' => 'Add Files',
        'first_photo' => '(First selected photo will be profile photo)',
        'category' => 'surrogate_mothers',
        'complete_signup' => 'Complete Sign Up!'

    ];
});
