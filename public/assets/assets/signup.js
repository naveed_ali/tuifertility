$(document).ready(function(){
	/*
	$('.jq_date_formatted').datepicker({dateFormat: 'mm-dd-yy', yearRange: "-50:-18", changeMonth: true, changeYear: true,
		onChangeMonthYear: function(year, month, inst) {
			var curDate = $(this).datepicker("getDate");
			if (curDate == null)
			return;
			if (curDate.getYear() != year || curDate.getMonth() != month - 1) {
				curDate.setYear(year);
				curDate.setMonth(month - 1);
				$(this).datepicker("setDate", curDate);
			}
		}
	});
	*/
	
	/*
	$.each($('.jq_multiselect'), function(){					
		$(this).multiselect(
			{
				buttonWidth: 200, maxHeight: 300, buttonClass: "btn btn-success", selectedClass: null, noneSelectedText: 'Select Please', selectedList: 5, header: ($(this).attr('enable_select_all') == "true") ? true : '',
				click: function(e){
					//if(( $(this).multiselect("widget").find("input:checked").length > 10 ) && ($(this).attr('enable_select_all') == "false") ){					
					if(( $(this).multiselect("widget").find("input:checked").length > 10 )){
						alert("You can only select 10!");
						return false;
					} else {
					}
				}
			}
		).multiselectfilter({label: '', autoReset: true});

	});
	*/

	$("#date_of_birth").bdatepicker({
		format: 'yyyy-mm-dd',
		endDate: "-18y",
		autoclose: true,
		startView: 2
	});

	$.each($('.jq_multiselect'), function(){					
		$(this).multiselect(
			{
				// onChange: function(option, checked) {

				//console.log(option);

				// var selectedOptions = $(this).siblings('.multiselect-container').find("input:checked");
				//var selectedOptions = $(this).find("option:selected");
				//alert(selectedOptions);
				//alert(selectedOptions.length);
			// },
			enableFiltering: true,
			enableCaseInsensitiveFiltering: true,
			buttonClass: "btn btn-success",
			selectedClass: null,
			buttonWidth: 800,
			maxHeight: 300
			}
		)

	});

/*
	$(".jq_multiselect").multiselect({
		onChange: function(option, checked) {
			
			console.log(option);
			var selectedOptions = option.prevObject.find("option:selected");
			//var selectedOptions = $(this).find("option:selected");
			alert(selectedOptions);
			alert(selectedOptions.length);
			
		},
		enableFiltering: true,
		enableCaseInsensitiveFiltering: true,
		buttonClass: "btn btn-success",
		selectedClass: null,
		buttonWidth: 200,
		maxHeight: 300
	});
	*/
	/*
	 $('#example37').multiselect({
		onChange: function(option, checked) {
		// Get selected options.
		var selectedOptions = $('#example37 option:selected');
		 
		if (selectedOptions.length >= 4) {
		// Disable all other checkboxes.
		var nonSelectedOptions = $('#example37 option').filter(function() {
		return !$(this).is(':selected');
		});
		 
		var dropdown = $('#example37').siblings('.multiselect-container');
		nonSelectedOptions.each(function() {
		var input = $('input[value="' + $(this).val() + '"]');
		input.prop('disabled', true);
		input.parent('li').addClass('disabled');
		});
		}
		else {
		// Enable all checkboxes.
		var dropdown = $('#example37').siblings('.multiselect-container');
		$('#example37 option').each(function() {
		var input = $('input[value="' + $(this).val() + '"]');
		input.prop('disabled', false);
		input.parent('li').addClass('disabled');
		});
		}
		}
		});*/

})

function getRegionListDrp(country_id){
	$("#region_id").html('');
	$("#city_id").html('');
	if(country_id==210){
		$('.city_id_holder').show();
		$('.region_id_holder label').text('State');
	}else{
		$('.city_id_holder').hide();
		$('.region_id_holder label').text('City');
	}
	$.ajax({
		type: "POST",
		url: "/signup/get_region_list_drp",
		data: {'country_id':country_id},
		success: function(data){
			$("#region_id").html(data);
			//$('#region_id')[0].jcf.buildDropdown();
			//$('#region_id')[0].jcf.refreshState();
	}});
}

function getCityListDrp(region_id){
	$("#city_id").html('');
	$.ajax({
		type: "POST",
		url: "/signup/get_city_list_drp",
		data: {'region_id':region_id},
		success: function(data){
			$("#city_id").html(data);
			//$('#city_id')[0].jcf.buildDropdown();
			//$('#city_id')[0].jcf.refreshState();
	}});
}

function update_information(tr){
	if(tr!=-10){
		validate_result=$("#myInformationForm").validate().form();
		if (!validate_result){
			alert('Must complete the required fields');
			return false;
		}
	}
	var update_data = $("#myInformationForm").serializeArray();

	$.each($(".jq_multiselect"), function() {
		update_data.push({name: this.name+'_arr', value: $(this).val()});
	});

	$('#update_profile').button('loading');

	jQuery.ajax({
		type: "POST",
		url: "/signup/update_information",
		data: update_data,
		success: function(data){
			window.onbeforeunload = null;
			if (tr==20){
				window.location="/member_main/edit_profile";
			}else if (tr==30){
				window.location="/member_main/edit_classified";
			}else if (tr==40){
				window.location="/member_main/edit_avatar";
			}else if (tr==50){
				window.location="/member_main/gallery";
			}else if(tr==-10){
				$('#update_profile').button('reset');
			}else{
				location.reload();
			}
		}
	});

}
