<?php

use Illuminate\Support\Facades\Route;

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

//=====================Front end===============================
Route::get('/','HomeController@index');
Route::get('search','HomeController@searchResult')->name('search.result');
Route::get('search/{category}','HomeController@searchFertilityClinicResult')->name('search.fertility-clicnic.result');
Route::get('category/search/{category?}','HomeController@showSearchForm')->name('search.form');

Route::get('member/{becomeMember}','HomeController@singleMemberDetails')->name('single.member');
Route::get('fertility-clinics/{fertilityClinicMember}','HomeController@singleClinicMemberDetails')->name('single.clinic.member');

//======================Admin Dashboard==================================
Route::group(['prefix'=>'admin-en'],function(){

	//===================Menus Management===========================
	Route::resource('menus','MenuController');
	Route::get('menus/delete/{menu}','MenuController@destroy')->name('menus.destroy');

	//=======================Pages Management==========================
	Route::resource('pages','PageController');
	Route::get('page/create/new','PageController@create')->name('pages.create');
	Route::get('pages/delete/{page}','PageController@destroy')->name('pages.destroy');

	//=======================Sub Pages Management==========================
	Route::resource('subpages','SubPageController');
	Route::get('subpage/create/new','SubPageController@create')->name('subpages.create');
	Route::get('subpages/delete/{subpage}','SubPageController@destroy')->name('subpages.destroy');



	

	//========================intended parents================================
	Route::resource('intended_parents','IntendedParentController');
	Route::get('intended_parents/delete/{intendedParent}','IntendedParentController@destroy')->name('intended_parents.destroy');
	Route::get('all-email','IntendedParentController@allEmails')->name('all.emails');
	//emails export
	Route::get('export/emails','IntendedParentController@exportEmails')->name('export.emails');
	//==========================Reviews=========================================
		
	//======================Scouts Management===================================
	Route::resource('scouts','ScoutController');
	Route::get('scouts/destroy/{scout}','ScoutController@destroy')->name('scouts.destroy');

	//============================Website Settings======================================
	Route::resource('settings','SettingController',['only'=>['index','update']]);
	

});

//==============================================================================
		// m.tuifertility.com (members management admin dashboard)
//==============================================================================

Route::group(['prefix'=>'m.tuifertility/admin-en'],function(){
	//===============admin side become a member========================

	Route::resource('become_members','BecomeMemberController',['except'=>['index']]);
	Route::get('become_members/category/{category}','BecomeMemberController@index')->name('become_members.index');
	Route::get('become_members/show/{becomeMember}','BecomeMemberController@show')->name('become_members.show');
	Route::get('become_members/delete/{becomeMember}','BecomeMemberController@destroy')->name('become_members.destroy');
	Route::get('become-a-member/verify/{becomeMember}','BecomeMemberController@verifyMember')->name('become_members.verify');

//===================Become fertility member routes====================
	Route::resource('fertility_clinic_members','FertilityClinicMemberController');
	Route::get('become-clinic-member/verify/{fertilityClinicMember}','FertilityClinicMemberController@verifyMember')->name('become_fertility_members.verify');
	Route::get('fertility_clinic_members/delete/{fertilityClinicMember}','FertilityClinicMemberController@destroy')->name('fertility_clinic_members.destroy');

	//===================Menus Management===========================
	Route::resource('menus','MemberSiteMenuController',['names'=>[
		'index' => 'member.menus.index',
		'store' => 'member.menus.store',
		'update' => 'member.menus.update',
		'edit' => 'member.menus.edit',
	]]);
	Route::get('menus/delete/{menu}','MenuController@destroy')->name('member.menus.destroy');

	//=======================Pages Management==========================
	Route::resource('pages','MemberSitePageController',['names'=>[
		'index' => 'member.pages.index',
		'store' => 'member.pages.store',
		'update' => 'member.pages.update',
		'edit' => 'member.pages.edit',
	],'except'=>['create','destroy']]);
	Route::get('page/create/new','MemberSitePageController@create')->name('member.pages.create');
	Route::get('pages/delete/{page}','MemberSitePageController@destroy')->name('member.pages.destroy');

	//=======================Sub Pages Management==========================
	Route::resource('subpages','MemberSiteSubPageController',['names'=>[
		'index' => 'member.subpages.index',
		'store' => 'member.subpages.store',
		'update' => 'member.subpages.update',
		'edit' => 'member.subpages.edit',
	],'except'=>['create','destroy']]);
	Route::get('subpage/create/new','MemberSiteSubPageController@create')->name('member.subpages.create');
	Route::get('subpages/delete/{subpage}','MemberSiteSubPageController@destroy')->name('member.subpages.destroy');

	//=======================Pages Text========================================
	Route::get('all-pages','MemberFormPageTextController@allPages')->name('all.pages');
	// become member form page text management
	Route::get('form-text-page/{category}','MemberFormPageTextController@edit')->name('member.form.edit');
	Route::put('form-text-page/{memberFormPageText}','MemberFormPageTextController@update')->name('member.form.update');
	Route::put('clinic-form-text-page/{clinicFormPageText}','MemberFormPageTextController@clinicUpdate')->name('clinic.member.form.update');

	//Apply Now buttons text
	Route::get('apply-now-buttons','MemberFormPageTextController@editApplyNowButtons')->name('apply.now.buttons');
	Route::put('apply-now-buttons/{applyNowButton}','MemberFormPageTextController@updateApplyNowButtons')->name('applynow.button.update');

});

//================Become a member Routes===========================
Route::group(['prefix'=>'m.tuifertility'],function(){
	Route::get('become-a-member/signup','BecomeMemberController@SelectForm');
	Route::get('become-member/{category}','HomeController@becomeMember')->name('become.member');
	Route::post('become-member/applied','BecomeMemberController@store')->name('member.store');
});
	//=========================End become member routes====================

//======================Become Intended parent route======================
	Route::get('intended-parent/sign-up/{category}','HomeController@intendedParentForm')->name('intendent.parent.form');



Route::get('admin-en', function () {
    return view('admin-dashboard.layouts.master');
});
Route::get('m.tuifertility/admin-en', function () {
    return view('member-admin-dashboard.layouts.master');
});


Route::get('thank-you','HomeController@thankyou')->name('thankyou');
Route::get('{menu}/{page}','HomeController@showPage');
Route::get('{menu}/{page}/{subpage}','HomeController@showSubPage');

Route::get('/clear-cache', function() {
    Artisan::call('cache:clear');
    return "Cache is cleared";
});

