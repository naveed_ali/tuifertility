<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Review extends Model
{
    protected $guarded = [];
    public function becomeMember(){
    	return $this->belongsTo('App\BecomeMember');
    }
}
