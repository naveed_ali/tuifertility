<?php

namespace App\Providers;

use Illuminate\Support\ServiceProvider;
use Illuminate\Support\Facades\Validator;
use App\Menu;
use App\Scout;
use App\Setting;
class AppServiceProvider extends ServiceProvider
{
    /**
     * Register any application services.
     *
     * @return void
     */
    public function register()
    {
        //
    }

    /**
     * Bootstrap any application services.
     *
     * @return void
     */
    public function boot()
    {
        view()->composer('*',function($view){
            $scouts = Scout::all();
            $admin = Setting::first();
            $menus = Menu::with('pages')->where('category',0)->get();
            $member_menus = Menu::with('pages')->where('category',1)->get();
            return $view->with(['menus'=>$menus,'scouts'=>$scouts,'member_menus'=> $member_menus,'admin' => $admin]);
        });
        Validator::extend('recaptcha', 'App\\Validators\\ReCaptcha@validate');

    }
}
