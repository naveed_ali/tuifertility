<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class SubPage extends Model
{
	protected $guarded=[];
	public function getRouteKeyName()
    {
        return 'slug';
    }
    public function page(){
    	return $this->belongsTo('App\Page');
    }
}
