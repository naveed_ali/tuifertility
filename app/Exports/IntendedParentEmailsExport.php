<?php

namespace App\Exports;

use App\IntendedParent;
use Maatwebsite\Excel\Concerns\FromCollection;

class IntendedParentEmailsExport implements FromCollection
{
    /**
    * @return \Illuminate\Support\Collection
    */
    public function collection()
    {
        return IntendedParent::select('email')->get();
    }
}
