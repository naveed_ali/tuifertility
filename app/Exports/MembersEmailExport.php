<?php

namespace App\Exports;

use App\BecomeMember;
use Maatwebsite\Excel\Concerns\FromCollection;

class MembersEmailExport implements FromCollection
{
    /**
    * @return \Illuminate\Support\Collection
    */
    public function collection()
    {
        return BecomeMember::select('email')->get();
    }
}
