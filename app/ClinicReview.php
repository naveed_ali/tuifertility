<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class ClinicReview extends Model
{
    protected $guarded = [];
    public function fertilityClinicMember(){
    	return $this->belongsTo('App\FertilityClinicMember');
    }
}
