<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class FertilityClinicMember extends Model
{
    protected $guarded = [];
    public function clinic_review(){
    	return $this->hasOne('App\ClinicReview');
    }
}
