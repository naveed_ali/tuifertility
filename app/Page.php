<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Page extends Model
{
    protected $guarded=[];
    public function getRouteKeyName()
    {
        return 'slug';
    }
    public function menu(){
    	return $this->belongsTo('App\Menu');
    }
    public function subPages(){
    	return $this->hasMany('App\SubPage');
    }
}
