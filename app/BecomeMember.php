<?php

namespace App;

use Illuminate\Database\Eloquent\Model;
use Carbon\Carbon;
class BecomeMember extends Model
{
    protected $guarded = [];
    public function review(){
    	return $this->hasOne('App\Review');
    }
    public function getAgeAttribute()
{
    return Carbon::parse($this->attributes['birthday'])->age;
}
}
