<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class IntendedParent extends Model
{
    protected $guarded = [];
}
