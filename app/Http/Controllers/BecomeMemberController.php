<?php

namespace App\Http\Controllers;

use App\BecomeMember;
use Illuminate\Http\Request;
use App\Http\Requests\BecomeMemberRequest;
use Storage;
use DB;
use App\Review;
use Excel;
use App\ApplyNowButton;
use App\MemberFormPageText;
class BecomeMemberController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index($category)
    {
        $members = BecomeMember::where('category',$category)->latest()->get();
        return view('member-admin-dashboard.members-management.all-become-members',compact('members','category'));
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        //
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(BecomeMemberRequest $request)
    {
        $fileName="";

       
        $become_member = BecomeMember::create([
            'name' => $request->name,
            'email' => $request->email,
            'birthday' => $request->birthdate,
            'country' => $request->country,
            'address' => $request->address,
            'phone' => $request->phone,
            'race' => $request->race,
            'religion' => $request->religion,
            'education' => $request->education,
            'health_status' => $request->health_status,
            'smoker' => $request->smoker,
            'eye_color' => $request->eye_color,
            'hair_color' => $request->hair_color,
            'weight' => $request->weight,
            'height' => $request->height,
            'blood_type' => $request->blood_type,
            'have_passport' => $request->have_passport,
            'willing_to_travel' => $request->willing_to_travel,
            'number_of_children' => $request->number_of_children,
            'number_of_surrogate_children' => $request->number_of_surrogate_children,
            'surrogate_type' => implode(',',$request->surrogate_type),
            'given_service_before' => $request->given_service_before,
            'marital_status' => $request->marital_status,
            'spoken_language' => implode(',',$request->spoken_language),
            'willing_to_assist' => implode(',',$request->willing_to_assist),
            'describe_yourself' => $request->describe_yourself,
            'why_become_member' => $request->why_become_member,
            'communication_letter' => $request->communication_letter,
            'video_url' => $request->video_url,
            'category' => $request->category,

        ]);
        if($request->hasFile('picture')){
            $pictures=array();
            foreach ($request->file('picture') as $picture) {
            $pictures[] = $fileName = time().$picture->getClientOriginalName();
            // dd($fileName);
            Storage::put('public/'.$fileName,file_get_contents($picture));
            }
            $become_member->picture = implode(',', $pictures);
            $become_member->save();
        }
        return redirect()->route('thankyou');
    }

    /**
     * Display the specified resource.
     *
     * @param  \App\BecomeMember  $becomeMember
     * @return \Illuminate\Http\Response
     */
    public function show(BecomeMember $becomeMember)
    {
         $countries = DB::table('countries')->get();
        $languages = DB::table('languages')->get();
        return view('member-admin-dashboard.members-management.single-member-details',compact('becomeMember','countries','languages'));
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  \App\BecomeMember  $becomeMember
     * @return \Illuminate\Http\Response
     */
    public function edit(BecomeMember $becomeMember)
    {
        $countries = DB::table('countries')->get();
        $languages = DB::table('languages')->get();
        $formText = MemberFormPageText::where('category',$becomeMember->category)->first();
        return view('member-admin-dashboard.members-management.edit-member-details',compact('becomeMember','countries','languages','formText'));
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \App\BecomeMember  $becomeMember
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, BecomeMember $becomeMember)
    {
        // dd($request->spoken_language);
        $category = $request->category;
            $becomeMember->name = $request->name;
            $becomeMember->email = $request->email;
            $becomeMember->birthday = $request->birthdate;
            $becomeMember->country = $request->country;
            $becomeMember->address = $request->address;
            $becomeMember->phone = $request->phone;
            $becomeMember->race = $request->race;
            $becomeMember->religion = $request->religion;
            $becomeMember->education = $request->education;
            $becomeMember->health_status = $request->health_status;
            $becomeMember->smoker = $request->smoker;
            $becomeMember->eye_color = $request->eye_color;
            $becomeMember->hair_color = $request->hair_color;
            $becomeMember->weight = $request->weight;
            $becomeMember->height = $request->height;
            $becomeMember->blood_type = $request->blood_type;
            $becomeMember->have_passport = $request->have_passport;
            $becomeMember->willing_to_travel = $request->willing_to_travel;
            $becomeMember->number_of_children = $request->number_of_children;
            $becomeMember->number_of_surrogate_children = $request->number_of_surrogate_children;
            $becomeMember->video_url = $request->video_url;
            if($request->filled('surrogate_type')){
                $becomeMember->surrogate_type = implode(',',$request->surrogate_type);
            }
            $becomeMember->given_service_before = $request->given_service_before;
            $becomeMember->marital_status = $request->marital_status;
            if($request->filled('spoken_language')){
                $becomeMember->spoken_language = implode(',',$request->spoken_language);
            }
            if($request->filled('willing_to_assist')){
                $becomeMember->willing_to_assist = implode(',',$request->willing_to_assist);
            }
            
            $becomeMember->describe_yourself = $request->describe_yourself;
            $becomeMember->why_become_member = $request->why_become_member;
            $becomeMember->communication_letter = $request->communication_letter;
            $becomeMember->category = $request->category;
            $becomeMember->save();

           if($request->hasFile('picture')){
                $pictures = array();
                //delete old pictures
                foreach (explode(',',$becomeMember->picture) as $picture) {
                    Storage::delete('public/'.$picture);
                }
                // add new pictures
                foreach ($request->file('picture') as $picture) {
                    $pictures[] = $fileName = time().$picture->getClientOriginalName();
                    // dd($fileName);
                    Storage::put('public/'.$fileName,file_get_contents($picture));
                    }
                    $becomeMember->picture = implode(',', $pictures);
                    $becomeMember->save();
            }
            if($request->filled('stars') && $request->filled('review')){
                $review = Review::where('become_member_id',$becomeMember->id)->first();
                if($review){
                    $review->stars = $request->stars;
                    $review->review = $request->review;
                    $review->save();
                }
                else{
                    $review = new Review([
                    'stars' => $request->stars,
                    'review' => $request->review,
                    ]);
                    $becomeMember->review()->save($review);
                }
            }
            return redirect()->route('become_members.index',$category)->with('success','Member data updated successfully');

    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  \App\BecomeMember  $becomeMember
     * @return \Illuminate\Http\Response
     */
    public function destroy(BecomeMember $becomeMember)
    {
        $becomeMember->delete();
        return back()->with('success','Member Deleted Sucessfully');
    }

    public function verifyMember(BecomeMember $becomeMember){
        if($becomeMember->status==0){
            $becomeMember->status = 1;
        }
        else{
            $becomeMember->status = 0;   
        }
        $becomeMember->save();
        return back()->with('success','Member Status updated successfully');
    }
    
    public function selectForm(){
        $button = ApplyNowButton::first();
        return view('front-end.become-member-management.select-category-page',compact('button'));
    }
}
