<?php

namespace App\Http\Controllers;

use App\ClinicReview;
use Illuminate\Http\Request;

class ClinicReviewController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        //
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        //
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        //
    }

    /**
     * Display the specified resource.
     *
     * @param  \App\ClinicReview  $clinicReview
     * @return \Illuminate\Http\Response
     */
    public function show(ClinicReview $clinicReview)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  \App\ClinicReview  $clinicReview
     * @return \Illuminate\Http\Response
     */
    public function edit(ClinicReview $clinicReview)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \App\ClinicReview  $clinicReview
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, ClinicReview $clinicReview)
    {
        //
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  \App\ClinicReview  $clinicReview
     * @return \Illuminate\Http\Response
     */
    public function destroy(ClinicReview $clinicReview)
    {
        //
    }
}
