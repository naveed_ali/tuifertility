<?php

namespace App\Http\Controllers;

use App\MemberFormPageText;
use Illuminate\Http\Request;
use App\ClinicFormPageText;
use App\ApplyNowButton;
class MemberFormPageTextController extends Controller
{
    public function edit($category)
    {
        if($category=='fertility-clinics'){
            $clinicMember = ClinicFormPageText::first();
            return view('member-admin-dashboard.text-management.clinic-member-form-text',compact('clinicMember'));
        }
        else{
            $becomeMember = MemberFormPageText::where('category',$category)->first();
        return view('member-admin-dashboard.text-management.member-form-text',compact('becomeMember'));
        }
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \App\MemberFormPageText  $memberFormPageText
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, MemberFormPageText $memberFormPageText)
    {
        $category = $request->category;
        $memberFormPageText->title = $request->title;
        $memberFormPageText->note = $request->note;
            $memberFormPageText->name = $request->name;
            $memberFormPageText->email = $request->email;
            $memberFormPageText->birthday = $request->birthdate;
            $memberFormPageText->country = $request->country;
            $memberFormPageText->address = $request->address;
            $memberFormPageText->phone = $request->phone;
            $memberFormPageText->race = $request->race;
            $memberFormPageText->race_data = $request->race_data;
            $memberFormPageText->religion = $request->religion;
            $memberFormPageText->religion_data = $request->religion_data;
            $memberFormPageText->education = $request->education;
            $memberFormPageText->education_data = $request->education_data;
            $memberFormPageText->health_status = $request->health_status;
            $memberFormPageText->health_status_data = $request->health_status_data;
            $memberFormPageText->smoker = $request->smoker;
            $memberFormPageText->smoker_data = $request->smoker_data;
            $memberFormPageText->eye_color = $request->eye_color;
            $memberFormPageText->eye_color_data = $request->eye_color_data;
            $memberFormPageText->hair_color = $request->hair_color;
            $memberFormPageText->hair_color_data = $request->hair_color_data;
            $memberFormPageText->weight = $request->weight;
            $memberFormPageText->weight_data = $request->weight_data;
            $memberFormPageText->height = $request->height;
            $memberFormPageText->height_data = $request->height_data;
            $memberFormPageText->blood_type = $request->blood_type;
            $memberFormPageText->blood_type_data = $request->blood_type_data;
            $memberFormPageText->have_passport = $request->have_passport;
            $memberFormPageText->have_passport_data = $request->have_passport_data;
            $memberFormPageText->willing_to_travel = $request->willing_to_travel;
            $memberFormPageText->willing_to_travel_data = $request->willing_to_travel_data;
            $memberFormPageText->number_of_children = $request->number_of_children;
            $memberFormPageText->number_of_children_data = $request->number_of_children_data;
            $memberFormPageText->number_of_surrogate_children = $request->number_of_surrogate_children;
            $memberFormPageText->number_of_surrogate_children_data = $request->number_of_surrogate_children_data;
            $memberFormPageText->video_url = $request->video_url;
            $memberFormPageText->video_url_hint = $request->video_url_hint;
            $memberFormPageText->add_photo = $request->add_photo;
            $memberFormPageText->first_photo = $request->first_photo;
            $memberFormPageText->add_file = $request->add_file;
                $memberFormPageText->surrogate_type = $request->surrogate_type;
                $memberFormPageText->surrogate_type_data = $request->surrogate_type_data;
            $memberFormPageText->given_service_before = $request->given_service_before;
            $memberFormPageText->given_service_before_data = $request->given_service_before_data;
            $memberFormPageText->marital_status = $request->marital_status;
            $memberFormPageText->marital_status_data = $request->marital_status_data;
                $memberFormPageText->spoken_language =$request->spoken_language;
                $memberFormPageText->spoken_language_data =$request->spoken_language_data;
                $memberFormPageText->willing_to_assist = $request->willing_to_assist;
                $memberFormPageText->willing_to_assist_data = $request->willing_to_assist_data;
            $memberFormPageText->describe_yourself = $request->describe_yourself;
            $memberFormPageText->why_become_member = $request->why_become_member;
            $memberFormPageText->communication_letter = $request->communication_letter;
            $memberFormPageText->complete_signup = $request->complete_signup;
            $memberFormPageText->category = $request->category;
            $memberFormPageText->save();
            return redirect()->route('member.form.edit',$category)->with('success','Page Text updated successfully');
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \App\MemberFormPageText  $memberFormPageText
     * @return \Illuminate\Http\Response
     */
    public function clinicUpdate(Request $request, ClinicFormPageText $clinicFormPageText)
    {
        $clinicFormPageText->title = $request->title;
        $clinicFormPageText->note = $request->note;
            $clinicFormPageText->name = $request->name;
            $clinicFormPageText->clinic_name = $request->clinic_name;
            $clinicFormPageText->email = $request->email;
            $clinicFormPageText->website = $request->website;
            $clinicFormPageText->country = $request->country;
            $clinicFormPageText->address = $request->address;
            $clinicFormPageText->phone = $request->phone;
            $clinicFormPageText->provide_services_for = $request->provide_services_for;
            $clinicFormPageText->provide_services_for_data = $request->provide_services_for_data;
            $clinicFormPageText->providing_service = $request->providing_service;
            $clinicFormPageText->providing_service_data = $request->providing_service_data;
            $clinicFormPageText->treatments = $request->treatments;
            $clinicFormPageText->treatments_data = $request->treatments_data;
            $clinicFormPageText->video_url = $request->video_url;
            $clinicFormPageText->video_url_hint = $request->video_url_hint;
            $clinicFormPageText->pictures = $request->pictures;
            // $clinicFormPageText->first_photo = $request->first_photo;
            $clinicFormPageText->add_file = $request->add_file;
            $clinicFormPageText->willing_to_assist = $request->willing_to_assist;
            $clinicFormPageText->willing_to_assist_data = $request->willing_to_assist_data;
            $clinicFormPageText->willing_to_assist_countries = $request->willing_to_assist_countries;
            $clinicFormPageText->describe_clinic = $request->describe_clinic;
            $clinicFormPageText->complete_signup = $request->complete_signup;
            // $clinicFormPageText->category = $request->category;
            $category = 'fertility-clinics';
            $clinicFormPageText->save();
            return redirect()->route('member.form.edit',$category)->with('success','Page Text updated successfully');
    }

    public function allPages(){
        return view('member-admin-dashboard.text-management.all-pages');
    }

    public function editApplyNowButtons(){
        $button = ApplyNowButton::first();
        return view('member-admin-dashboard.text-management.apply-now-buttons',compact('button'));
    }
    public function updateApplyNowButtons(Request $request, ApplyNowButton $applyNowButton){
        $applyNowButton->surrogate_mother = $request->surrogate_mother;
        $applyNowButton->sperm_donor = $request->sperm_donor;
        $applyNowButton->egg_donor = $request->egg_donor;
        $applyNowButton->fertility_clinic = $request->fertility_clinic;
        $applyNowButton->save();
        return redirect()->route('apply.now.buttons')->with('success','Button Text updated successfully');
    }
}
