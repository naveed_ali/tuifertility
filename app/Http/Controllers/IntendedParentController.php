<?php

namespace App\Http\Controllers;

use App\IntendedParent;
use Illuminate\Http\Request;
use Storage;
use App\Exports\IntendedParentEmailsExport;
use Maatwebsite\Excel\Facades\Excel;
class IntendedParentController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $members = IntendedParent::all();
        return view('admin-dashboard.members-management.intended-parent-members.all-intended-parents',compact('members'));
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        //
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        $request->validate([
            'email' => 'required|string|email|max:255|unique:intended_parents',
            'name' => 'required|string|max:255',
            'birthdate'=>'required',
            'address' => 'max:255',
            'phone' => 'max:255',
            'describe_yourself' => 'max:500',
            'communication_letter' => 'max:500',
            'reason_for_wanting_surrogate' => 'max:500',
        ]);
        $fileName="";

        if($request->hasFile('picture')){
            $file = $request->file('picture');
            $fileName = time().$file->getClientOriginalName();
            // dd($fileName);
            Storage::put('public/'.$fileName,file_get_contents($file));
        }
        $become_member = IntendedParent::create([
            'name' => $request->name,
            'email' => $request->email,
            'birthday' => $request->birthdate,
            'country' => $request->country,
            'address' => $request->address,
            'phone' => $request->phone,
            'nationality' => $request->nationality,
            'race' => $request->race,
            'religion' => $request->religion,
            'education' => $request->education,
            'your_couple_type' => $request->your_couple_type,
            'had_surrogate_before' => $request->had_surrogate_before,
            'budget' => $request->budget,
            'smoker' => $request->smoker,
            'willing_to_travel' => $request->willing_to_travel,
            'number_of_children' => $request->number_of_children,
            'number_of_surrogate_children' => $request->number_of_surrogate_children,
            'marital_status' => $request->marital_status,
            'prefer_surrogate_age' => $request->prefer_surrogate_age,
            'describe_yourself' => $request->describe_yourself,
            'reason_for_wanting_surrogate' => $request->reason_for_wanting_surrogate,
            'communication_letter' => $request->communication_letter,
            'category' => $request->category,
            'picture' => $fileName,

        ]);
        if($request->filled('prefer_surrogate_country')){
             $become_member->prefer_surrogate_country= implode(',',$request->prefer_surrogate_country);
        }
        if($request->filled('prefer_surrogate_nationality')){
             $become_member->prefer_surrogate_nationality= implode(',',$request->prefer_surrogate_nationality);
             $become_member->save();
        }
        if($request->filled('spoken_language')){
             $become_member->spoken_language= implode(',',$request->spoken_language);
             $become_member->save();
        }
        if($request->filled('looking_to_connect_with')){
             $become_member->looking_to_connect_with= implode(',',$request->looking_to_connect_with);
             $become_member->save();
        }
        
        return redirect()->route('thankyou');
    }

    /**
     * Display the specified resource.
     *
     * @param  \App\IntendedParent  $intendedParent
     * @return \Illuminate\Http\Response
     */
    public function show(IntendedParent $intendedParent)
    {
        return view('admin-dashboard.members-management.intended-parent-members.single-intended-parent-details',compact('intendedParent'));
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  \App\IntendedParent  $intendedParent
     * @return \Illuminate\Http\Response
     */
    public function edit(IntendedParent $intendedParent)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \App\IntendedParent  $intendedParent
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, IntendedParent $intendedParent)
    {
        //
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  \App\IntendedParent  $intendedParent
     * @return \Illuminate\Http\Response
     */
    public function destroy(IntendedParent $intendedParent)
    {
        $intendedParent->delete();
        return back()->with('success','Intended parent data has been delete suucessfully.');
    }
    public function allEmails(){
        $emails = IntendedParent::select('email')->get();
        return view('admin-dashboard.members-management.intended-parent-members.all-emails',compact('emails'));
    }
    public function exportEmails(){
         return Excel::download(new IntendedParentEmailsExport, 'emails.csv');
    }
}
