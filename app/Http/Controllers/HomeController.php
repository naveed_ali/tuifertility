<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Menu;
use App\Page;
use App\SubPage;
use DB;
use App\BecomeMember;
use App\FertilityClinicMember;
use App\MemberFormPageText;
use App\ClinicFormPageText;
class HomeController extends Controller
{
    public function index(){
    	// $menus = Menu::with('pages')->get();
    	return view('front-end.home-page');
    }
    public function showPage(Menu $menu, Page $page){
    	return view('front-end.menu-pages.page',compact('page'));
    }
    public function showSubPage(Menu $menu, Page $page, SubPage $subpage){
    	return view('front-end.menu-pages.subpage',compact('subpage'));	
    }
    public function BecomeMember(Request $request,$category){
        $countries = DB::table('countries')->get();
        $languages = DB::table('languages')->get();
        if($category=='fertility-clinics'){
            $formText = ClinicFormPageText::first();
            return view('front-end.become-member-management.become-fertility-clinic',compact('category','countries','languages','formText'));
        }
        else{
            $formText = MemberFormPageText::where('category',$category)->first();
            return view('front-end.become-member-management.become-member',compact('category','countries','languages','formText'));
        }
    }
    public function showSearchForm(Request $request){
        $members = BecomeMember::where('category',$request->category)->where('status',1)->get();
        $category = $request->category;
        $countries = DB::table('countries')->get();
        $languages = DB::table('languages')->get();
        if($category=='fertility-clinics'){
            $members = FertilityClinicMember::where('status',1)->get();
            return view('front-end.search-members.show-fertility-clinic-search-form',compact('members','countries','category'));
        }
        else{
            return view('front-end.search-members.show-search-form',compact('members','countries','languages','category'));
        }
        
    }

    public function searchResult(Request $request){
        $members = new BecomeMember;
        $category = $request->category;
        if($request->filled('willing_to_assist')){
             $terms = $request->willing_to_assist;
            $members = $members->where('willing_to_assist', 'like', "%$terms%");
        }
        if($request->filled('spoken_language')){
            $spoken_language = $request->spoken_language;
            $members = $members ->where('spoken_language', 'like', "%$spoken_language%");
        }
        if($request->filled('country')){
            // dd($request->country);
            $members = $members->where('country',$request->country);

        }
        if($request->filled('name')){
            $members = $members->where('name',$request->name);

        }
        if($request->filled('nationality')){
            $members = $members->where('nationality',$request->nationality);

        }
        if($request->filled('race')){
            $members = $members->where('race',$request->race);

        }
        if($request->filled('religion')){
            $members = $members->where('religion',$request->religion);

        }
        if($request->filled('education')){
            $members = $members->where('education',$request->education);

        }
        if($request->filled('smoker')){
            $members = $members->where('smoker',$request->smoker);

        }
        if($request->filled('hair_color')){
            $members = $members->where('hair_color',$request->hair_color);

        }
        if($request->filled('eye_color')){
            $members = $members->where('eye_color',$request->eye_color);

        }
        if($request->filled('marital_status')){
            $members = $members->where('marital_status',$request->marital_status);

        }if($request->filled('has_photo')){
            $members = $members->where('has_photo',$request->has_photo);

        }
        $members = $members->where('status',1)->where('category',$category)->get();
        $countries = DB::table('countries')->get();
        $languages = DB::table('languages')->get();
        return view('front-end.search-members.show-search-form',compact('members','countries','languages','category'));

    }

    public function singleMemberDetails(Request $request, BecomeMember $becomeMember){
        $category = $request->category;
        return view('front-end.search-members.single-member-details',compact('becomeMember','category'));
    }
    public function singleClinicMemberDetails(Request $request, FertilityClinicMember $fertilityClinicMember){
        $category = $request->category;
        return view('front-end.search-members.single-clinic-member-details',compact('fertilityClinicMember','category'));
    }
    public function searchFertilityClinicResult(Request $request){
        $members = new FertilityClinicMember;
        $category = $request->category;
        if($request->filled('willing_to_assist')){
             $terms = $request->willing_to_assist;
            $members = $members->where('willing_to_assist', 'like', "%$terms%");
        }
        if($request->filled('country')){
            // dd($request->country);
            $members = $members->where('country',$request->country);

        }
        if($request->filled('name')){
            $members = $members->where('name',$request->name);

        }
        if($request->filled('provide_services_for')){
             $provide_services_for = $request->provide_services_for;
            $members = $members->where('provide_services_for', 'like', "%$provide_services_for%");
        }
        if($request->filled('treatments')){
             $treatments = $request->treatments;
            $members = $members->where('treatments', 'like', "%$treatments%");
        }if($request->filled('providing_service')){
             $providing_service = $providing_service;
            $members = $members->where('providing_service', 'like', "%$providing_service%");
        }
        $members = $members->where('status',1)->get();
        $countries = DB::table('countries')->get();
        // $languages = DB::table('languages')->get();
        return view('front-end.search-members.show-fertility-clinic-search-form',compact('members','countries','category'));


    }
    public function intendedParentForm($category){
        $countries = DB::table('countries')->get();
        return view('front-end.become-member-management.become-intended-parent',compact('category','countries'));
    }

    public function thankyou(){
        return view('front-end.thank-you');
    }
}
