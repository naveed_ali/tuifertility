<?php

namespace App\Http\Controllers;

use App\FertilityClinicMember;
use Illuminate\Http\Request;
use Storage;
use DB;
use App\ClinicReview;
use App\ClinicFormPageText;
class FertilityClinicMemberController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $members = FertilityClinicMember::latest()->get();
        return view('member-admin-dashboard.members-management.fertility-clinic-members.all-clinic-members',compact('members'));
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        //
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        $request->validate([
            'email' => 'required|string|email|max:255|unique:fertility_clinic_members',
            'name' => 'required|string|max:255',
            'address' => 'max:255',
            'phone' => 'max:255',
            'website' => 'max:255',
            'treatments' => 'required',
            'provide_services_for' => 'required',
            'providing_service' => 'required',
            'willing_to_assist' => 'required',
            'willing_to_assist_countries' => 'required',
            'describe_clinic' => 'required|max:255',
            'video_url' => 'url',
            'pictures.*' => 'image',

        ]);

        $fertilityClinicMember = FertilityClinicMember::create([
            'clinic_name' => $request->clinic_name,
            'name' => $request->name,
            'email' => $request->email,
            'address' => $request->address,
            'website' => $request->website,
            'phone' => $request->phone,
            'country' => $request->country,
            'willing_to_assist' => implode(',',$request->willing_to_assist),
            'willing_to_assist_countries' => implode(',',$request->willing_to_assist_countries),
            'providing_service' => implode(',',$request->providing_service),
            'treatments' => implode(',',$request->treatments),
            'provide_services_for' => implode(',',$request->provide_services_for),
            'status' => 0,
            'video_url' => $request->video_url,
            'describe_clinic' => $request->describe_clinic,
        ]);
        if($request->hasFile('pictures')){
            $pictures=array();
            foreach ($request->file('pictures') as $picture) {
            $pictures[] = $fileName = time().$picture->getClientOriginalName();
            // dd($fileName);
            Storage::put('public/'.$fileName,file_get_contents($picture));
            }
            $fertilityClinicMember->pictures = implode(',', $pictures);
            $fertilityClinicMember->save();
        }

        return redirect()->route('thankyou');
    }

    /**
     * Display the specified resource.
     *
     * @param  \App\FertilityClinicMember  $fertilityClinicMember
     * @return \Illuminate\Http\Response
     */
    public function show(FertilityClinicMember $fertilityClinicMember)
    {
         $countries = DB::table('countries')->get();
        return view('member-admin-dashboard.members-management.fertility-clinic-members.single-clinic-member-details',compact('fertilityClinicMember','countries'));
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  \App\FertilityClinicMember  $fertilityClinicMember
     * @return \Illuminate\Http\Response
     */
    public function edit(FertilityClinicMember $fertilityClinicMember)
    {
        $countries = DB::table('countries')->get();
        $formText = ClinicFormPageText::first();
        return view('member-admin-dashboard.members-management.fertility-clinic-members.edit-clinic-member-details',compact('fertilityClinicMember','countries','formText'));
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \App\FertilityClinicMember  $fertilityClinicMember
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, FertilityClinicMember $fertilityClinicMember)
    {
            $fertilityClinicMember->name = $request->name;
            $fertilityClinicMember->clinic_name = $request->clinic_name;
            $fertilityClinicMember->email = $request->email;
            $fertilityClinicMember->country = $request->country;
            $fertilityClinicMember->address = $request->address;
            $fertilityClinicMember->phone = $request->phone;
            $fertilityClinicMember->website = $request->website;
            $fertilityClinicMember->video_url = $request->video_url;
            $fertilityClinicMember->describe_clinic = $request->describe_clinic;
            if($request->filled('willing_to_assist')){
                $fertilityClinicMember->willing_to_assist = implode(',',$request->willing_to_assist);
            }
            if($request->filled('willing_to_assist_countries')){
                $fertilityClinicMember->willing_to_assist_countries = implode(',',$request->willing_to_assist_countries);
            }
            if($request->filled('provide_services_for')){
                $fertilityClinicMember->provide_services_for = implode(',',$request->provide_services_for);
            }
            if($request->filled('providing_service')){
                $fertilityClinicMember->providing_service = implode(',',$request->providing_service);
            }
            if($request->filled('treatments')){
                $fertilityClinicMember->treatments = implode(',',$request->treatments);
            }
             if($request->hasFile('pictures')){
                $pictures = array();
                //delete old pictures
                foreach (explode(',',$fertilityClinicMember->pictures) as $picture) {
                    Storage::delete('public/'.$picture);
                }
                // add new pictures
                foreach ($request->file('pictures') as $picture) {
                    $pictures[] = $fileName = time().$picture->getClientOriginalName();
                    // dd($fileName);
                    Storage::put('public/'.$fileName,file_get_contents($picture));
                    }
                    $fertilityClinicMember->pictures = implode(',', $pictures);
            }
            if($request->filled('stars') && $request->filled('review')){
                $review = ClinicReview::where('fertility_clinic_member_id',$fertilityClinicMember->id)->first();
                if($review){
                    $review->stars = $request->stars;
                    $review->review = $request->review;
                    $review->save();
                }
                else{
                    $review = new ClinicReview([
                    'stars' => $request->stars,
                    'review' => $request->review,
                    ]);
                    $fertilityClinicMember->clinic_review()->save($review);
                }
            }
            $fertilityClinicMember->save();
            return redirect()->route('fertility_clinic_members.index')->with('success', $fertilityClinicMember->name."'s Information has been updated successfully");
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  \App\FertilityClinicMember  $fertilityClinicMember
     * @return \Illuminate\Http\Response
     */
    public function destroy(FertilityClinicMember $fertilityClinicMember)
    {
        $fertilityClinicMember->delete();
        return back()->with('success','Member Deleted Sucessfully');
    }

    public function verifyMember(FertilityClinicMember $fertilityClinicMember){
        if($fertilityClinicMember->status==0){
            $fertilityClinicMember->status = 1;
        }
        else{
            $fertilityClinicMember->status = 0;   
        }
        $fertilityClinicMember->save();
        return back()->with('success','Member Status updated successfully');
    }
}
