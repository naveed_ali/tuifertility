<?php

namespace App\Http\Controllers;

use App\ClinicFormPageText;
use Illuminate\Http\Request;

class ClinicFormPageTextController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        //
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        //
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        //
    }

    /**
     * Display the specified resource.
     *
     * @param  \App\ClinicFormPageText  $clinicFormPageText
     * @return \Illuminate\Http\Response
     */
    public function show(ClinicFormPageText $clinicFormPageText)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  \App\ClinicFormPageText  $clinicFormPageText
     * @return \Illuminate\Http\Response
     */
    public function edit(ClinicFormPageText $clinicFormPageText)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \App\ClinicFormPageText  $clinicFormPageText
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, ClinicFormPageText $clinicFormPageText)
    {
        //
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  \App\ClinicFormPageText  $clinicFormPageText
     * @return \Illuminate\Http\Response
     */
    public function destroy(ClinicFormPageText $clinicFormPageText)
    {
        //
    }
}
