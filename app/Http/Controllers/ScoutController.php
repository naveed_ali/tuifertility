<?php

namespace App\Http\Controllers;

use App\Scout;
use Illuminate\Http\Request;
use Storage;
class ScoutController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $members = Scout::all();
        return view('admin-dashboard.scouts-management.all-scouts',compact('members'));
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        return view('admin-dashboard.scouts-management.add-scout');
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        $fileName='';
        if($request->hasFile('picture')){
            $file = $request->file('picture');
            $fileName = time().$file->getClientOriginalName();
            // dd($fileName);
            Storage::put('public/'.$fileName,file_get_contents($file));
        }
        Scout::create([
            'name' => $request->name,
            'phone' => $request->phone,
            'messenger' => $request->messenger,
            'telegram' => $request->telegram,
            'whatsapp' => $request->whatsapp,
            'viber' => $request->viber,
            'picture' => $fileName,
        ]);
        return redirect()->route('scouts.index')->with('success','Scout member added successfully');
    }

    /**
     * Display the specified resource.
     *
     * @param  \App\Scout  $scout
     * @return \Illuminate\Http\Response
     */
    public function show(Scout $scout)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  \App\Scout  $scout
     * @return \Illuminate\Http\Response
     */
    public function edit(Scout $scout)
    {
        return view('admin-dashboard.scouts-management.edit-scout',compact('scout'));
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \App\Scout  $scout
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, Scout $scout)
    {
        $scout->name = $request->name;
        $scout->phone = $request->phone;
        $scout->messenger = $request->messenger;
        $scout->whatsapp = $request->whatsapp;
        $scout->viber = $request->viber;
        $scout->telegram = $request->telegram;

        if($request->hasFile('picture')){
            Storage::delete('public/'.$scout->picture);

             $file = $request->file('picture');
            $fileName = time().$file->getClientOriginalName();
            // dd($fileName);
            Storage::put('public/'.$fileName,file_get_contents($file));
            $scout->picture = $fileName;
        }
        $scout->save();
        return redirect()->route('scouts.index')->with('success',$scout->name."'s Information has been updated successfully" );
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  \App\Scout  $scout
     * @return \Illuminate\Http\Response
     */
    public function destroy(Scout $scout)
    {
        $scout->delete();
        return back()->with('success','Scout member Deleted successfully');
    }
}
