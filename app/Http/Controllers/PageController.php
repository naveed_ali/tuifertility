<?php

namespace App\Http\Controllers;

use App\Page;
use Illuminate\Http\Request;
use Storage;
class PageController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $pages = Page::all();
        return view('admin-dashboard.pages-management.all-pages',compact('pages'));
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        return view('admin-dashboard.pages-management.create-page');
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        $request->validate([
            'name' => 'required|unique:pages|min:4|max:15',
            'slug' => 'required|unique:pages',
            'title' => 'required|unique:pages',
            'description' => 'required',
        ]);
        $fileName="";

        if($request->hasFile('picture')){
            $file = $request->file('picture');
            $fileName = time().$file->getClientOriginalName();
            // dd($fileName);
            Storage::put('public/'.$fileName,file_get_contents($file));
        }
        Page::create([
            'name' => $request->name,
            'title' => $request->title,
            'slug' => $request->slug,
            'meta_data' => $request->meta_data,
            'description' => $request->description,
            'status' => $request->status,
            'menu_id' => $request->menu_id,
            'picture' => $fileName,
        ]);

        return redirect()->route('pages.index')->with('success','Page Added successfully');
    }

    /**
     * Display the specified resource.
     *
     * @param  \App\Page  $page
     * @return \Illuminate\Http\Response
     */
    public function show(Page $page)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  \App\Page  $page
     * @return \Illuminate\Http\Response
     */
    public function edit(Page $page)
    {
        return view('admin-dashboard.pages-management.edit-page',compact('page'));
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \App\Page  $page
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, Page $page)
    {
        $request->validate([
            'name' => 'required',
            'slug' => 'required',
            'title' => 'required',
            'description' => 'required',
        ]);

        $page->name = $request->name;
        $page->title = $request->title;
        $page->slug = $request->slug;
        $page->status = $request->status;
        $page->meta_data = $request->meta_data;
        $page->description = $request->description;
        $page->menu_id = $request->menu_id;
        if($request->hasFile('picture')){
            Storage::delete('public/'.$page->picture);
            $file = $request->file('picture');
            $fileName = time().$file->getClientOriginalName();
            // dd($fileName);
            Storage::put('public/'.$fileName,file_get_contents($file));
            $page->picture = $fileName;
            $page->save();
            }
        $page->save();
        return redirect()->route('pages.index')->with('success','Page has been updated successfully');
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  \App\Page  $page
     * @return \Illuminate\Http\Response
     */
    public function destroy(Page $page)
    {
        $page->delete();
        return back()->with('success','Page has been deleted successfully');
    }
}
