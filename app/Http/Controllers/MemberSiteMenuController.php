<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Menu;
class MemberSiteMenuController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        return view('member-admin-dashboard.menus-management.all-menus');
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        $request->validate([
            'name' => 'required|unique:menus|min:4',
            'slug' => 'required|unique:menus',
        ]);
        Menu::create([
            'name' => $request->name,
            'slug' => $request->slug,
            'category' => 1,
        ]);
        return redirect()->route('member.menus.index')->with('success','Menu Added successfully');
    }

    /**
     * Display the specified resource.
     *
     * @param  \App\Menu  $menu
     * @return \Illuminate\Http\Response
     */
    public function show(Menu $menu)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  \App\Menu  $menu
     * @return \Illuminate\Http\Response
     */
    public function edit(Menu $menu)
    {
        return view('member-admin-dashboard.menus-management.edit-menu',compact('menu'));
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \App\Menu  $menu
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, Menu $menu)
    {
        $request->validate([
            'name' => 'required|min:4',
            'slug' => 'required',
        ]);

        $menu->name = $request->name;
        $menu->slug = $request->slug;
        $menu->save();
        return redirect()->route('member.menus.index')->with('success','Menu Added Successfully');
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  \App\Menu  $menu
     * @return \Illuminate\Http\Response
     */
    public function destroy(Menu $menu)
    {
        $menu->delete();
        return back()->with('success','Menu deleted Successfully');
    }
}
