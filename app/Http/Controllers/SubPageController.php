<?php

namespace App\Http\Controllers;

use App\SubPage;
use Illuminate\Http\Request;
use App\Page;
use Storage;
class SubPageController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $subpages = SubPage::all();
        return view('admin-dashboard.sub-pages-management.all-subpages',compact('subpages'));
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        $pages = Page::all();
        return view('admin-dashboard.sub-pages-management.create-subpage',compact('pages'));
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        $request->validate([
            'name' => 'required|unique:sub_pages|min:4|max:15',
            'slug' => 'required|unique:sub_pages',
            'title' => 'required|unique:sub_pages',
            'description' => 'required',
        ]);
        $fileName="";

        if($request->hasFile('picture')){
            $file = $request->file('picture');
            $fileName = time().$file->getClientOriginalName();
            // dd($fileName);
            Storage::put('public/'.$fileName,file_get_contents($file));
        }
        SubPage::create([
            'name' => $request->name,
            'title' => $request->title,
            'slug' => $request->slug,
            'meta_data' => $request->meta_data,
            'description' => $request->description,
            'status' => $request->status,
            'page_id' => $request->page_id,
            'picture' => $fileName,
        ]);
        return redirect()->route('subpages.index')->with('success','Sub Page Added successfully');
    }

    /**
     * Display the specified resource.
     *
     * @param  \App\Page  $page
     * @return \Illuminate\Http\Response
     */
    public function show(SubPage $subpage)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  \App\Page  $page
     * @return \Illuminate\Http\Response
     */
    public function edit(SubPage $subpage)
    {
        $pages = Page::all();
        return view('admin-dashboard.sub-pages-management.edit-subpage',compact('subpage','pages'));
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \App\Page  $page
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, SubPage $subpage)
    {
        $request->validate([
            'name' => 'required',
            'slug' => 'required',
            'title' => 'required',
            'description' => 'required',
        ]);

        $subpage->name = $request->name;
        $subpage->title = $request->title;
        $subpage->slug = $request->slug;
        $subpage->status = $request->status;
        $subpage->meta_data = $request->meta_data;
        $subpage->description = $request->description;
        $subpage->page_id = $request->page_id;
        if($request->hasFile('picture')){
            Storage::delete('public/'.$subpage->picture);
            $file = $request->file('picture');
            $fileName = time().$file->getClientOriginalName();
            // dd($fileName);
            Storage::put('public/'.$fileName,file_get_contents($file));
            $subpage->picture = $fileName;
            $subpage->save();
            }
        $subpage->save();
        return redirect()->route('subpages.index')->with('success','Sub Page has been updated successfully');
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  \App\Page  $page
     * @return \Illuminate\Http\Response
     */
    public function destroy(SubPage $subpage)
    {
        $subpage->delete();
        return back()->with('success','Sub Page has been deleted successfully');
    }
}
