<?php

namespace App\Http\Requests;

use Illuminate\Foundation\Http\FormRequest;

class BecomeMemberRequest extends FormRequest
{
    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize()
    {
        return true;
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules()
    {
        return [
            'email' => 'required|string|email|max:255|unique:become_members',
            'name' => 'required|string|max:255',
            'birthdate'=>'required',
            'country' => 'required|string',
            'address' => 'max:255',
            'phone' => 'max:255',
            'nationality' => 'required|string',
            'race' => 'required|string',
            'eye_color' => 'required',
            'hair_color' => 'required',
            'height' => 'required',
            'weight' => 'required',
            'willing_to_assist' => 'required',
            'describe_yourself' => 'max:255',
            'why_become_member' => 'required|string|max:255',
            // 'g-recaptcha-response' => 'required|recaptcha',
        ];
    }
}
