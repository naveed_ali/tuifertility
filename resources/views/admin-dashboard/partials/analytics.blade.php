<div class="col-xl-3 col-md-6 margin-bottom-30px">
                    <a class="d-block padding-30px background-main-color box-shadow border-radius-10 hvr-float">
                        <h6 class="text-white margin-0px font-weight-400">
                            <i class="far fa-map text-icon-large margin-bottom-10px opacity-5 d-block"></i>
                            <span class="font-2 text-extra-large">1456</span>
                            <span class="margin-left-10px">Added Listings</span>
                        </h6>
                    </a>
                </div>

                <div class="col-xl-3 col-md-6 margin-bottom-30px">
                    <a class="d-block padding-30px background-lime box-shadow border-radius-10 hvr-float">
                        <h6 class="text-white margin-0px font-weight-400">
                            <i class="far fa-user text-icon-large margin-bottom-10px opacity-5 d-block"></i>
                            <span class="font-2 text-extra-large">450</span>
                            <span class="margin-left-10px">Doctors</span>
                        </h6>
                    </a>
                </div>

                <div class="col-xl-3 col-md-6 margin-bottom-30px">
                    <a class="d-block padding-30px background-amber box-shadow border-radius-10 hvr-float">
                        <h6 class="text-white margin-0px font-weight-400">
                            <i class="far fa-star text-icon-large margin-bottom-10px opacity-5 d-block"></i>
                            <span class="font-2 text-extra-large">12,550</span>
                            <span class="margin-left-10px">Total Reviews</span>
                        </h6>
                    </a>
                </div>

                <div class="col-xl-3 col-md-6 margin-bottom-30px">
                    <a class="d-block padding-30px background-red box-shadow border-radius-10 hvr-float">
                        <h6 class="text-white margin-0px font-weight-400">
                            <i class="fas fa-chart-line text-icon-large margin-bottom-10px opacity-5 d-block"></i>
                            <span class="font-2 text-extra-large">2,560,456</span>
                            <span class="margin-left-10px">Visitors</span>
                        </h6>
                    </a>
                </div>