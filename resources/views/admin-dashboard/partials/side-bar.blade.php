<nav class="navbar navbar-expand-lg navbar-dark z-index-9  fixed-top" id="mainNav">

        <div class="collapse navbar-collapse" id="navbarResponsive">

            <ul class="navbar-nav navbar-sidenav background-main-color admin-nav" id="admin-nav">
                <li class="nav-item">
                    <span class="nav-title-text">Main</span>
                </li>
                <li class="nav-item" data-toggle="tooltip" data-placement="right" title="Dashboard">
                    <a class="nav-link active" href="dashboard-home.html">
                        <i class="fas fa-fw fa-home"></i><span class="nav-link-text">Dashboard</span>
                    </a>
                </li>
                <li class="nav-item" data-toggle="tooltip" data-placement="right" title="My items">
                    <a class="nav-link" href="{{route('intended_parents.index')}}">
                <i class="fa fa-fw fa-table"></i>
                <span class="nav-link-text">Intended Parents</span>
              </a>
                </li>
                 <li class="nav-item" data-toggle="tooltip" data-placement="right" title="My items">
                    <a class="nav-link" href="{{route('all.emails')}}">
                <i class="fa fa-fw fa-table"></i>
                <span class="nav-link-text">Emails</span>
              </a>
                </li>
                 <li class="nav-item" data-toggle="tooltip" data-placement="right" title="My items">
                    <a class="nav-link" href="{{route('settings.index')}}">
                <i class="fa fa-fw fa-table"></i>
                <span class="nav-link-text">Settings</span>
              </a>
                </li>
                 <!-- <li class="nav-item" data-toggle="tooltip" data-placement="right" title="My items">
                    <a class="nav-link" href="{{route('export.emails')}}">
                <i class="fa fa-fw fa-table"></i>
                <span class="nav-link-text">Exports</span>
              </a>
                </li> -->
                <li class="nav-item" data-toggle="tooltip" data-placement="right" title="My Favorites">
                    <a class="nav-link" href="{{route('scouts.index')}}">
                <i class="fa fa-fw fa-user-circle"></i>
                <span class="nav-link-text">Scout Members</span>
              </a>
                </li>
                <!-- <li class="nav-item" data-toggle="tooltip" data-placement="right" title="Reviews">
                    <a class="nav-link" href="dashboard-reviews.html">
                <i class="fa fa-fw fa-star"></i>
                <span class="nav-link-text">Reviews</span>
              </a>
                </li> -->
                <li class="nav-item">
                    <span class="nav-title-text">CMS</span>
                </li>
                <li class="nav-item" data-toggle="tooltip" data-placement="right" title="Menus Management">
                    <a class="nav-link" href="{{route('menus.index')}}">
                        <i class="far fa-fw fa-bookmark"></i>
                        <span class="nav-link-text">Text Management</span>
                    </a>
                </li>
                <li class="nav-item" data-toggle="tooltip" data-placement="right" title="Menus Management">
                    <a class="nav-link" href="{{route('menus.index')}}">
                        <i class="far fa-fw fa-bookmark"></i>
                        <span class="nav-link-text">Menus Management</span>
                    </a>
                </li>
                <li class="nav-item" data-toggle="tooltip" data-placement="right" title="Pages Management">
                    <a class="nav-link" href="{{route('pages.index')}}">
                        <i class="far fa-fw fa-bookmark"></i>
                        <span class="nav-link-text">Pages Management</span>
                    </a>
                </li>
                <li class="nav-item" data-toggle="tooltip" data-placement="right" title="Sub Pages Management">
                    <a class="nav-link" href="{{route('subpages.index')}}">
                        <i class="far fa-fw fa-bookmark"></i>
                        <span class="nav-link-text">Sub Pages Management</span>
                    </a>
                </li>
                <!-- <li class="nav-item" data-toggle="tooltip" data-placement="right" title="Add Listing">
                    <a class="nav-link" href="dashboard-add-listing.html">
                        <i class="fa fa-fw fa-plus-circle"></i>
                        <span class="nav-link-text">Add Listing</span>
                    </a>
                </li>
                <li class="nav-item" data-toggle="tooltip" data-placement="right" title="Packages">
                    <a class="nav-link" href="dashboard-packages.html">
                        <i class="far fa-fw fa-list-alt"></i>
                        <span class="nav-link-text">Packages</span>
                    </a>
                </li>

                <li class="nav-item">
                    <span class="nav-title-text">User Area</span>
                </li> -->
                <li class="nav-item" data-toggle="tooltip" data-placement="right" title="My Profile">
                    <a class="nav-link" href="dashboard-my-profile.html">
                        <i class="fa fa-fw fa-user-circle"></i>
                        <span class="nav-link-text">My Profile</span>
                    </a>
                </li>
                <li class="nav-item" data-toggle="tooltip" data-placement="right" title="Sing Out">
                    <a class="nav-link" href="#" data-toggle="modal" data-target="#exampleModal">
                        <i class="fa fa-fw fa-sign-out-alt"></i>
                        <span class="nav-link-text">Sing Out</span>
                    </a>
                </li>
            </ul>
        </div>
    </nav>