@extends('admin-dashboard.layouts.master')
@section('additional-head')
<link rel="stylesheet" type="text/css" href="https://cdn.datatables.net/1.10.20/css/jquery.dataTables.min.css">
@endsection
@section('content')
<hr>
@if ($errors->any())
    <div class="alert alert-danger">
        <ul>
            @foreach ($errors->all() as $error)
                <li>{{ $error }}</li>
            @endforeach
        </ul>
    </div>
@endif
<h2>All Scout Members</h2>
<a href="{{route('scouts.create')}}" class="btn btn-primary float-right margin-bottom-40px">Add New Member</a>
<table class="table table-hover" id="myTable">
  <thead>
    <tr>
      <th scope="col">Image</th>
      <th scope="col">Name</th>
      <th scope="col">Phone</th>
      <th scope="col">Messenger</th>
      <th scope="col">WhatsApp</th>
      <th scope="col">Viber</th>
      <th scope="col">Telegram</th>
      <th scope="col">Action</th>
    </tr>
  </thead>
  <tbody>
  	@php $i=1; @endphp
    	@foreach($members as $member)
    <tr>
      <td><img src="{{asset('storage/'.$member->picture)}}"></td>
      <td style="white-space:nowrap; font-size: 13px;">{{$member->name}}</td>
      <td style="white-space:nowrap; font-size: 13px;">{{$member->phone}}</td>
      <td style="white-space:nowrap; font-size: 13px;">{{$member->messenger}}</td>
      <td style="white-space:nowrap; font-size: 13px;">{{$member->whatsapp}}</td>
      <td style="white-space:nowrap; font-size: 13px;">{{$member->viber}}</td>
      <td style="white-space:nowrap; font-size: 13px;">{{$member->telegram}}</td>
      <td>
        <a href="{{route('scouts.edit',$member->id)}}" title="Edit Scout Member Details"><i class="fa fa-edit"></i></a>
      	<a href="{{route('scouts.destroy',$member->id)}}" onclick="return confirm('Are you sre to delete the member ?')" title="Delete Member"><i class="fa fa-trash"></i></a>
      </td>
    </tr>
    @php $i++; @endphp
      @endforeach
  </tbody>
</table>
@endsection
@section('additional-js')
<script type="text/javascript" src="https://cdn.datatables.net/1.10.20/js/jquery.dataTables.min.js"></script>
<script type="text/javascript">
  $(document).ready( function () {
    $('#myTable').DataTable({
      "lengthMenu": [[100, 200, 300, -1], [100, 200, 300, "All"]]
    });
} );
</script>
@endsection