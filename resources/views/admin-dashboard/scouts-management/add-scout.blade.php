@extends('admin-dashboard.layouts.master')
@section('additional-head')
@endsection
@section('content')
<!-- Page Title -->
                <div id="page-title" class="padding-30px background-white full-width">
                    <div class="container">
                        <ol class="breadcrumb opacity-5">
                            <li><a href="#">Home</a></li>
                            <li><a href="#">Add Scout member</a></li>
                        </ol>
                        <h1 class="font-weight-300">Create New Scout Member</h1>
                    </div>
                </div>
                <!-- // Page Title -->

                <div class="margin-tb-45px full-width">
                    <div class="padding-30px background-white border-radius-20 box-shadow">
                        <h3><i class="far fa-list-alt margin-right-10px text-main-color"></i> Scout Member Information </h3>
                        <hr>
                        <form method="post" action="{{route('scouts.store')}}" enctype="multipart/form-data">
                        	@csrf
                            <div class="form-group margin-bottom-20px">
                                <label><i class="far fa-user fa-lg margin-right-10px"></i>Name</label>
                                <input type="text" class="form-control form-control-sm" name="name" id="name" placeholder="Name of the scout member">
                            </div>
                            <div class="form-group margin-bottom-20px">
                                <label><i class="far fa fa-phone fa-lg margin-right-10px"></i> Phone Number</label>
                                <input type="text" class="form-control form-control-sm" name="phone" id="phone" placeholder="Phone Number">
                            </div>
                            <div class="row">
                                <div class="col-md-12 form-group margin-bottom-20px">
                                    <label><i class="fab fa-facebook-messenger fa-lg  margin-right-10px"></i>Messenger</label>
                                    <input type="text" name="messenger" class="form-control form-control-sm" placeholder="Link of the Messenger">
                                </div>
                            </div>
                            <div class="row">
                                <div class="col-md-12 form-group margin-bottom-20px">
                                    <label><i class="fab fa-whatsapp fa-lg margin-right-10px"></i>WhatsApp</label>
                                    <input type="text" name="whatsapp" class="form-control form-control-sm" placeholder="Link of the WhatsApp">
                                </div>
                            </div>
                            <div class="row">
                                <div class="col-md-12 form-group margin-bottom-20px">
                                    <label><i class="fab fa-telegram fa-lg margin-right-10px"></i>Telegram</label>
                                    <input type="text" name="telegram" class="form-control form-control-sm" placeholder="Link of the Telegram">
                                </div>
                            </div>
                            <div class="row">
                                <div class="col-md-12 form-group margin-bottom-20px">
                                    <label><i class="fab fa-viber fa-lg margin-right-10px"></i>Viber</label>
                                    <input type="text" name="viber" class="form-control form-control-sm" placeholder="Link of the Viber">
                                </div>
                            </div>
                            <div class="row">
                                <div class="col-md-12 form-group margin-bottom-20px">
                                    <label><i class="fas fa-viber margin-right-10px"></i>Image of the member</label>
                                    <input type="file" name="picture" class="form-control form-control-sm" placeholder="Image of the Member">
                                </div>
                            </div>
                    </div>
                </div>
                <button type="submit" class="btn btn-lg border-2  btn-primary btn-block border-radius-15 padding-15px box-shadow">Add Member</button>
                </form>
@endsection
@section('additional-js')

@endsection