@extends('admin-dashboard.layouts.master')

@section('content')
<hr>
<a class="btn btn-primary float-right" href="{{route('subpages.create')}}" style="color: white;"><span class="fa fa-plus"></span> Add New Sub Page</a>
<button class="btn btn-primary float-right mr-2" data-toggle="modal" data-target="#createMenu"><span class="fa fa-plus"></span> Add New Menu</button>
@if ($errors->any())
    <div class="alert alert-danger">
        <ul>
            @foreach ($errors->all() as $error)
                <li>{{ $error }}</li>
            @endforeach
        </ul>
    </div>
@endif
<h2>All Sub Pages</h2>
<table class="table table-hover">
  <thead>
    <tr>
      <th scope="col">#</th>
      <th scope="col">Name</th>
      <th scope="col">Slug</th>
      <th scope="col">Title</th>
      <th scope="col">Main Page</th>
      <th scope="col">Status</th>
      <th scope="col">Action</th>
    </tr>
  </thead>
  <tbody>
  	@php $i=1; @endphp
    	@foreach($subpages as $page)
    <tr>
      <th scope="row">{{$i}}</th>
      <td>{{$page->name}}</td>
      <td>{{$page->slug}}</td>
      <td>{{$page->title}}</td>
      <td>{{$page->page->name}}</td>
      <td>{{$page->status=='1'?'Published':'Unpublished'}}</td>
      <td>
      	<a href="{{route('subpages.edit',$page->slug)}}" title="Edit Page"><i class="fa fa-edit"></i></a>
      	<a href="{{route('subpages.destroy',$page->slug)}}" onclick="return confirm('Are you sre to delete the Sub Page ?')" title="Delete Page"><i class="fa fa-trash"></i></a>
      </td>
    </tr>
    @php $i++; @endphp
      @endforeach
  </tbody>
</table>
@endsection
@section('modal')
<div class="modal fade" id="createMenu" tabindex="-1" role="dialog" aria-labelledby="exampleModalLabel" aria-hidden="true">
            <div class="modal-dialog" role="document">
                <div class="modal-content">
                    <div class="modal-header">
                        <h5 class="modal-title" id="exampleModalLabel">Add New Menu</h5>
                        <button class="close" type="button" data-dismiss="modal" aria-label="Close">
                  <span aria-hidden="true">×</span>
                </button>
                    </div>
                    <div class="modal-body">
                    	<form method="post" action="{{route('menus.store')}}">
                    		@csrf
						  <div class="form-group">
						    <label for="name">Menu Name</label>
						    <input type="text" class="form-control" name="name" id="name" aria-describedby="Menu Name" placeholder="Menu Name">
						  </div>
						  <div class="form-group">
						    <label for="slug">Slug</label>
						    <input type="text" class="form-control" name="slug" id="slug" placeholder="Menu Slug">
						  </div>
						  <button type="submit" class="btn btn-primary">Add Menu</button>
						</form>
                    </div>
                    <div class="modal-footer">
                        <button class="btn btn-secondary" type="button" data-dismiss="modal">Cancel</button>
                    </div>
                </div>
            </div>
        </div>
    </div>
@endsection