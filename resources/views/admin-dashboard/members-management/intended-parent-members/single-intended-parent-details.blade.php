@extends('admin-dashboard.layouts.master')
@section('additional-head')
<!-- <link href="https://stackpath.bootstrapcdn.com/bootstrap/3.3.7/css/bootstrap.min.css" rel="stylesheet"> -->
@endsection
@section('content')
<!-- Page Title -->
                <div id="page-title" class="padding-30px background-white full-width">
                    <div class="container">
                        <ol class="breadcrumb opacity-5">
                            <li><a href="#">Home</a></li>
                            <li><a href="#">Member Details Page</a></li>
                        </ol>
                        <h1 class="font-weight-300">Member Details Page</h1>
                    </div>
                </div>
                <!-- // Page Title -->

                <div class="margin-tb-45px full-width">
                    <div class="padding-30px background-white border-radius-20 box-shadow">
                        <h3><i class="far fa-list-alt margin-right-10px text-main-color"></i> {{$intendedParent->name}}'s Detailed Information </h3>
                        <hr>
                        <form method="post" action="#" enctype="multipart/form-data">
                    @csrf
                    <input type="hidden" name="category" value="{{$intendedParent->category}}">
                     <div class="form-group label-floating">
                        
                        <img src="{{asset('storage/'.$intendedParent->picture)}}"   height="100">
                    </div>
                    <div class="form-group label-floating">
                        <label class="control-label">Your Email*</label>
                        <input class="form-control" selected value="{{$intendedParent->email}}" placeholder="" name="email" type="email">
                    </div>
                    <div class="form-group label-floating">
                        <label class="control-label">Name</label>
                        <input class="form-control" selected value="{{$intendedParent->name}}" name="name" placeholder="" type="text">
                    </div>
                    <div class="form-group label-floating">
                        <label class="control-label">Birthday (mo/day/yr)*</label>
                        <input class="form-control" selected value="{{$intendedParent->birthday}}" type="date">
                    </div>
                    <div class="form-group label-floating is-select">
                        <label class="control-label">Country</label>
                        <select class="selectpicker form-control" name="country">
                          
                          <option value="{{$intendedParent->country}}" selected>{{$intendedParent->country}}</option>
                       </select>
                    </div>
                    <div class="form-group label-floating">
                        <label class="control-label">Address</label>
                        <input class="form-control" selected value="{{$intendedParent->address}}"  type="text">
                    </div>
                    <div class="form-group label-floating">
                        <label class="control-label">Phone/WhatsApp/Viber/Telegram</label>
                        <input class="form-control" selected value="{{$intendedParent->phone}}" type="text">
                    </div>
                    <div class="form-group label-floating is-select">
                        <label class="control-label">Nationality*</label>
                        <select class="selectpicker form-control" name="nationality">
                              <option selected value="{{$intendedParent->nationality}}">{{$intendedParent->nationality}}</option> 
                       </select>
                    </div>

                    <div class="form-group label-floating is-select">
                        <label class="control-label">Race</label>
                        <select class="selectpicker form-control" name="race">
                           <option selected value="{{$intendedParent->race}}">{{$intendedParent->race}}</option>
                       </select>
                    </div>

                    <div class="form-group label-floating is-select">
                        <label class="control-label">Religion</label>
                        <select class="selectpicker form-control" name="religion">
                           <option selected value="{{$intendedParent->religion}}">{{$intendedParent->religion}}</option>
                       </select>
                    </div>
                    <div class="form-group label-floating is-select">
                        <label class="control-label">Education</label>
                        <select class="selectpicker form-control" name="education">
                           <option selected value="{{$intendedParent->education}}">{{$intendedParent->education}}</option>
                       </select>
                    </div>
                    <div class="form-group label-floating is-select">
                        <label class="control-label">Smoker</label>
                        <select class="selectpicker form-control" name="smoker">
                            <option >{{$intendedParent->smoker==1?'Yes':'No'}}</option>
                       </select>
                    </div>
                    
                    <div class="form-group label-floating is-select">
                        <label class="control-label">Willing To Travel</label>
                        <select class="selectpicker form-control" name="willing_to_travel">
                            <option selected value="{{$intendedParent->willing_to_travel}}">{{$intendedParent->willing_to_travel}}</option>
                       </select>
                    </div>
                    <div class="form-group label-floating is-select">
                        <label class="control-label">Number of Children(non-donor)</label>
                        <select class="selectpicker form-control" name="number_of_children">
                            <option selected value="{{$intendedParent->number_of_children}}">{{$intendedParent->number_of_children}}</option>
                       </select>
                    </div>
                    <div class="form-group label-floating is-select">
                        <label class="control-label">Number of Surrogate Children</label>
                        <select class="selectpicker form-control" name="number_of_surrogate_children">
                            <option selected value="{{$intendedParent->number_of_surrogate_children}}">{{$intendedParent->number_of_surrogate_children}}</option>
                       </select>
                    </div>
                    <div class="form-group label-floating is-select">
                        <label class="control-label">Have you had a Surrogate/Donor before?</label>
                        <select class="selectpicker form-control" name="given_service_before">
                            <option selected value="{{$intendedParent->had_surrogate_before}}">{{$intendedParent->had_surrogate_before}}</option>
                       </select>
                    </div>
                    <div class="form-group label-floating is-select">
                        <label class="control-label">Marital Status</label>
                        <select class="selectpicker form-control" name="marital_status">
                            <option selected value="{{$intendedParent->marital_status}}">{{$intendedParent->marital_status}}</option>
                       </select>
                    </div>
                    <div class="form-group label-floating is-select">
                        <label class="control-label">Spoken Languages</label>
                        <select class="selectpicker form-control" name="spoken_language">
                        <option selected value="{{$intendedParent->spoken_language}}">{{$intendedParent->spoken_language}}</option>
                    </select>
                    </div>
                    <div class="form-group label-floating is-select">
                        <label class="control-label">What is your budget for surrogacy/donor expenses?</label>
                        <select class="selectpicker form-control" name="budget">
                          <option selected value="{{$intendedParent->budget}}">{{$intendedParent->budget}}</option>
                        </select>
                    </div>
                    <div class="form-group label-floating is-select">
                        <label class="control-label">Looking to Connect With</label>
                        <select class="selectpicker form-control" name="looking_to_connect_with">
                          <option selected value="{{$intendedParent->looking_to_connect_with}}">{{$intendedParent->looking_to_connect_with}}</option>
                        </select>
                    </div>
                    <div class="form-group label-floating is-select">
                        <label class="control-label">Explain Your Couple Type</label>
                        <select class="selectpicker form-control" name="your_couple_type">
                          <option selected value="{{$intendedParent->your_couple_type}}">{{$intendedParent->your_couple_type}}</option>
                        </select>
                    </div>
                    <div class="form-group label-floating is-select">
                        <label class="control-label">You prefer Surrogates/Donors between the ages of?</label>
                        <select class="selectpicker form-control" name="prefer_surrogate_age">
                          <option selected value="{{$intendedParent->prefer_surrogate_age}}">{{$intendedParent->prefer_surrogate_age}}</option>
                        </select>
                    </div>
                    <div class="form-group label-floating is-select">
                        <label class="control-label">You prefer Surrogates/Donors from which country?</label>
                        <select class="selectpicker form-control" name="prefer_surrogate_country">
                          <option selected value="{{$intendedParent->prefer_surrogate_country}}">{{$intendedParent->prefer_surrogate_country}}</option>
                        </select>
                    </div>
                    <div class="form-group label-floating is-select">
                        <label class="control-label">You prefer Surrogates/Donors from which nationality?</label>
                        <select class="selectpicker form-control" name="prefer_surrogate_nationality">
                          <option selected value="{{$intendedParent->prefer_surrogate_nationality}}">{{$intendedParent->prefer_surrogate_nationality}}</option>
                        </select>
                    </div>
                    <div class="form-group label-floating">
                        <label class="control-label">How would you best describe yourself?</label>
                        <textarea class="form-control" name="describe_yourself" >{{$intendedParent->describe_yourself}}</textarea>
                    </div>
                    <div class="form-group label-floating">
                        <label class="control-label">Explain your reasons for wanting a surrogate/donor</label>
                        <textarea class="form-control" name="reason_for_wanting_surrogate" >{{$intendedParent->reason_for_wanting_surrogate}}</textarea>
                    </div>
                    <div class="form-group label-floating">
                        <label class="control-label">Use this space to communicate a letter to potential Surrogate/Donor.</label>
                        <textarea class="form-control" name="communication_letter">{{$intendedParent->communication_letter}}</textarea>
                    </div>
                   
                </div>
                <a href="{{url()->previous()}}" class="btn btn-lg border-2  btn-primary btn-block border-radius-15 padding-15px box-shadow">Return Back</a>
            </form>
                
@endsection
@section('additional-js')
<!-- <script src="https://code.jquery.com/jquery-3.4.1.min.js"></script>
  <script src="https://stackpath.bootstrapcdn.com/bootstrap/3.3.7/js/bootstrap.min.js"></script> -->
<script src="https://cdn.jsdelivr.net/npm/summernote@0.8.16/dist/summernote.min.js"></script>
 <script>
    $(document).ready(function() {
        $('#summernote').summernote();
    });
  </script>
@endsection