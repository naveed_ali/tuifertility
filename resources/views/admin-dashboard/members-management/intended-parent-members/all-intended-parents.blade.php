@extends('admin-dashboard.layouts.master')
@section('additional-head')
<link rel="stylesheet" type="text/css" href="https://cdn.datatables.net/1.10.20/css/jquery.dataTables.min.css">
@endsection
@section('content')
<hr>
@if ($errors->any())
    <div class="alert alert-danger">
        <ul>
            @foreach ($errors->all() as $error)
                <li>{{ $error }}</li>
            @endforeach
        </ul>
    </div>
@endif
<h2>Intended Parents</h2><span>(Applied to use Surrogate Mothers, Sperm Donors, Egg Donors)</span>
<table class="table table-hover" id="myTable">
  <thead>
    <tr>
      <th scope="col">#</th>
      <th scope="col">Email</th>
      <th scope="col">Full Name</th>
      <th scope="col">Category</th>
      <th scope="col">Status</th>
      <th scope="col">Action</th>
    </tr>
  </thead>
  <tbody>
  	@php $i=1; @endphp
    	@foreach($members as $member)
    <tr>
      <th scope="row">{{$i}}</th>
      <td>{{$member->email}}</td>
      <td>{{$member->name}}</td>
      <td>{{$member->category}}</td>
      <td>{{$member->status==1 ? 'Verified':'Not Verified'}}</td>
      <td>
        <a href="{{route('intended_parents.show',$member->id)}}" title="Show Member Details"><i class="fa fa-eye"></i></a>
      	<a href="{{route('intended_parents.destroy',$member->id)}}" onclick="return confirm('Are you sre to delete the record ?')" title="Delete Member"><i class="fa fa-trash"></i></a>
      </td>
    </tr>
    @php $i++; @endphp
      @endforeach
  </tbody>
</table>
@endsection
@section('additional-js')
<script type="text/javascript" src="https://cdn.datatables.net/1.10.20/js/jquery.dataTables.min.js"></script>
<script type="text/javascript">
  $(document).ready( function () {
    $('#myTable').DataTable({
      "lengthMenu": [[100, 200, 300, -1], [100, 200, 300, "All"]]
    });
} );
</script>
@endsection