@extends('admin-dashboard.layouts.master')
@section('additional-head')
<link rel="stylesheet" type="text/css" href="https://cdn.datatables.net/1.10.20/css/jquery.dataTables.min.css">
@endsection
@section('content')
<hr>
@if ($errors->any())
    <div class="alert alert-danger">
        <ul>
            @foreach ($errors->all() as $error)
                <li>{{ $error }}</li>
            @endforeach
        </ul>
    </div>
@endif
<h2>All Intended Parent Emails</h2><span>(Applied to use Surrogate Mothers, Sperm Donors, Egg Donors)</span>
<a href="{{route('export.emails')}}" class="btn btn-primary" style="float: right;">Export All Emails</a>
<table class="table table-hover" id="myTable">
  <thead>
    <tr>
      <th scope="col">#</th>
      <th scope="col">Email</th>
    </tr>
  </thead>
  <tbody>
  	@php $i=1; @endphp
    	@foreach($emails as $email)
    <tr>
      <th scope="row">{{$i}}</th>
      <td>{{$email->email}}</td>
    </tr>
    @php $i++; @endphp
      @endforeach
  </tbody>
</table>
@endsection
@section('additional-js')
<script type="text/javascript" src="https://cdn.datatables.net/1.10.20/js/jquery.dataTables.min.js"></script>
<script type="text/javascript">
  $(document).ready( function () {
    $('#myTable').DataTable({
      "lengthMenu": [[100, 200, 300, -1], [100, 200, 300, "All"]]
    });
} );
</script>
@endsection