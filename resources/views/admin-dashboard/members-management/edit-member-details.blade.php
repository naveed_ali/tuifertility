@extends('admin-dashboard.layouts.master')
@section('additional-head')
<!-- <link href="https://stackpath.bootstrapcdn.com/bootstrap/3.3.7/css/bootstrap.min.css" rel="stylesheet"> -->
<link href="https://cdn.jsdelivr.net/npm/summernote@0.8.16/dist/summernote.min.css" rel="stylesheet">
@endsection
@section('content')
<!-- Page Title -->
                <div id="page-title" class="padding-30px background-white full-width">
                    <div class="container">
                        <ol class="breadcrumb opacity-5">
                            <li><a href="#">Home</a></li>
                            <li><a href="#">Edit informationPage</a></li>
                        </ol>
                        <h1 class="font-weight-300">Edit information Page</h1>
                    </div>
                </div>
                <!-- // Page Title -->

                <div class="margin-tb-45px full-width">
                     @if ($errors->any())
                        <div class="alert alert-danger">
                            <ul>
                                @foreach ($errors->all() as $error)
                                    <li>{{ $error }}</li>
                                @endforeach
                            </ul>
                        </div>
                    @endif
                    <div class="padding-30px background-white border-radius-20 box-shadow">
                        <h3><i class="far fa-list-alt margin-right-10px text-main-color"></i>{{$becomeMember->name}}'s Edit Details Page </h3>
                        <hr>
                        <form method="post" action="{{route('become_members.update',$becomeMember->id)}}" enctype="multipart/form-data">
                            @method('put')
                            @csrf
                    <input type="hidden" name="category" value="{{$becomeMember->category}}">
                    <div class="form-group label-floating">
                        <label class="control-label">Your Email*</label>
                        <input class="form-control" placeholder="" value="{{$becomeMember->email}}" name="email" type="email">
                    </div>
                    <div class="form-group label-floating">
                        <label class="control-label">Name</label>
                        <input class="form-control" value="{{$becomeMember->name}}" name="name" placeholder="" type="text">
                    </div>
                    <div class="form-group label-floating">
                        <label class="control-label">Birthday (mo/day/yr)*</label>
                        <input class="form-control" value="{{$becomeMember->birthday}}" name="birthdate" placeholder="Your Birthday" type="date">
                    </div>
                    <div class="form-group label-floating is-select">
                        <label class="control-label">Country</label>
                        <select class="selectpicker form-control" name="country">
                          @foreach($countries as $country)
                          <option value="{{$country->country_name}}" {{$becomeMember->country==$country->country_name?'selected':''}}>{{$country->country_name}}</option>
                          @endforeach
                       </select>
                    </div>
                    <div class="form-group label-floating">
                        <label class="control-label">Address</label>
                        <input class="form-control" value="{{$becomeMember->address}}" name="address" placeholder="Address" type="text">
                    </div>
                    <div class="form-group label-floating">
                        <label class="control-label">Phone/WhatsApp/Viber/Telegram</label>
                        <input class="form-control" value="{{$becomeMember->phone}}" name="phone" placeholder="Phone/WhatsApp/Viber/Telegram" type="text">
                    </div>
                    <div class="form-group label-floating is-select">
                        <label class="control-label">Race*</label>
                        <select class="selectpicker form-control" name="race">
                           <option value="African" {{$becomeMember->race=='African'?'selected':''}}>African</option>
                           <option value="american-indian" {{$becomeMember->race=='american-indian'?'selected':''}}>American Indian</option>
                           <option value="asian-(Chinese)" {{$becomeMember->race=='asian-(Chinese)'?'selected':''}} >Asian (Chinese)</option>
                           <option value="asian(Japanese)" {{$becomeMember->race=='asian(Japanese)'?'selected':''}}>Asian (Japanese)</option>
                           <option value="asian(other)" {{$becomeMember->race=='asian(other)'?'selected':''}}>Asian (other)</option>
                           <option value="caucasian" {{$becomeMember->race=='caucasian'?'selected':''}}>Caucasian</option>
                           <option value="hispanic" {{$becomeMember->race=='hispanic'?'selected':''}}>Hispanic</option>
                           <option value="middle-eastern" {{$becomeMember->race=='middle-eastern'?'selected':''}}>Middle Eastern</option>
                           <option value="pacific-islander" {{$becomeMember->race=='pacific-islander'?'selected':''}}>Pacific Islander</option>
                           <option value="other" {{$becomeMember->race=='other'?'selected':''}}>Other</option>
                       </select>
                    </div>

                    <div class="form-group label-floating is-select">
                        <label class="control-label">Religion</label>
                        <select class="selectpicker form-control" name="religion">
                           <option value="">Please Select</option>
                           
                           <option value="African Traditional &amp; Diasporic" {{$becomeMember->religion=='African Traditional &amp; Diasporic'?'selected':''}}>African Traditional &amp; Diasporic</option>
                           
                           <option value="Agnostic" {{$becomeMember->religion=='Agnostic'?'selected':''}}>Agnostic</option>
                           
                           <option value="Atheist" {{$becomeMember->religion=='Atheist'?'selected':''}}>Atheist</option>
                           
                           <option value="Baha'i" {{$becomeMember->religion=="Baha'i"?'selected':''}}>Baha'i</option>
                           
                           <option value="Buddhism" {{$becomeMember->religion=='Buddhism'?'selected':''}}>Buddhism</option>
                           
                           <option value="Cao Dai" {{$becomeMember->religion=='Cao Dai'?'selected':''}}>Cao Dai</option>
                           
                           <option value="Chinese traditional religion" {{$becomeMember->religion=='Chinese traditional religion'?'selected':''}}>Chinese traditional religion</option>
                           
                           <option value="Christianity" {{$becomeMember->religion=='Christianity'?'selected':''}}>Christianity</option>
                           
                           <option value="Hinduism" {{$becomeMember->religion=='Hinduism'?'selected':''}}>Hinduism</option>
                           
                           <option value="Islam" {{$becomeMember->religion=='Islam'?'selected':''}}>Islam</option>
                           
                           <option value="Jainism" {{$becomeMember->religion=='Jainism'?'selected':''}}>Jainism</option>
                           
                           <option value="Juche" {{$becomeMember->religion=='Juche'?'selected':''}}>Juche</option>
                           
                           <option value="Judaism" {{$becomeMember->religion=='Judaism'?'selected':''}}>Judaism</option>
                           
                           <option value="Neo-Paganism" {{$becomeMember->religion=='Neo-Paganism'?'selected':''}}>Neo-Paganism</option>
                           
                           <option value="Nonreligious" {{$becomeMember->religion=='Nonreligious'?'selected':''}}>Nonreligious</option>
                           
                           <option value="Other" {{$becomeMember->religion=='Other'?'selected':''}}>Other</option>
                           
                           <option value="primal-indigenous" {{$becomeMember->religion=='primal-indigenous'?'selected':''}}>primal-indigenous</option>
                           
                           <option value="Rastafarianism" {{$becomeMember->religion=='Rastafarianism'?'selected':''}}>Rastafarianism</option>
                           
                           <option value="Secular" {{$becomeMember->religion=='Secular'?'selected':''}}>Secular</option>
                           
                           <option value="Shinto" {{$becomeMember->religion=='Shinto'?'selected':''}}>Shinto</option>
                           
                           <option value="Sikhism" {{$becomeMember->religion=='Sikhism'?'selected':''}}>Sikhism</option>
                           
                           <option value="Spiritism" {{$becomeMember->religion=='Spiritism'?'selected':''}}>Spiritism</option>
                           
                           <option value="Tenrikyo" {{$becomeMember->religion=='Tenrikyo'?'selected':''}}>Tenrikyo</option>
                           
                           <option value="Unitarian-Universalism" {{$becomeMember->religion=='Unitarian-Universalism'?'selected':''}}>Unitarian-Universalism</option>

                           <option value="Zoroastrianism" {{$becomeMember->religion=='Zoroastrianism'?'selected':''}}>Zoroastrianism</option>
                       </select>
                    </div>
                    <div class="form-group label-floating is-select">
                        <label class="control-label">Education</label>
                        <select class="selectpicker form-control" name="education">
                           <option value="Never Finished High School" {{$becomeMember->education=='Never Finished High School'?'selected':''}}>Never Finished High School</option>
                           <option value="Completed High School" {{$becomeMember->education=='Completed High School'?'selected':''}}>Completed High School</option>
                           <option value="Completed College or University" {{$becomeMember->education=='Completed College or University'?'selected':''}}>Completed College or University</option>
                           <option value="Completed Master Degree" {{$becomeMember->education=='Completed Master Degree'?'selected':''}}>Completed Master Degree</option>
                       </select>
                    </div>
                    <div class="form-group label-floating is-select">
                        <label class="control-label">Health Status</label>
                        <select class="selectpicker form-control" name="health_status">
                            <option value="">Please Select Health Status</option>
                            <option value="1" {{$becomeMember->health_status=='1'?'selected':''}}>Good</option>
                            <option value="2" {{$becomeMember->health_status=='2'?'selected':''}}>Medium</option>
                            <option value="3" {{$becomeMember->health_status=='3'?'selected':''}}>Bad</option>
                       </select>
                    </div>
                    <div class="form-group label-floating is-select">
                        <label class="control-label">Smoker</label>
                        <select class="selectpicker form-control" name="smoker">
                            <option value="1" {{$becomeMember->smoker=='1'?'selected':''}}>Yes</option>
                            <option value="0" {{$becomeMember->smoker=='0'?'selected':''}}>No</option>
                       </select>
                    </div>
                    <div class="form-group label-floating is-select">
                        <label class="control-label">Eye Color*</label>
                        <select class="selectpicker form-control" name="eye_color">
                           <option value="Amber" {{$becomeMember->eye_color=='Amber'?'selected':''}}>Amber</option>
                           <option value="Blue" {{$becomeMember->eye_color=='Blue'?'selected':''}}>Blue</option>
                           <option value="Brown" {{$becomeMember->eye_color=='Brown'?'selected':''}}>Brown</option>
                           <option value="Green" {{$becomeMember->eye_color=='Green'?'selected':''}}>Green</option>
                           <option value="Grey" {{$becomeMember->eye_color=='Grey'?'selected':''}}>Grey</option>
                           <option value="Hazel" {{$becomeMember->eye_color=='Hazel'?'selected':''}}>Hazel</option>
                       </select>
                    </div>
                    <div class="form-group label-floating is-select">
                        <label class="control-label">Hair Color*</label>
                        <select class="selectpicker form-control" name="hair_color">
                           <option value="Auburn" {{$becomeMember->hair_color=='Auburn'?'selected':''}}>Auburn</option>
                           <option value="Black" {{$becomeMember->hair_color=='Black'?'selected':''}}>Black</option>
                           <option value="Blonde" {{$becomeMember->hair_color=='Blonde'?'selected':''}}>Blonde</option>
                           <option value="Brown" {{$becomeMember->hair_color=='Brown'?'selected':''}}>Brown</option>
                           <option value="Ginger/Red" {{$becomeMember->hair_color=='Ginger/Red'?'selected':''}}>Ginger/Red</option>
                           <option value="Grey" {{$becomeMember->hair_color=='Grey'?'selected':''}}>Grey</option>
                           <option value="Light Brown" {{$becomeMember->hair_color=='Light Brown'?'selected':''}}>Light Brown</option>
                           <option value="White" {{$becomeMember->hair_color=='White'?'selected':''}}>White</option>
                       </select>
                    </div>
                    <div class="form-group label-floating is-select">
                        <label class="control-label">Height*</label>
                        <select class="selectpicker form-control" name="height">
                            <option value="1" {{$becomeMember->height=='1'?'selected':''}}>Below 5 feet</option>
                            <option value="2" {{$becomeMember->height=='2'?'selected':''}}>In Between 5 feet - 6 feet</option>
                            <option value="3" {{$becomeMember->height=='3'?'selected':''}}>Above 6 feet</option>
                       </select>
                    </div>
                    <div class="form-group label-floating is-select">
                        <label class="control-label">Weight*</label>
                        <select class="selectpicker form-control" name="weight">
                            <option value="1" {{$becomeMember->weight=='1'?'selected':''}}>Below 50 KG</option>
                            <option value="2" {{$becomeMember->weight=='2'?'selected':''}}>Between 50 KG - 70 KG</option>
                            <option value="3" {{$becomeMember->weight=='3'?'selected':''}}>Above 70 KG</option>
                       </select>
                    </div>
                    <div class="form-group label-floating is-select">
                        <label class="control-label">Blood Type</label>
                        <select class="selectpicker form-control" name="blood_type">
                            <option value="A+" {{$becomeMember->blood_type=='A+'?'selected':''}}>A+</option>
                            <option value="A-" {{$becomeMember->blood_type=='A-'?'selected':''}}>A-</option>
                            <option value="B+" {{$becomeMember->blood_type=='B+'?'selected':''}}>B+</option>
                            <option value="B-" {{$becomeMember->blood_type=='B-'?'selected':''}}>B-</option>
                            <option value="AB+" {{$becomeMember->blood_type=='AB+'?'selected':''}}>AB+</option>
                            <option value="AB-" {{$becomeMember->blood_type=='AB-'?'selected':''}}>AB-</option>
                            <option value="O+" {{$becomeMember->blood_type=='O+'?'selected':''}}>O+</option>
                            <option value="O-" {{$becomeMember->blood_type=='O-'?'selected':''}}>O-</option>
                       </select>
                    </div>
                    <div class="form-group label-floating is-select">
                        <label class="control-label">Have Passport</label>
                        <select class="selectpicker form-control" name="have_passport">
                        <option value="yes" {{$becomeMember->have_passport=='yes'?'selected':''}}>Yes</option>
                        <option value="no" {{$becomeMember->have_passport=='no'?'selected':''}}>No</option>
                    </select>
                    </div>
                    <div class="form-group label-floating is-select">
                        <label class="control-label">Willing To Travel</label>
                        <select class="selectpicker form-control" name="willing_to_travel">
                            <option value="yes" {{$becomeMember->willing_to_travel=='yes'?'selected':''}}>Yes</option>
                            <option value="No" {{$becomeMember->willing_to_travel=='No'?'selected':''}}>No</option>
                       </select>
                    </div>
                    <div class="form-group label-floating is-select">
                        <label class="control-label">Number of Children(non-donor)</label>
                        <select class="selectpicker form-control" name="number_of_children">
                            <option value="1" {{$becomeMember->number_of_children=='1'?'selected':''}}>1</option>
                            <option value="2" {{$becomeMember->number_of_children=='2'?'selected':''}}>More Than 1</option>
                            <option value="3" {{$becomeMember->number_of_children=='3'?'selected':''}}>More Than 3</option>
                            <option value="4" {{$becomeMember->number_of_children=='4'?'selected':''}}>More Than 5</option>
                       </select>
                    </div>
                    <div class="form-group label-floating is-select">
                        <label class="control-label">Number of Surrogate Children</label>
                        <select class="selectpicker form-control" name="number_of_surrogate_children">
                            <option value="1" {{$becomeMember->number_of_surrogate_children=='1'?'selected':''}}>1</option>
                            <option value="2" {{$becomeMember->number_of_surrogate_children=='2'?'selected':''}}>More Than 1</option>
                            <option value="3" {{$becomeMember->number_of_surrogate_children=='3'?'selected':''}}>More Than 3</option>
                            <option value="4" {{$becomeMember->number_of_surrogate_children=='4'?'selected':''}}>More Than 5</option>
                       </select>
                    </div>
                     
                    <div class="form-group label-floating is-select">
                        <label class="control-label">Surrogate Mother Types That You Agree To:*</label>
                        <div class="row">
                      <div class="col-md-6">
                        <div class="input-group">
                        <select class="selectpicker form-control jq_multiselect" multiple="multiple" name="surrogate_type[]">
                            <option value="Gestational">Gestational</option>
                            <option value="Traditional"> Traditional</option>
                       </select>
                     </div>
                   </div>
                   <div class="col-md-6">
               <div class="form-group">
                  <label class="control-label">Selected By User: {{$becomeMember->surrogate_type}}</label>
               </div>
            </div>
                    </div>
                  </div>
                    <div class="form-group label-floating is-select">
                        <label class="control-label">Have you been a @if($becomeMember->category=='surrogate-mothers')
                            surrogate mother
                        @endif
                          @if($becomeMember->category=='sperm-donors')
                            sperm donor
                        @endif
                          @if($becomeMember->category=='egg-donors')
                            egg donor
                        @endif
                         before?</label>
                        <select class="selectpicker form-control" name="given_service_before">
                            <option value="yes" {{$becomeMember->given_service_before=='yes'?'selected':''}}>Yes</option>
                            <option value="no" {{$becomeMember->given_service_before=='no'?'selected':''}}>No</option>
                       </select>
                    </div>
                    <div class="form-group label-floating is-select">
                        <label class="control-label">Marital Status</label>
                        <select class="selectpicker form-control" name="marital_status">
                            <option value="Divorced" {{$becomeMember->marital_status=='Divorced'?'selected':''}}>Divorced</option>
                            <option value="Married" {{$becomeMember->marital_status=='Married'?'selected':''}}>Married</option>
                            <option value="Separated" {{$becomeMember->marital_status=='Separated'?'selected':''}}>Separated</option>
                            <option value="Single (No partner)" {{$becomeMember->marital_status=='Single (No partner)'?'selected':''}}>Single (No partner)</option>
                            <option value="Single (With partner)" {{$becomeMember->marital_status=='Single (With partner)'?'selected':''}}>Single (With partner)</option>
                       </select>
                    </div>
                    <div class="form-group label-floating is-select">
                        <label class="control-label">Spoken Languages</label>
                        <div class="row">
                      <div class="col-md-6">
                        <div class="input-group">
                        <select class="form-control jq_multiselect" id="spoken_languages" enable_select_all="false" name="spoken_language[]" multiple="multiple" >
                              <option value="Abkhaz">Abkhaz</option>
                               <option value="Afrikaans">Afrikaans</option>
                               <option value="Albanian">Albanian</option>
                               <option value="Amharic">Amharic</option>
                               <option value="Arabic">Arabic</option>
                               <option value="Armenian">Armenian</option>
                               <option value="Assamese">Assamese</option>
                               <option value="Aymara">Aymara</option>
                               <option value="Bajan">Bajan</option>
                               <option value="Basque">Basque</option>
                               <option value="Belarusian">Belarusian</option>
                               <option value="Bengali">Bengali</option>
                               <option value="Bhojpuri">Bhojpuri</option>
                               <option value="Bislama">Bislama</option>
                               <option value="Bosnian">Bosnian</option>
                               <option value="Bulgarian">Bulgarian</option>
                               <option value="Burmese">Burmese</option>
                               <option value="Catalan">Catalan</option>
                               <option value="Chinese">Chinese</option>
                               <option value="Croatian">Croatian</option>
                               <option value="Czech">Czech</option>
                               <option value="Danish">Danish</option>
                               <option value="Dhivehi">Dhivehi</option>
                               <option value="Dutch">Dutch</option>
                               <option value="Dzongkha">Dzongkha</option>
                               <option value="English">English</option>
                               <option value="Estonian">Estonian</option>
                               <option value="Fijian">Fijian</option>
                               <option value="Filipino">Filipino</option>
                               <option value="Finnish">Finnish</option>
                               <option value="French">French</option>
                               <option value="Georgian">Georgian</option>
                               <option value="German">German</option>
                               <option value="Greek">Greek</option>
                               <option value="Guarani">Guarani</option>
                               <option value="Gujarati">Gujarati</option>
                               <option value="Haitian Creole">Haitian Creole</option>
                               <option value="Hebrew">Hebrew</option>
                               <option value="Hindi">Hindi</option>
                               <option value="Hiri Motu">Hiri Motu</option>
                               <option value="Hungarian">Hungarian</option>
                               <option value="Icelandic">Icelandic</option>
                               <option value="Indonesian">Indonesian</option>
                               <option value="Inuktitut">Inuktitut</option>
                               <option value="Italian">Italian</option>
                               <option value="Japanese">Japanese</option>
                               <option value="Kannada">Kannada</option>
                               <option value="Kashmiri">Kashmiri</option>
                               <option value="Kazakh">Kazakh</option>
                               <option value="Khmer">Khmer</option>
                               <option value="Korean">Korean</option>
                               <option value="Kurdish">Kurdish</option>
                               <option value="Lao">Lao</option>
                               <option value="Latvian">Latvian</option>
                               <option value="Lithuanian">Lithuanian</option>
                               <option value="Luxembourgish">Luxembourgish</option>
                               <option value="Macedonian">Macedonian</option>
                               <option value="Malagasy">Malagasy</option>
                               <option value="Malay">Malay</option>
                               <option value="Malayalam">Malayalam</option>
                               <option value="Maltese">Maltese</option>
                               <option value="Manx Gaelic">Manx Gaelic</option>
                               <option value="Maori">Maori</option>
                               <option value="Marathi">Marathi</option>
                               <option value="Mayan">Mayan</option>
                               <option value="Mongolian">Mongolian</option>
                               <option value="Nepali">Nepali</option>
                               <option value="Norwegian">Norwegian</option>
                               <option value="Occitan">Occitan</option>
                               <option value="Oriya">Oriya</option>
                               <option value="Ossetian">Ossetian</option>
                               <option value="Papiamento">Papiamento</option>
                               <option value="Pashto">Pashto</option>
                               <option value="Persian">Persian</option>
                               <option value="Polish">Polish</option>
                               <option value="Portuguese">Portuguese</option>
                               <option value="Punjabi">Punjabi</option>
                               <option value="Quechua">Quechua</option>
                               <option value="Rhaeto-Romansh">Rhaeto-Romansh</option>
                               <option value="Romanian">Romanian</option>
                               <option value="Russian">Russian</option>
                               <option value="Sanskrit">Sanskrit</option>
                               <option value="Serbian">Serbian</option>
                               <option value="Sindhi">Sindhi</option>
                               <option value="Sinhala">Sinhala</option>
                               <option value="Slovak">Slovak</option>
                               <option value="Slovene">Slovene</option>
                               <option value="Somali">Somali</option>
                               <option value="Sotho">Sotho</option>
                               <option value="Spanish">Spanish</option>
                               <option value="Swahili">Swahili</option>
                               <option value="Swedish">Swedish</option>
                               <option value="Tajik">Tajik</option>
                               <option value="Tamil">Tamil</option>
                               <option value="Telugu">Telugu</option>
                               <option value="Thai">Thai</option>
                               <option value="Tswana">Tswana</option>
                               <option value="Turkish">Turkish</option>
                               <option value="Turkmen">Turkmen</option>
                               <option value="Ukrainian">Ukrainian</option>
                               <option value="Urdu">Urdu</option>
                               <option value="Uzbek">Uzbek</option>
                               <option value="Venda">Venda</option>
                               <option value="Vietnamese">Vietnamese</option>
                               <option value="Welsh">Welsh</option>
                               <option value="Yiddish">Yiddish</option>
                               <option value="Zulu">Zulu</option>
                            </select>
                  </div>
                </div>
                <div class="col-md-6">
               <div class="form-group">
                  <label class="control-label">Selected By User: {{$becomeMember->spoken_language}}</label>
               </div>
            </div>
                    </div>
                  </div>
                    <div class="form-group label-floating is-select">
                        <label class="control-label">Willing To Assist Couple Types:*</label>
                         <div class="row">
                      <div class="col-md-6">
                        <div class="input-group">
                         <select class="selectpicker form-control spoken_language jq_multiselect" name="willing_to_assist[]" multiple="multiple">
                              <option value="Gay Couple">Gay Couple</option>
                              <option value="Heterosexual Couple">Heterosexual Couple</option>
                              <option value="Lesbian Couple">Lesbian Couple</option>
                              <option value="Single Man">Single Man</option>
                              <option value="Single Woman">Single Woman</option>
                           </select>
                      </div>
                    </div>
                     <div class="col-md-6">
               <div class="form-group">
                  <label class="control-label">Selected By User: {{$becomeMember->willing_to_assist}}</label>
               </div>
                  </div>
                    </div>
                    <div class="form-group label-floating">
                        <label class="control-label">How would you best describe yourself?</label>
                        <textarea class="form-control" name="describe_yourself" >{{$becomeMember->describe_yourself}}</textarea>
                    </div>
                    <div class="form-group label-floating">
                        <label class="control-label">Why do you want to be a 
                        @if($becomeMember->category=='surrogate-mothers')
                            surrogate mother
                        @endif
                          @if($becomeMember->category=='sperm-donors')
                            sperm donor
                        @endif
                          @if($becomeMember->category=='egg-donors')
                            egg donor
                        @endif
                        ?*</label>
                        <textarea class="form-control" name="why_become_member" >{{$becomeMember->why_become_member}}</textarea>
                    </div>
                    <div class="form-group label-floating">
                        <label class="control-label">Use this space to communicate a letter to potential intended parents.</label>
                        <textarea class="form-control" name="communication_letter">{{$becomeMember->communication_letter}}</textarea>
                    </div>
                    <div class="form-group label-floating">
                        
                        <img src="{{asset('storage/'.$becomeMember->picture)}}"   height="100">
                    </div>
                    <div class="form-group label-floating">
                        <label class="control-label">Select Another Photo to Update</label>
                        <input class="form-control" type="file" name="picture" placeholder="">
                    </div>
                        
                    </div>
                <button type="submit" class="btn btn-lg border-2  btn-primary btn-block border-radius-15 padding-15px box-shadow">Update</button>
            </form>
                
@endsection
@section('additional-js')
<script type="text/javascript" src="{{asset('assets/assets/bootstrap-datepicker.js')}}"></script>
<script type="text/javascript" src="{{asset('assets/assets/bootstrap-datepicker.init.js')}}"></script>
<script type="text/javascript" src="{{asset('assets/assets/signup.js')}}"></script>
<script type="text/javascript" src="{{asset('assets/assets/bootstrap-multiselect.js')}}"></script>
<link rel="stylesheet" type="text/css" href="{{asset('assets/assets/datepicker3.css')}}">
<link rel="stylesheet" type="text/css" href="{{asset('assets/assets/bootstrap-multiselect.css')}}">
<link rel="stylesheet" type="text/css" href="{{asset('assets/assets/member_generic.css')}}">
<script src='https://www.google.com/recaptcha/api.js'></script>
@endsection