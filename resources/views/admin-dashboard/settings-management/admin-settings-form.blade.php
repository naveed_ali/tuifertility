@extends('admin-dashboard.layouts.master')
@section('additional-head')
@endsection
@section('content')
<!-- Page Title -->
                <div id="page-title" class="padding-30px background-white full-width">
                    <div class="container">
                        <ol class="breadcrumb opacity-5">
                            <li><a href="#">Home</a></li>
                            <li><a href="#">Admin Settings</a></li>
                        </ol>
                        <h1 class="font-weight-300">Admin Settings</h1>
                    </div>
                </div>
                <!-- // Page Title -->

                <div class="margin-tb-45px full-width">
                    <div class="padding-30px background-white border-radius-20 box-shadow">
                        <h3><i class="far fa-list-alt margin-right-10px text-main-color"></i> Admin settings Information </h3>
                        <hr>
                        <form method="post" action="{{route('settings.update',$setting->id)}}">
                            @method('put')
                        	@csrf
                            <div class="form-group margin-bottom-20px">
                                <label><i class="far fa-user fa-lg margin-right-10px"></i>Email</label>
                                <input type="email" class="form-control form-control-sm" name="admin_email" id="admin_email" value="{{$setting->admin_email}}">
                            </div>
                            <div class="form-group margin-bottom-20px">
                                <label><i class="far fa fa-globe fa-lg margin-right-10px"></i> Site Name</label>
                                <input type="text" class="form-control form-control-sm" name="site_name" id="site_name" value="{{$setting->site_name}}">
                            </div>
                            <div class="row">
                                <div class="col-md-12 form-group margin-bottom-20px">
                                    <label><i class="far fa-file fa-lg  margin-right-10px"></i>Meta Description</label>
                                    <textarea name="meta_description" class="form-control">{{$setting->meta_description}}</textarea>
                                </div>
                            </div>
                            <div class="row">
                                <div class="col-md-12 form-group margin-bottom-20px">
                                    <label><i class="fa fa-tag fa-lg margin-right-10px"></i>Meta Keywords</label>
                                    <textarea name="meta_keyword" class="form-control">{{$setting->meta_keyword}}</textarea>
                                </div>
                            </div>
                    </div>
                </div>
                <button type="submit" class="btn btn-lg border-2  btn-primary btn-block border-radius-15 padding-15px box-shadow">Update Settings</button>
                </form>
@endsection
@section('additional-js')

@endsection