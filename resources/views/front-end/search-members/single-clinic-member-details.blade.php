@extends('front-end.layouts.master')

@section('content')
<section class="banner padding-top-170px padding-bottom-30px tablet-padding-p-top sm-ptb-30px search_page_bg" style="background-image: url({{asset('assets/img/banner_1.jpg')}});">
<div id="page-title" class="padding-tb-30px">
        <div class="container">
            <ol class="breadcrumb opacity-5">
                <li><a href="#">Home</a></li>
                <li><a href="{{url('search/fertility-clinics')}}">Fertility Clinics</a></li>
                <li class="active">{{$fertilityClinicMember->clinic_name}}</li>
            </ol>
        </div>
    </div>


    <div class="margin-tb-0px">
        <div class="container">
            <div class="row">
              <div class="col-lg-12">
                <div class="margin-bottom-30px box-shadow">
                  <section id="homeblog" style="height: auto !important; margin-top: 5px;"> 
                     <div class="blogpo" style="height: auto !important;">
                        <article class="blogelem">
                           <div class="row"> 
                              <div class="col-6 col-sm-6 col-md-3 col-lg-2 imgcont margin-bottom-10px">
                                 @php $pictures[] = explode(',',$fertilityClinicMember->pictures);
                                 @endphp
                                 <a href="#">
                                 <img style="" src="{{asset('storage/'.$pictures[0][0])}}" class="img-responsive img-float clip" alt="American Surrogate Mother">
                                 </a>
                              </div>
                              <div class="col-sm-12 col-md-9 col-lg-10">
                                 <div class="postdata">
                                    <div class="ptitle">
                                      @php
                                      $first_name = explode(' ',$fertilityClinicMember->name);
                                     @endphp
                                       <a href="#">{{$first_name[0]}}</a><br>
                                       {{$fertilityClinicMember->country}}'s Fertility Clinic 
                                    </div>
                                    <div class="bauthor">
                                       <table class="table">
                                          <tbody>
                                             <tr>
                                                <td width="15%"><b style="margin-left: 10px;">Name:</b></td>
                                                <td width="85%">{{$first_name[0]}}</td>
                                             </tr>
                                             <!-- <tr>
                                                <td width="15%"><b>Member Type:</b></td>
                                                <td width="85%">Fertility Clinic</td>
                                             </tr> -->
                                             <tr>
                                                <td width="15%"><b style="margin-left: 10px;">Country:</b></td>
                                                <td width="85%">{{$fertilityClinicMember->country}}</td>
                                             </tr>
                                             <!-- <tr>
                                                <td width="15%" style="padding-left: 12px;"><b>Date Registered:</b></td>
                                                <td width="85%">{{$fertilityClinicMember->created_at->diffForHumans()}} </td>
                                             </tr> -->
                                          </tbody>
                                       </table>
                                    </div>
                                 </div>
                              </div>
                           </div>
                        </article>
                     </div>
                  </section>
               </div>
               <div class="margin-bottom-30px box-shadow" style="font-family: Raleway;">
                  <div class="padding-30px" style="box-shadow: 0 0 20px #7e7b7b;">
                    <ul class="nav nav-tabs" id="myTab" role="tablist">
                        <li class="nav-item">
                           <a class="nav-link active" id="about-me-tab" data-toggle="tab" href="#about-me" role="tab" aria-controls="about-me" aria-selected="true">About Me</a>
                        </li>
                        @if($fertilityClinicMember->pictures)
                        <li class="nav-item">
                           <a class="nav-link" id="pictures-tab" data-toggle="tab" href="#pictures" role="tab" aria-controls="pictures" aria-selected="false">Pictures</a>
                        </li>
                        @endif
                        @if($fertilityClinicMember->video_url)
                        <li class="nav-item">
                           <a class="nav-link" id="video-tab" data-toggle="tab" href="#video" role="tab" aria-controls="video" aria-selected="false">Videos</a>
                        </li>
                        @endif
                        @if($fertilityClinicMember->review)
                        <li class="nav-item">
                           <a class="nav-link" id="reviews-tab" data-toggle="tab" href="#reviews" role="tab" aria-controls="reviews" aria-selected="false">Reviews</a>
                        </li>
                        @endif
                     </ul>
                     <div class="tab-content" id="myTabContent">
                        <div class="tab-pane fade show active" id="about-me" role="tabpanel" aria-labelledby="about-me-tab">
                          <div class="row margin-top-20px">
                            <div class="col-8 col-md-4">
                              <b>Practice/Clinic Name: </b>
                            </div>
                            <div class="col-4 col-md-8">
                              {{$fertilityClinicMember->clinic_name}}
                            </div>
                           </div>
                           <hr>
                           <div class="row">
                            <div class="col-8 col-md-4">
                              
                                <b>We Assist the Following Groups: </b>
                              
                            </div>
                            <div class="col-4 col-md-8">
                              {{$fertilityClinicMember->provide_services_for}}
                            </div>
                           </div>
                           <hr>
                           <div class="row">
                            <div class="col-8 col-md-4">
                              
                                <b>Willing to Assist Couples Types of: </b>
                              
                            </div>
                            <div class="col-4 col-md-8">
                              {{$fertilityClinicMember->willing_to_assist}}
                            </div>
                           </div>
                           <hr>
                           <div class="row">
                            <div class="col-8 col-md-4">
                              
                                <b>We Offer the Following Treatments: </b>
                              
                            </div>
                            <div class="col-4 col-md-8">
                              {{$fertilityClinicMember->treatments}}
                            </div>
                           </div>
                           <hr>
                           <div class="row">
                            <div class="col-8 col-md-4">
                              
                                <b>We Provide the Following Services: </b>
                              
                            </div>
                            <div class="col-4 col-md-8">
                              {{$fertilityClinicMember->providing_service}}
                            </div>
                           </div>
                           <hr>
                           <div class="row">
                            <div class="col-8 col-md-4">
                              
                                <b>Willing to Assist Surrogates/Donors/Parents from these Countries: </b>
                              
                            </div>
                            <div class="col-4 col-md-8">
                              {{$fertilityClinicMember->willing_to_assist_countries}}
                            </div>
                           </div>
                        </div>
                        <div class="tab-pane fade" id="pictures" role="tabpanel" aria-labelledby="pictures-tab">
                           <div class="row margin-top-30px">
                              @foreach(explode(',',$fertilityClinicMember->pictures) as $picture)
                              <div class="col-md-6 margin-top-10px">
                                 <img src="{{asset('storage/'.$picture)}}"  data-toggle="modal" data-target="#exampleModalCenter" class="img-fluid" alt="Responsive image">
                              </div>
                              @endforeach
                              
                           </div>
                        </div>
                        <div class="tab-pane fade" id="video" role="tabpanel" aria-labelledby="video-tab">
                           <div class="row margin-top-30px">
                              <div class="embed-responsive embed-responsive-16by9">
                                <iframe class="embed-responsive-item" src="{{$fertilityClinicMember->video_url}}" allowfullscreen></iframe>
                              </div>
                           </div>
                        </div>
                        <div class="tab-pane fade" id="reviews" role="tabpanel" aria-labelledby="reviews-tab">
                          <div class="row margin-top-15px" >
                             <div class="col-lg-12">
                                 <div class="rating clearfix">
                                    <ul class="float-left">
                                       @if($fertilityClinicMember->clinic_review)
                                       @if($fertilityClinicMember->clinic_review->stars==0)
                                       <li></li>
                                       <li></li>
                                       <li></li>
                                       <li></li>
                                       <li></li>
                                       @elseif($fertilityClinicMember->clinic_review->stars==1)
                                       <li class="active"></li>
                                       <li></li>
                                       <li></li>
                                       <li></li>
                                       <li></li>
                                       @elseif($fertilityClinicMember->clinic_review->stars==2)
                                       <li class="active"></li>
                                       <li class="active"></li>
                                       <li></li>
                                       <li></li>
                                       <li></li>
                                       @elseif($fertilityClinicMember->clinic_review->stars==3)
                                       <li class="active"></li>
                                       <li class="active"></li>
                                       <li class="active"></li>
                                       <li></li>
                                       <li></li>
                                       @elseif($fertilityClinicMember->clinic_review->stars==4)
                                       <li class="active"></li>
                                       <li class="active"></li>
                                       <li class="active"></li>
                                       <li class="active"></li>
                                       <li></li>
                                       @elseif($fertilityClinicMember->clinic_review->stars==5)
                                       <li class="active"></li>
                                       <li class="active"></li>
                                       <li class="active"></li>
                                       <li class="active"></li>
                                       <li class="active"></li>
                                       @endif
                                      @else
                                      <li></li>
                                       <li></li>
                                       <li></li>
                                       <li></li>
                                       <li></li>
                                      @endif
                                    </ul>
                                 </div>
                             </div>
                             <div class="col-lg-12">
                                 @if($fertilityClinicMember->clinic_review)
                                <p>{!!$fertilityClinicMember->clinic_review->review!!}</p>
                                @else
                                <p class="margin-top-15px">There are no reviews yet</p>
                                @endif
                             </div>
                           </div>
                        </div>
                      </div>
                  </div>
                </div>
              </div>
            </div>
            <div class="row">
              <div class="col-sm-12 col-md-12 col-lg-12 margin-bottom-30px">
                <a href="{{route('intendent.parent.form','fertility-clinics')}}">
                  <button class="search_btn btn btn-block" type="button">
                    Apply to use this Clinic
                  </button>
                </a>
              </div>
         </div>
        </div>
    </div>
</section>
<!-- Button trigger modal -->
<!-- <button type="button" class="btn btn-primary" data-toggle="modal" data-target="#exampleModalCenter">
  Launch demo modal
</button> -->

<!-- Modal -->

@endsection
@section('pop-up-modal')
<div class="modal fade" id="exampleModalCenter" tabindex="-1" role="dialog" aria-labelledby="exampleModalCenterTitle" aria-hidden="true">
  <div class="modal-dialog modal-dialog-centered modal-lg" role="document">
    <div class="modal-content">
      <div class="modal-header">
        <h5 class="modal-title" id="exampleModalLongTitle">Image Gallery</h5>
        <button type="button" class="close" data-dismiss="modal" aria-label="Close">
          <span aria-hidden="true">&times;</span>
        </button>
      </div>
      <div class="modal-body">
        <div id="carouselExampleControls" class="carousel slide" data-ride="carousel">
          <div class="carousel-inner">
              @php $multiple_pictures = explode(',',$fertilityClinicMember->pictures);
              
              @endphp
            @foreach($multiple_pictures as $picture)

            <div class="carousel-item  {{$multiple_pictures[0]==$picture ? 'active' :''}}">
              <img class="d-block w-100" src="{{asset('storage/'.$picture)}}" alt="First slide">
            </div>
            @endforeach
          </div>
          <a class="carousel-control-prev" href="#carouselExampleControls" role="button" data-slide="prev">
            <span class="carousel-control-prev-icon" aria-hidden="true"></span>
            <span class="sr-only">Previous</span>
          </a>
          <a class="carousel-control-next" href="#carouselExampleControls" role="button" data-slide="next">
            <span class="carousel-control-next-icon" aria-hidden="true"></span>
            <span class="sr-only">Next</span>
          </a>
        </div>
      </div>
      <div class="modal-footer">
        <button type="button" class="btn btn-secondary" data-dismiss="modal">Close</button>
      </div>
    </div>
  </div>
</div>
@endsection