@extends('front-end.layouts.master')
@section('additional-head')
<style type="text/css">
	.seeall {
    border: 1px solid #007cff;
}
.actionbut {
    margin: 15px 0;
}
.pull-right {
    float: right !important;
}
</style>
@endsection
@section('content')
<section class="banner padding-top-170px padding-bottom-30px tablet-padding-top sm-ptb-0px search_page_bg" style="background-image: url({{asset('assets/img/banner_1.jpg')}});">
 <div id="page-title" class="margin-mob">
        <div class="container">
           <h2 class="result margin-b-30px" style="font-size: 25px;">Fertility Clinics</h2>
            <form action="{{route('search.fertility-clicnic.result','fertility-clinics')}}" class="search-form" method="get">
               <input type="hidden" name="category" value="{{$category}}">
               <div class="row">
                  <div class="col-sm-4 col-md-3">
                     <div class="form-group">
                        <label for="country_id">Country</label>
                        <select class="selectpicker form-control" name="country">
                          <option value="">Please Select</option>
                          @foreach($countries as $country)
                          <option value="{{$country->country_name}}">{{$country->country_name}}</option>
                          @endforeach
                        </select>
                     </div>
                  </div>
                  <div class="col-sm-4 col-md-3">
                    <div class="form-group label-floating is-select">
                        <label class="control-label">Provides Service For:*</label>
                        <div class="input-group">
                          <select class="selectpicker form-control" name="provide_services_for">
                            <option value="">Please Select</option>
                            <option value="Surrogate Mothers">Surrogate Mothers</option>
                            <option value="Intended Parents">Intended Parents</option>
                            <option value="Egg Donors">Egg Donors</option>
                            <option value="Sperm Donors">Sperm Donors</option>
                          </select>
                        </div>
                    </div>
                  </div>
                  <div class="col-sm-4 col-md-3">
                    <div class="form-group label-floating is-select">
                      <label class="control-label">Treatments Offered:*</label>
                      <div class="input-group">
                        <select class="selectpicker form-control" name="treatments">
                          <option value="">Please Select</option>
                          <option value="AEC (autologous endometrial coculture)">AEC (autologous endometrial coculture)</option>
                           <option value="Egg Donation">Egg Donation</option>
                           <option value="Egg Freezing">Egg Freezing</option>
                           <option value="Embryo Adoption">Embryo Adoption</option>
                           <option value="Embryo Donation">Embryo Donation</option>
                           <option value="Embryo Freezing">Embryo Freezing</option>
                           <option value="Frozen Embryo Transfer Cycle">Frozen Embryo Transfer Cycle</option>
                           <option value="Gender Selection">Gender Selection</option>
                           <option value="ICSI (intracytoplasmic sperm injection)">ICSI (intracytoplasmic sperm injection)</option>
                           <option value="IMSI">IMSI</option>
                           <option value="IUI (intrauterine insemination)">IUI (intrauterine insemination)</option>
                           <option value="IVF (in vitro fertilization)">IVF (in vitro fertilization)</option>
                           <option value="IVM (in vitro maturation)">IVM (in vitro maturation)</option>
                           <option value="Microdissection TESE/TESA">Microdissection TESE/TESA</option>
                           <option value="MicroSort (gender selection)">MicroSort (gender selection)</option>
                           <option value="Mini IVF">Mini IVF</option>
                           <option value="Natural Cycle IVF">Natural Cycle IVF</option>
                           <option value="PGD (pre-implantation genetic diagnosis)">PGD (pre-implantation genetic diagnosis)</option>
                           <option value="Pregnancy Care">Pregnancy Care</option>
                           <option value="Sperm Donation">Sperm Donation</option>
                           <option value="Sperm Freezing">Sperm Freezing</option>
                           <option value="Surrogacy">Surrogacy</option>
                           <option value="Tandem IVF Cycle">Tandem IVF Cycle</option>
                           <option value="TESA">TESA</option>
                           <option value="Vasectomy Reversal">Vasectomy Reversal</option>
                        </select>
                      </div>
                    </div>
                  </div>
                  <div class="col-sm-4 col-md-3">
                                    <div class="form-group label-floating is-select">
                                       <label class="control-label">Service Types</label>
                                       <div class="input-group">
                                       <select class="selectpicker form-control" name="providing_service">
                                        <option value="">Please Select</option>
                             <option value="Crisis Support">Crisis Support</option>
                             <option value="Egg Donor Coordinators">Egg Donor Coordinators</option>
                             <option value="Egg Donor Matching">Egg Donor Matching</option>
                             <option value="Fertility Doctors">Fertility Doctors</option>
                             <option value="Financial Assistance">Financial Assistance</option>
                             <option value="Journey Coordinators">Journey Coordinators</option>
                             <option value="Legal Support/Lawyers">Legal Support/Lawyers</option>
                             <option value="Medical Insurance Assistance">Medical Insurance Assistance</option>
                             <option value="Midwives/Birthing Team">Midwives/Birthing Team</option>
                             <option value="Nursing Staff">Nursing Staff</option>
                             <option value="Psychologists Evaluations">Psychologists Evaluations</option>
                             <option value="Psychotherapists Evaluations">Psychotherapists Evaluations</option>
                             <option value="Screening &amp; Matching Team">Screening &amp; Matching Team</option>
                             <option value="Social Workers">Social Workers</option>
                             <option value="Sperm Donor Matching">Sperm Donor Matching</option>
                             <option value="Surrogacy Applications and Contracts">Surrogacy Applications and Contracts</option>
                             <option value="Surrogate Care Assistance">Surrogate Care Assistance</option>
                             <option value="Surrogate Mother Matching">Surrogate Mother Matching</option>
                                       </select>
                                     </div>
                                    </div>
                                 </div>
                  <div class="col-sm-4 col-md-3">
                     <div class="form-group">
                        <label for="intended_parent_type_id">Willing to Assist</label>
                         <select class="selectpicker form-control"  name="willing_to_assist">
                            <option value="">Please Select</option>
                            <option value="Gay Couple">Gay Couple</option>
                            <option value="Heterosexual Couple">Heterosexual Couple</option>
                            <option value="Lesbian Couple">Lesbian Couple</option>
                            <option value="Single Man">Single Man</option>
                            <option value="Single Woman">Single Woman</option>
                          </select>
                     </div>
                  </div>
                  <div class="col-4 col-sm-3 col-md-4 col-lg-2 offset-8 offset-lg-10 offset-md-8 offset-sm-9" style="text-align: right;">
                              <button id="submit" class="search_btn searchbut btn btn-block" name="submit" type="submit">SEARCH</button>
                            </div>
               </div>
               
            </form>
        </div>
    </div>


    <div class="margin-tb-30px">
        <div class="container">
            <div style="margin-bottom: 24px; margin-top: 30px;">
      <ol class="breadcrumb">
      <li>
      <a href="{{url('/')}}">Home</a>
      </li>
      <li><a href="{{url('search/fertility-clinics')}}">Fertility Clinics</a></li>
      <ol>
      </ol></ol>
    </div>
    <div class="row">
      <div class="col-md-12">
        <span class="result" style="font-size: 25px;">
          Fertility Clinics
          <span>{{$members->count()}}</span></span>
      </div>
    </div>
            <div class="row">
              @foreach($members as $member)
              <div class="col-md-12 col-lg-12">
                  <section id="homeblog" style="height: auto !important; margin-top: 28px;">
                    <div class="blogpo" style="height: auto !important;">
                      <article class="blogelem">
                         <div class="row"><!-- imgcont   -->
                            <div class="col-sm-12 col-md-3 col-lg-2 padding-bottom-10px">
                              <div class="row">
                                @if($member->review)
                                <div class="col-sm-12 col-md-12 col-lg-12" style="margin-bottom: 5px;">
                                  <div class="rating rating-float clearfix">
                                    <span style="font-weight: 600">Rating</span>
                                    <ul>
                                       
                                       @if($member->review->stars==0)
                                       <li></li>
                                       <li></li>
                                       <li></li>
                                       <li></li>
                                       <li></li>
                                       @elseif($member->review->stars==1)
                                       <li class="active"></li>
                                       <li></li>
                                       <li></li>
                                       <li></li>
                                       <li></li>
                                       @elseif($member->review->stars==2)
                                       <li class="active"></li>
                                       <li class="active"></li>
                                       <li></li>
                                       <li></li>
                                       <li></li>
                                       @elseif($member->review->stars==3)
                                       <li class="active"></li>
                                       <li class="active"></li>
                                       <li class="active"></li>
                                       <li></li>
                                       <li></li>
                                       @elseif($member->review->stars==4)
                                       <li class="active"></li>
                                       <li class="active"></li>
                                       <li class="active"></li>
                                       <li class="active"></li>
                                       <li></li>
                                       @elseif($member->review->stars==5)
                                       <li class="active"></li>
                                       <li class="active"></li>
                                       <li class="active"></li>
                                       <li class="active"></li>
                                       <li class="active"></li>
                                       @endif
                                      
                                    </ul>
                                  </div>
                                </div>
                                @endif
                                <div class="col-6 col-sm-6 col-md-12 col-lg-12 imgcont margin-bottom-10px">
                                  <a href="#">
                                    @php
                                      $pictures = explode(',',$member->pictures);
                                     @endphp
                                    <img src="{{asset('storage/'.$pictures[0])}}" class="img-responsive profimg clip" alt="American Surrogate ">
                                   </a>
                                </div>
                              </div>
                               
                            </div>
                            <div class="col-sm-12 col-md-9 col-lg-10">
                              <div class="row">
                                <div class="col-sm-12 col-md-12 col-lg-12 ptitle">
                                  <a href="/surrogate-mothers/53447">
                                    @php
                                      $first_name = explode(' ',$member->name);
                                     @endphp
                                   {{$first_name[0]}}</a>
                                  <br>
                                  {{$member->country}}'s Fertility Clinic  
                                </div>
                                <div class="col-sm-12 col-md-12 col-lg-12">
                                  
                                </div>
                                <div class="col-sm-12 col-md-12 col-lg-12 bauthor">
                                  <table class="table">
                                    <tbody>
                                       <tr>
                                          <td width="15%"><b style="margin-left: 10px;">Location:</b></td>
                                          <td width="85%">{{$member->country}}</td>
                                       </tr>
                                       <tr>
                                          <td width="15%" style="padding-left: 12px;"><b>Willing to Assist Couples Types of:</b></td>
                                          <td width="85%">{{$member->willing_to_assist}}</td>
                                        </tr>
                                    </tbody>
                                  </table>
                                </div>
                                <div class="col-sm-12 col-md-12 col-lg-12">
                                  <span class="excerpt">
                                    <div class="text" style="line-height: 20px; margin-left: 12px;">
                                      {{$member->describe_clinic}}
                                    </div>
                                    <div class="clearfix"></div>
                                  </span>
                                </div>
                                <div class="col-5 col-sm-3 col-md-4 col-lg-2 offset-7 offset-lg-10 offset-md-8 offset-sm-9 padding-top-20px">
                                  <a href="{{route('single.clinic.member',$member->id)}}">
                                    <button class="search_btn btn btn-block" type="button">See Profile</button>
                                  </a>
                                </div>
                              </div>
                            </div>
                         </div>
                      </article>
                    </div>
                  </section>
                  <ul class="pagination pagination-md ">
                     
                  </ul>
              </div>
              @endforeach
            </div>
        </div>
    </div>
  </section>
@endsection
@section('additional-js')

@endsection