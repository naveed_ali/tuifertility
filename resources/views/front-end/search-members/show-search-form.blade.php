@extends('front-end.layouts.master')
@section('additional-head')
<style type="text/css">
	.seeall {
    border: 1px solid #007cff;
  }
  .pull-right {
      float: right !important;
  }
  .seeall {
      padding: 10px 20px;
  }
  
  /*.crop {
   height: 300px;
   width: 400px;
   overflow: hidden;
  }
  .crop img {
   height: auto;
   width: 400px;
  }*/
</style>
@endsection
@section('content')
<section class="banner padding-top-170px padding-bottom-30px tablet-padding-top sm-ptb-30px search_page_bg" style="background-image: url({{asset('assets/img/banner_1.jpg')}}); ">
  <div id="page-title" class="margin-mob">
    <div class="container">
      <h2 class="result margin-b-30px" style="font-size: 25px;">
       @if($category=='surrogate-mothers')
        Surrogate Mothers
        @elseif($category=='egg-donors')
        Egg Donors
        @elseif($category=='sperm-donors')
        Sperm Donors
        @endif
      </h2>
      <form action="{{route('search.result')}}" class="search-form" method="get">
         <input type="hidden" name="category" value="{{$category}}">
         <div class="row">
            <div class="col-sm-4 col-md-3">
               <div class="form-group">
                  <label for="country_id">Country</label>
                  <select class="selectpicker form-control" name="country">
                    <option value="">Please Select</option>
                    @foreach($countries as $country)
                    <option value="{{$country->country_name}}">{{$country->country_name}}</option>
                    @endforeach
                  </select>
               </div>
            </div>
            <div class="col-sm-4 col-md-3">
               <div class="form-group">
                  <label for="intended_parent_type_id">Willing to Assist</label>
                   <select class="selectpicker form-control"  name="willing_to_assist">
                      <option value="">Please Select</option>
                      <option value="Gay Couple">Gay Couple</option>
                      <option value="Heterosexual Couple">Heterosexual Couple</option>
                      <option value="Lesbian Couple">Lesbian Couple</option>
                      <option value="Single Man">Single Man</option>
                      <option value="Single Woman">Single Woman</option>
                    </select>
               </div>
            </div>
            <div class="col-sm-4 col-md-3">
               <div class="form-group">
                  <label for="race_id">Race</label>
                  <select class="selectpicker form-control" name="race">
                    <option value="">Please Select</option>
                    <option value="African">African</option>
                    <option value="American-Indian">American Indian</option>
                    <option value="Asian (Chinese)">Asian (Chinese)</option>
                    <option value="Asian (Japanese)">Asian (Japanese)</option>
                    <option value="Asian (other)">Asian (other)</option>
                    <option value="Caucasian">Caucasian</option>
                    <option value="Hispanic">Hispanic</option>
                    <option value="Middle Eastern">Middle Eastern</option>
                    <option value="Pacific Islander">Pacific Islander</option>
                    <option value="Other">Other</option>
                  </select>
               </div>
            </div>
            <div class="col-sm-4 col-md-3">
               <div class="form-group">
                  <label for="religion_id">Religion</label>
                  <select class="selectpicker form-control" name="religion">
                    <option value="">Please Select</option>
                    <option value="African Traditional &amp; Diasporic">African Traditional &amp; Diasporic</option>
                    <option value="Agnostic">Agnostic</option>
                    <option value="Atheist">Atheist</option>
                    <option value="Baha'i">Baha'i</option>
                    <option value="Buddhism">Buddhism</option>
                    <option value="Cao Dai">Cao Dai</option>
                    <option value="Chinese traditional religion">Chinese traditional religion</option>
                    <option value="Christianity">Christianity</option>
                    <option value="Hinduism">Hinduism</option>
                    <option value="Islam">Islam</option>
                    <option value="Jainism">Jainism</option>
                    <option value="Juche">Juche</option>
                    <option value="Judaism">Judaism</option>
                    <option value="Neo-Paganism">Neo-Paganism</option>
                    <option value="Nonreligious">Nonreligious</option>
                    <option value="Other">Other</option>
                    <option value="primal-indigenous">primal-indigenous</option>
                    <option value="Rastafarianism">Rastafarianism</option>
                    <option value="Secular">Secular</option>
                    <option value="Shinto">Shinto</option>
                    <option value="Sikhism">Sikhism</option>
                    <option value="Spiritism">Spiritism</option>
                    <option value="Tenrikyo">Tenrikyo</option>
                    <option value="Unitarian-Universalism">Unitarian-Universalism</option>
                    <option value="Zoroastrianism">Zoroastrianism</option>
                 </select>
               </div>
            </div>
            <div class="col-sm-4 col-md-3">
               <div class="form-group">
                  <label for="spoken_languages">Spoken Language</label>
                  <select class="selectpicker form-control" name="spoken_language">
                      <option value="">Please Select</option>

                    @foreach($languages as $language)
                    <option value="{{$language->name}}">{{$language->name}}</option>
                    @endforeach
                  </select>
               </div>
            </div>
            <div class="col-sm-4 col-md-3">
               <div class="form-group">
                  <label for="education_id">Education</label>
                  <select class="selectpicker form-control" name="education">
                    <option value="">Please Select</option>
                    <option value="Never Finished High School">Never Finished High School</option>
                    <option value="Completed High School">Completed High School</option>
                    <option value="Completed College or University">Completed College or University</option>
                    <option value="Completed College or University">Completed Master Degree</option>
                  </select>
               </div>
            </div>
            <div class="col-sm-4 col-md-3">
               <div class="form-group">
                  <label for="smoker">Smoker</label>
                  <select class="selectpicker form-control" name="smoker">
                    <option value="">Please Select</option>
                    <option value="0">Yes</option>
                    <option value="1">No</option>
                  </select>
               </div>
            </div>
            <div class="col-sm-4 col-md-3">
               <div class="form-group">
                  <label for="hair_color_id">Hair Color</label>
                  <select class="selectpicker form-control" name="hair_color">
                    <option value="">Please Select</option>
                    <option value="Auburn">Auburn</option>
                    <option value="Black">Black</option>
                    <option value="Blonde">Blonde</option>
                    <option value="Brown">Brown</option>
                    <option value="Ginger/Red">Ginger/Red</option>
                    <option value="Grey">Grey</option>
                    <option value="Light Brown">Light Brown</option>
                    <option value="White">White</option>
                 </select>
               </div>
            </div>
            <div class="col-sm-4 col-md-3">
               <div class="form-group">
                  <label for="eye_color_id">Eye Color</label>
                  <select class="selectpicker form-control" name="eye_color">
                    <option value="">Please Select</option>
                    <option value="Amber">Amber</option>
                    <option value="Blue">Blue</option>
                    <option value="Brown">Brown</option>
                    <option value="Green">Green</option>
                    <option value="Grey">Grey</option>
                    <option value="Hazel">Hazel</option>
                 </select>
               </div>
            </div>
            <div class="col-sm-4 col-md-3">
               <div class="form-group">
                  <label for="marital_status_id">Marital Status</label>
                  <select class="selectpicker form-control" name="marital_status">
                    <option value="">Please Select</option>
                    <option value="Divorced">Divorced</option>
                    <option value="Married">Married</option>
                    <option value="Separated">Separated</option>
                    <option value="Single (No partner)">Single (No partner)</option>
                    <option value="Single (With partner)">Single (With partner)</option>
                 </select>
               </div>
            </div><!-- searchbut -->
            <div class="col-4 col-sm-3 col-md-4 col-lg-2 offset-8 offset-lg-10 offset-md-8 offset-sm-9" style="text-align: right;">
              <button id="submit" class="search_btn searchbut btn btn-block" name="submit" type="submit">SEARCH</button>
            </div>
         </div>

      </form>
     
    </div>
  </div>

    <div class="">
        <div class="container">
          <!-- breadcrumb margin-bottom-30px m-top-breadcrump -->
          <div class="ol-breadcrumb">
            <ol class="breadcrumb">
            <li>
            <a href="{{url('/')}}">Home</a>
            </li>
            <li>
              @if($category=='surrogate-mothers')
                <a href="{{url('category/search/surrogate-mothers')}}">Surrogate Mothers</a>
              @elseif($category=='egg-donors')
                <a href="{{url('category/search/egg-donors')}}">Egg Donors</a>
              @elseif($category=='sperm-donors')
                <a href="{{url('category/search/sperm-donors')}}">Sperm Donors</a>
              @endif
            </li>
            <ol>
            </ol></ol>
          </div>
          <!-- Search Title -->
          <div class="row">
            <div class="col-md-12">
              <span class="result" style="font-size: 25px;">

                @if($category=='surrogate-mothers')
                Surrogate Mothers
                @elseif($category=='egg-donors')
                Egg Donors
                @elseif($category=='sperm-donors')
                Sperm Donors
                @endif
                <span>{{$members->count()}}</span></span>
            </div>
          </div>
          <!-- Members Div -->
          <div class="row">
            @foreach($members as $member)
              <div class="col-md-12 col-lg-12">
                  <section id="homeblog" style="height: auto !important; margin-top: 28px;">
                    <div class="blogpo" style="height: auto !important;">
                      <article class="blogelem">
                         <div class="row"><!-- imgcont   -->
                            <div class="col-sm-12 col-md-3 col-lg-2 padding-bottom-10px">
                              <div class="row">
                                <div class="col-sm-12 col-md-12 col-lg-12" style="margin-bottom: 5px;">
                                  <div class="rating rating-float clearfix">
                                    @if($member->review)
                                      <span style="font-weight: 600">Rating</span>
                                      <ul>
                                        @if($member->review->stars==0)
                                        <li></li>
                                        <li></li>
                                        <li></li>
                                        <li></li>
                                        <li></li>
                                        @elseif($member->review->stars==1)
                                        <li class="active"></li>
                                        <li></li>
                                        <li></li>
                                        <li></li>
                                        <li></li>
                                        @elseif($member->review->stars==2)
                                        <li class="active"></li>
                                        <li class="active"></li>
                                        <li></li>
                                        <li></li>
                                        <li></li>
                                        @elseif($member->review->stars==3)
                                        <li class="active"></li>
                                        <li class="active"></li>
                                        <li class="active"></li>
                                        <li></li>
                                        <li></li>
                                        @elseif($member->review->stars==4)
                                        <li class="active"></li>
                                        <li class="active"></li>
                                        <li class="active"></li>
                                        <li class="active"></li>
                                        <li></li>
                                        @elseif($member->review->stars==5)
                                        <li class="active"></li>
                                        <li class="active"></li>
                                        <li class="active"></li>
                                        <li class="active"></li>
                                        <li class="active"></li>
                                        @endif
                                      </ul>
                                    @endif
                                    
                                  </div>
                                </div>
                                <div class="col-6 col-sm-6 col-md-12 col-lg-12 imgcont margin-bottom-10px">
                                  <a href="#">
                                    @php
                                      $pictures = explode(',',$member->picture);
                                     @endphp
                                    <!--  <img src="{{asset('storage/'.$pictures[0])}}" class="" alt="American"> -->
                                    <img src="{{asset('storage/'.$pictures[0])}}" class="img-responsive profimg clip" alt="American Surrogate Mother">
                                   </a>
                                </div>
                              </div>
                               
                            </div>
                            <div class="col-sm-12 col-md-9 col-lg-10">
                              <div class="row">
                                <div class="col-sm-12 col-md-12 col-lg-12 ptitle">
                                  <a href="/surrogate-mothers/53447">
                                    @php
                                      $first_name = explode(' ',$member->name);
                                     @endphp
                                    {{$first_name[0]}}</a>
                                  <br>
                                  {{$member->country}}'s 
                                  @if($category=='surrogate-mothers')
                                    Surrogate Mother
                                  @elseif($category=='egg-donors')
                                    Egg Donor
                                  @elseif($category=='sperm-donors')
                                    Sperm Donor
                                  @endif 
                                </div>
                                <div class="col-sm-12 col-md-12 col-lg-12">
                                  
                                </div>
                                <div class="col-sm-12 col-md-12 col-lg-12 bauthor">
                                  <table class="table">
                                    <tbody>
                                      <tr>
                                          <td width="15%"><b style="margin-left: 10px;">Age:</b></td>
                                          <td width="85%">{{$member->age}} Years</td>
                                       </tr>
                                       <tr>
                                          <td width="15%"><b style="margin-left: 10px;">Location:</b></td>
                                          <td width="85%">{{$member->country}}</td>
                                       </tr>
                                       @if($member->number_of_children)
                                       <tr> <!-- style="margin-left: 10px;" -->
                                          <td width="15%" style="padding-left: 12px;"><b>Own Child:</b></td> 
                                          <td width="85%">{{$member->number_of_children}} </td>
                                       </tr>
                                       @endif
                                    </tbody>
                                  </table>
                                </div>
                                <div class="col-sm-12 col-md-12 col-lg-12">
                                  <span class="excerpt">
                                    <div class="text" style="line-height: 20px; margin-left: 12px;">
                                      {{$member->describe_yourself}}
                                    </div>
                                    <div class="clearfix"></div> 
                                  </span>
                                </div>
                                <div class="col-5 col-sm-3 col-md-4 col-lg-2 offset-7 offset-lg-10 offset-md-8 offset-sm-9 padding-top-20px">
                                  <a href="{{route('single.member',[$member->id,'category'=>$category])}}">
                                    <button class="search_btn btn btn-block" type="button">See Profile</button>
                                  </a>
                                </div>
                              </div>
                            </div>
                         </div>
                      </article>
                    </div>
                  </section>
                  <ul class="pagination pagination-md ">
                     
                  </ul>
              </div>
              @endforeach
          </div>
        </div>
    </div>
  </section>
@endsection
@section('additional-js')

@endsection
<!-- <div class="col-sm-3">
                   <div class="form-group region_id_holder">
                      <label for="region_id">Region/State</label>
                      <select class="form-control" id="region_id" name="region_id" onchange="getCityListDrp(this.value);">
                         <option value="">Please Select</option>
                      </select>
                   </div>
                </div>
                <div class="col-sm-3">
                   <div class="form-group city_id_holder">
                      <label for="city_id">City</label>
                      <select class="form-control" id="city_id" name="city_id">
                         <option value="">Please Select</option>
                      </select>
                   </div>
                </div> -->
               <!--  <div class="col-sm-3">
                   <div class="form-group">
                    <label for="postcode">Zipcode</label>
                    <input type="text" class="form-control" id="postcode" name="postcode" value="">
                   </div>
                </div> -->
                <!-- <div class="col-sm-3">
                   <div class="form-group">
                      <label for="users_2_sm_types">Type of Surrogate</label>
                      <select class="form-control" name="users_2_sm_types">
                         <option value="">Please Select</option>
                         <option value="1">Gestational</option>
                         <option value="2">Traditional</option>
                      </select>
                   </div>
                </div> -->