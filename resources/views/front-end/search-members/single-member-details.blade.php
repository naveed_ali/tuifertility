@extends('front-end.layouts.master')
@section('content')
<section class="banner padding-top-170px padding-bottom-30px tablet-padding-p-top sm-ptb-30px search_page_bg" style="background-image: url({{asset('assets/img/banner_1.jpg')}});">
   <div id="page-title" class="margin-mob"><!-- padding-tb-30px -->
      <div class="container">
         <ol class="breadcrumb opacity-5">
            <li><a href="{{url('/')}}">Home</a></li>
            <li>
              @if($category=='surrogate-mothers')
              <a href="{{url('category/search/surrogate-mothers')}}">Surrogate Mothers</a>
              @elseif($category=='egg-donors')
              <a href="{{url('category/search/egg-donors')}}">Egg Donors</a>
              @elseif($category=='sperm-donors')
              <a href="{{url('category/search/sperm-donors')}}">Sperm Donors</a>
              @endif
            </li>
            @php
               $first_name = explode(' ',$becomeMember->name);
            @endphp
            <li class="active">{{$first_name[0]}}</li>
         </ol>
        <!--  <h1 class="font-weight-300">Alrayan Eye Clinic</h1> -->
      </div>
   </div>
   <div class="margin-tb-0px">
      <div class="container">
         <div class="row">
            <div class="col-lg-12">
               <div class="margin-bottom-30px box-shadow">
                  <section id="homeblog" style="height: auto !important; margin-top: 5px;"> 
                     <div class="blogpo" style="height: auto !important;">
                        <article class="blogelem">
                           <div class="row">
                              <div class="col-6 col-sm-6 col-md-3 col-lg-2 imgcont margin-bottom-10px">
                                 @php $pictures[] = explode(',',$becomeMember->picture);
                                 @endphp
                                 <a href="/surrogate-mothers/53447">
                                    <img src="{{asset('storage/'.$pictures[0][0])}}" class="img-responsive profimg clip" alt="American Surrogate Mother">
                                 </a>
                              </div>
                              <div class="col-sm-12 col-md-9 col-lg-10">
                                 <div class="postdata">
                                    <div class="ptitle">
                                       <a href="/surrogate-mothers/53447">{{$first_name[0]}}</a><br>
                                       {{$becomeMember->country}}'s 
                                       @if($category=='surrogate-mothers')
                                       Surrogate Mother
                                       @elseif($category=='egg-donors')
                                       Egg Donor
                                       @elseif($category=='sperm-donors')
                                       Sperm Donor
                                       @endif 
                                    </div>
                                    <div class="bauthor">
                                       <table class="table">
                                          <tbody>
                                             <!-- <tr>
                                                <td width="15%"><b>Name:</b></td>
                                                <td width="85%">{{$first_name[0]}}</td>
                                             </tr> -->
                                             <!-- <tr>
                                                <td><b>Member Type:</b></td>
                                                <td>{{$becomeMember->category}}</td>
                                             </tr> -->
                                             <!-- <tr>
                                                <td><b>Country:</b></td>
                                                <td>{{$becomeMember->country}}</td>
                                             </tr> -->
                                             <tr>
                                                <td width="15%" style="padding-left: 12px;"><b>Hair Color:</b></td>
                                                <td width="85%">{{$becomeMember->hair_color}} </td>
                                             </tr>
                                             <tr>
                                                <td width="15%" style="padding-left: 12px;"><b>Eye Color:</b></td>
                                                <td width="85%">{{$becomeMember->eye_color}} </td>
                                             </tr>
                                             <!-- <tr>
                                                <td><b>Date Registered:</b></td>
                                                <td>{{$becomeMember->created_at->diffForHumans()}} </td>
                                             </tr> -->
                                          </tbody>
                                       </table>
                                    </div>
                                 </div>
                              </div>
                           </div>
                        </article>
                     </div>
                  </section>
               </div>
               <div class="margin-bottom-30px box-shadow" style="font-family: Raleway;">
                  <div class="padding-30px" style="box-shadow: 0 0 20px #7e7b7b;">
                     <ul class="nav nav-tabs" id="myTab" role="tablist">
                        <li class="nav-item">
                           <a class="nav-link active" id="about-me-tab" data-toggle="tab" href="#about-me" role="tab" aria-controls="about-me" aria-selected="true">About Me</a>
                        </li>
                        @if($becomeMember->picture)
                        <li class="nav-item">
                           <a class="nav-link" id="pictures-tab" data-toggle="tab" href="#pictures" role="tab" aria-controls="pictures" aria-selected="false">Pictures</a>
                        </li>
                        @endif
                        @if($becomeMember->video_url)
                        <li class="nav-item">
                           <a class="nav-link" id="video-tab" data-toggle="tab" href="#video" role="tab" aria-controls="video" aria-selected="false">Videos</a>
                        </li>
                        @endif
                        @if($becomeMember->review)
                        <li class="nav-item">
                           <a class="nav-link" id="reviews-tab" data-toggle="tab" href="#reviews" role="tab" aria-controls="reviews" aria-selected="false">Reviews</a>
                        </li>
                        @endif
                     </ul>
                     <div class="tab-content" id="myTabContent">
                        <div class="tab-pane fade show active" id="about-me" role="tabpanel" aria-labelledby="about-me-tab">
                           <div class="row margin-top-20px" >
                              <div class="col-8 col-md-4">
                                 <b>I am Willing to Help Couples of: </b>
                              </div>
                              <div class="col-4 col-md-8">
                                 {{$becomeMember->willing_to_assist}}
                              </div>
                           </div>
                           <hr>
                           <div class="row">
                              <div class="col-8 col-md-4">
                                 <b>Number of Children (non-surrogate): </b>
                              </div>
                              <div class="col-4 col-md-8">
                                 {{$becomeMember->number_of_children}}
                              </div>
                           </div>
                           <hr>
                           <div class="row">
                              <div class="col-8 col-md-4">
                                 <b>I Have a Passport: </b>
                              </div>
                              <div class="col-4 col-md-8">
                                 {{$becomeMember->have_passport}}
                              </div>
                           </div>
                           <hr>
                           <div class="row">
                              <div class="col-8 col-md-4">
                                 <b>Willing to Travel : </b>
                              </div>
                              <div class="col-4 col-md-8">
                                 {{$becomeMember->willing_to_travel}}
                              </div>
                           </div>
                           <hr>
                           <div class="row">
                              <div class="col-8 col-md-4">
                                 <b>Race : </b>
                              </div>
                              <div class="col-4 col-md-8">
                                 {{$becomeMember->race}}
                              </div>
                           </div>
                           <hr>
                           <div class="row">
                              <div class="col-8 col-md-4">
                                 <b>Religion: </b>
                              </div>
                              <div class="col-4 col-md-8">
                                 {{$becomeMember->religion}}
                              </div>
                           </div>
                           <hr>
                           <div class="row">
                              <div class="col-8 col-md-4">
                                 <b>My Health is: </b>
                              </div>
                              <div class="col-4 col-md-8">
                                 {{$becomeMember->health_status}}
                              </div>
                           </div>
                           <hr>
                           <div class="row">
                              <div class="col-8 col-md-4">
                                 <b>My Spoken Languages: </b>
                              </div>
                              <div class="col-4 col-md-8">
                                 {{$becomeMember->spoken_language}}
                              </div>
                           </div>
                           <hr>
                           <div class="row">
                              <div class="col-8 col-md-4">
                                 <b>My Education is: </b>
                              </div>
                              <div class="col-4 col-md-8">
                                 {{$becomeMember->education}}
                              </div>
                           </div>
                           <hr>
                           <div class="row">
                              <div class="col-8 col-md-4">
                                 <b>I am a Smoker: </b>
                              </div>
                              <div class="col-4 col-md-8">
                                 {{$becomeMember->smoker}}
                              </div>
                           </div>
                           <hr>
                           <div class="row">
                              <div class="col-8 col-md-4">
                                 <b>My Blood Type is: </b>
                              </div>
                              <div class="col-4 col-md-8">
                                 {{$becomeMember->blood_type}}
                              </div>
                           </div>
                           <hr>
                           <div class="row">
                              <div class="col-8 col-md-4">
                                 <b>Height: </b>
                              </div>
                              <div class="col-4 col-md-8">
                                 {{$becomeMember->height}}
                              </div>
                           </div>
                           <hr>
                           <div class="row">
                              <div class="col-8 col-md-4">
                                 <b>Marital Status: </b>
                              </div>
                              <div class="col-4 col-md-8">
                                 {{$becomeMember->marital_status}}
                              </div>
                           </div>
                           <hr>
                           @if($becomeMember->why_become_member)
                           <div class="row">
                              <div class="col-8 col-md-4">
                                 <b>Why I want to be a surrogate mother:</b>
                              </div>
                              <div class="col-4 col-md-8">
                                 {{$becomeMember->why_become_member}}
                              </div>
                           </div>
                           <hr>
                           @endif
                           @if($becomeMember->describe_yourself)
                           <div class="row">
                              <div class="col-4 col-md-4">
                                 <b>Description of myself:</b>
                              </div>
                              <div class="col-8 col-md-8">
                                 {{$becomeMember->describe_yourself}}
                              </div>
                           </div>
                           <hr>
                           @endif
                           @if($becomeMember->communication_letter)
                           <div class="row">
                              <div class="col-8 col-md-4">
                                 <b>My letter to intended parents:</b>
                              </div>
                              <div class="col-4 col-md-8">
                                 {{$becomeMember->communication_letter}}
                              </div>
                           </div>
                           <hr>
                           @endif
                        </div>
                        @if($becomeMember->picture)
                        <div class="tab-pane fade" id="pictures" role="tabpanel" aria-labelledby="pictures-tab">
                           <div class="row margin-top-30px">
                              @foreach(explode(',',$becomeMember->picture) as $picture)
                              <div class="col-md-6">
                                 <img src="{{asset('storage/'.$picture)}}"  data-toggle="modal" data-target="#exampleModalCenter" class="img-fluid" alt="Responsive image">
                              </div>
                              @endforeach
                              
                           </div>
                        </div>
                        @endif
                        @if($becomeMember->video_url)
                        <div class="tab-pane fade" id="video" role="tabpanel" aria-labelledby="video-tab">
                           <div class="row margin-top-30px">
                              <div class="embed-responsive embed-responsive-16by9">
                                <iframe class="embed-responsive-item" src="{{$becomeMember->video_url}}" allowfullscreen></iframe>
                              </div>
                           </div>
                        </div>
                        @endif
                        @if($becomeMember->picture)
                        <div class="tab-pane fade" id="reviews" role="tabpanel" aria-labelledby="reviews-tab">
                           <div class="row margin-top-15px" >
                             <div class="col-lg-12">
                                 <div class="rating clearfix">
                                    <ul class="float-left">
                                       @if($becomeMember->review)
                                       @if($becomeMember->review->stars==0)
                                       <li></li>
                                       <li></li>
                                       <li></li>
                                       <li></li>
                                       <li></li>
                                       @elseif($becomeMember->review->stars==1)
                                       <li class="active"></li>
                                       <li></li>
                                       <li></li>
                                       <li></li>
                                       <li></li>
                                       @elseif($becomeMember->review->stars==2)
                                       <li class="active"></li>
                                       <li class="active"></li>
                                       <li></li>
                                       <li></li>
                                       <li></li>
                                       @elseif($becomeMember->review->stars==3)
                                       <li class="active"></li>
                                       <li class="active"></li>
                                       <li class="active"></li>
                                       <li></li>
                                       <li></li>
                                       @elseif($becomeMember->review->stars==4)
                                       <li class="active"></li>
                                       <li class="active"></li>
                                       <li class="active"></li>
                                       <li class="active"></li>
                                       <li></li>
                                       @elseif($becomeMember->review->stars==5)
                                       <li class="active"></li>
                                       <li class="active"></li>
                                       <li class="active"></li>
                                       <li class="active"></li>
                                       <li class="active"></li>
                                       @endif
                                      @else
                                      <li></li>
                                       <li></li>
                                       <li></li>
                                       <li></li>
                                       <li></li>
                                      @endif
                                    </ul>
                                 </div>
                             </div>
                             <div class="col-lg-12">
                                 @if($becomeMember->review)
                                <p>{!!$becomeMember->review->review!!}</p>
                                @else
                                <p class="margin-top-15px">There are no reviews yet</p>
                                @endif
                             </div>
                           </div>
                        </div>
                        @endif
                     </div>
                  </div>
               </div>
            </div>
         </div>
         <div class="row">
            <div class="col-12 col-sm-12 col-md-12 col-lg-12 margin-bottom-30px">
               <a href="{{route('intendent.parent.form',$category)}}">
                  <button class="search_btn btn btn-block" type="button">
                     Book This
                     @if($category=='surrogate-mothers')
                     Surrogate Mother
                     @elseif($category=='egg-donors')
                     Egg Donor
                     @elseif($category=='sperm-donors')
                     Sperm Donor
                     @endif
                  </button>
               </a>
               <!-- <a class="btn btn-lg border-2  white-btn-lg btn-block border-radius-15 padding-15px box-shadow">Apply to use it</a> -->
               
               
               </a>
            </div>
         </div>
      </div>
   </div>
</section>
@endsection


