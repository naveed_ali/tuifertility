<!DOCTYPE html>
<html lang="en-US">
<head>
    <title>{{$subpage->title}}</title>
    <meta name="author" content="Nile-Theme">
    <meta name="description" value="i am just checking">
    <meta name="robots" content="index follow">
    <meta name="googlebot" content="index follow">
    <meta http-equiv="content-type" content="text/html; charset=utf-8">
    <meta name="keywords" content="directory, doctor, doctor directory, Health directory, listing, map, medical, medical directory, professional directory, reservation, reviews">
    <meta name="description" content="{{$subpage->meta_data}}">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <!-- google fonts -->
    <link href="https://fonts.googleapis.com/css?family=Open+Sans:400,600,800%7CPoppins:300i,300,400,700,400i,500%7CDancing+Script:700%7CDancing+Script:700%7CRaleway:300,400,600,700|Montserrat:400,700"
        rel="stylesheet">
    <!-- animate -->
    <link rel="stylesheet" href="{{asset('assets/css/animate.css')}}" />
    <!-- owl Carousel assets -->
    <link href="{{asset('assets/css/owl.carousel.css')}}" rel="stylesheet">
    <link href="{{asset('assets/css/owl.theme.css')}}" rel="stylesheet">
    <!-- bootstrap -->
    <link rel="stylesheet" href="{{asset('assets/css/bootstrap.min.css')}}">
    <!-- hover anmation -->
    <link rel="stylesheet" href="{{asset('assets/css/hover-min.css')}}">
    <!-- flag icon -->
    <link rel="stylesheet" href="{{asset('assets/css/flag-icon.min.css')}}">
    <!-- main style -->
    <link rel="stylesheet" href="{{asset('assets/css/style.css')}}">
    <link rel="stylesheet" href="{{asset('assets/css/custom.css')}}">
    <!-- colors -->
    <link rel="stylesheet" href="{{asset('assets/css/colors/main.css')}}">
    <!-- elegant icon -->
    <link rel="stylesheet" href="{{asset('assets/css/elegant_icon.css')}}">
    <link rel="shortcut icon" type="image/png" href="https://tuifertility.trustechsol.com/assets/img/Background.png">
    <link href="https://fonts.googleapis.com/css2?family=Open+Sans:wght@600&family=Roboto&display=swap" rel="stylesheet">
    <!-- jquery library  -->
    <script src="{{asset('assets/js/jquery-3.2.1.min.js')}}"></script>
    <!-- fontawesome  -->
    <script defer src="https://use.fontawesome.com/releases/v5.0.6/js/all.js"></script>
    
  <meta property="og:title" content="{{$subpage->title}}" />
<meta property="og:description" content="{{$subpage->meta_data}}" />
<meta property="og:image" content="{{asset('storage/'.$subpage->picture)}}" />
<script type='text/javascript' src='https://platform-api.sharethis.com/js/sharethis.js#property=5c3cb69ae784a00011c52303&product=inline-share-buttons' async='async'></script>
@yield('additional-head')
</head>

<body>

   @include('front-end.partials.navigation')
   <section class="banner padding-top-180px padding-bottom-30px search_page_bg padding-top-screen" style="background-image: url({{asset('assets/img/banner_1.jpg')}});">
    <!-- // Header  -->
    
         <div class="container share_buttons">
            <div class="sharethis-inline-share-buttons padding-bottom-20px sm-mt-30px" data-description="{{$subpage->meta_data}}" data-title="{{$subpage->title}}"></div>
        </div>
        <div class="container">
            <div class="row"> 
                <div class="col-lg-12">
                    <div class="border-radius-10 background-white  margin-bottom-35px padding-30px">
                        <div class="row">
                            <div class="col-8 col-md-8 col-sm-8">
                                <ol class="breadcrumb opacity-5">
                                    <li><a href="{{url('/')}}">Home</a></li>
                                    <li><a href="{{url($subpage->page->menu->slug.'/'.$subpage->page->slug)}}">{{$subpage->page->menu->name}}</a></li>
                                     <li><a href="{{url($subpage->page->menu->slug.'/'.$subpage->page->slug)}}">{{$subpage->page->title}}</a></li>
                                     <li>{{$subpage->title}}</li>
                                </ol>
                                <h1 class="pagetitle" style="">{{$subpage->title}}</h1>
                            </div>
                            <div class="col-4 col-md-4 col-sm-4" style="text-align: right;">
                                <img class="menu-page-img" src="{{asset('storage/'.$subpage->picture)}}" alt="subpage-pic" style="border-radius:60px;" width="100" height="100">
                            </div>
                            <div class="entry-content margin-left-12px">
                                <p>{!! $subpage->description !!}</p>
                            </div>
                        <!-- .entry-content -->

                        <!-- Post tags -->
                            <hr>
                        </div>
                    </div>
                </div>
            </div>
        </div>
</section>
   @include('front-end.partials.footer')
   @include('front-end.partials.js-libraries')
    <script>
$(document).ready(function(){
    $("img").addClass("img-responsive");
    $("img").css("max-width", "100%");
});
</script>
</body>
</html>
