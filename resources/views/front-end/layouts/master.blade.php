<!DOCTYPE html>
<html lang="en-US">
<meta name="viewport" content="width=device-width, initial-scale=1">
@include('front-end.partials.head')

<body>

   @include('front-end.partials.navigation')
    <!-- // Header  -->
    @yield('content')
   @include('front-end.partials.footer')
   @yield('pop-up-modal')
   @include('front-end.partials.js-libraries')
   @yield('additional-js')
   <script type="text/javascript">
   	$(document).ready( function () {
    $('#looking_for_select').on('change',function(e){
    	$('#category').val($(this).val());

    	// $('.search_button').val($(this).val());
    });
});
   </script>
</body>
</html>
