<!DOCTYPE html>
<html lang="en-US">
<head>
    <title>{{$page->title}}</title>
    <meta name="author" content="Nile-Theme">
    <meta name="description" value="i am just checking">
    <meta name="robots" content="index follow">
    <meta name="googlebot" content="index follow">
    <meta http-equiv="content-type" content="text/html; charset=utf-8">
    <meta name="keywords" content="directory, doctor, doctor directory, Health directory, listing, map, medical, medical directory, professional directory, reservation, reviews">
    <meta name="description" content="{{$page->meta_data}}">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <!-- google fonts -->
    <link href="../../../fonts.googleapis.com/cssa182.css?family=Open+Sans:400,600,800%7Chttps://fonts.googleapis.com/css?family=Open+Sans:400,600,800%7CPoppins:300i,300,400,700,400i,500%7CDancing+Script:700%7CDancing+Script:700" rel="stylesheet">
    <!-- animate -->
    <link rel="stylesheet" href="{{asset('assets/css/animate.css')}}" />
    <!-- owl Carousel assets -->
    <link href="{{asset('assets/css/owl.carousel.css')}}" rel="stylesheet">
    <link href="{{asset('assets/css/owl.theme.css')}}" rel="stylesheet">
    <!-- bootstrap -->
    <link rel="stylesheet" href="{{asset('assets/css/bootstrap.min.css')}}">
    <!-- hover anmation -->
    <link rel="stylesheet" href="{{asset('assets/css/hover-min.css')}}">
    <!-- flag icon -->
    <link rel="stylesheet" href="{{asset('assets/css/flag-icon.min.css')}}">
    <!-- main style -->
    <link rel="stylesheet" href="{{asset('assets/css/style.css')}}">
    <link rel="stylesheet" href="{{asset('assets/css/custom.css')}}">
    <!-- colors -->
    <link rel="stylesheet" href="{{asset('assets/css/colors/main.css')}}">
    <!-- elegant icon -->
    <link rel="stylesheet" href="{{asset('assets/css/elegant_icon.css')}}">

    <!-- jquery library  -->
    <script src="{{asset('assets/js/jquery-3.2.1.min.js')}}"></script>
    <!-- fontawesome  -->
    <script defer src="https://use.fontawesome.com/releases/v5.0.6/js/all.js"></script>
    
 
<script type='text/javascript' src='https://platform-api.sharethis.com/js/sharethis.js#property=5c3cb69ae784a00011c52303&product=inline-share-buttons' async='async'></script>
@yield('additional-head')
</head>

<body>

   @include('front-end.partials.navigation')
<section class="banner padding-tb-170px sm-ptb-80px background-overlay" style="background-image: url({{asset('assets/img/banner_1.jpg')}});">
     <div id="page-title" class="padding-tb-70px gradient-white">
        <div class="container">
            <ol class="breadcrumb opacity-5">
                <li><a href="#">{{$subpage->page->menu->name}}</a></li>
                <li><a href="#">{{$subpage->page->name}}</a></li>
            </ol>
            <h1 class="font-weight-300">{{$subpage->title}}</h1>
        </div>
    </div>


    <div class="">
        <div class="container">
            <div class="row">
                <div class="col-lg-8">
                    <div class="border-radius-10 background-white  margin-bottom-35px padding-30px">
                        <h4><i class="fas fa-file-alt margin-right-10px text-main-color"></i> {{$subpage->title}}</h4>
                        <hr>

                        <div class="meta">
                            <span class="margin-right-20px text-extra-small">By :  <a href="#">admin</a></span>
                            <span class="margin-right-20px text-extra-small">Date :  <a href="#">December 27, 2017</a></span>
                        </div>
                        <hr>

                        <!-- <div class="thumbnail margin-bottom-20px"><img src="assets/img/blog-9.jpg" alt=""></div> -->
                        <div class="entry-content">
                            {!!$subpage->description!!}
                        </div>
                        <!-- .entry-content -->

                        <!-- Post tags -->
                        <hr>
                       <!--  <div class="post-tags">
                            <a href="#" rel="tag">chemicals</a>
                            <a href="#" rel="tag">chemicals</a>
                            <a href="#" rel="tag">chemicals</a>
                            <a href="#" rel="tag">chemicals</a>
                            <a href="#" rel="tag">chemicals</a>
                            <a href="#" rel="tag">chemicals</a>
                            
                        </div> -->
                    </div>
                </div>

                <div class="col-lg-4">

                    <div class="margin-bottom-30px">
                        <div class="padding-30px background-white border-radius-10">
                            <h4><i class="fas fa-search margin-right-10px text-main-color"></i> Search</h4>
                            <hr>
                            <div class="input-group mb-3">
                                <input type="text" value="Search..." class="form-control border-radius-0">
                                <div class="input-group-append">
                                    <button class="btn btn-outline-secondary text-white background-main-color border-radius-0" type="button">Search</button>
                                </div>
                            </div>

                        </div>
                    </div>

                    <div class="widget widget_categories">
                        <div class="margin-bottom-30px">
                            <div class="padding-30px background-white border-radius-10">
                                <h4><i class="far fa-folder-open margin-right-10px text-main-color"></i> Categories</h4>
                                <hr>
                                <ul>
                                    <li><a href="#">Tech</a></li>
                                    <li><a href="#">Gallary</a></li>
                                    <li><a href="#">UI Design </a></li>
                                    <li><a href="#">Shop</a></li>
                                    <li><a href="#">Wordpress  </a></li>
                                    <li><a href="#">Cultur</a></li>
                                </ul>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
</section>
@endsection
