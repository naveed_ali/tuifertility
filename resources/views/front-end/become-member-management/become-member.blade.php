@extends('front-end.layouts.member-master')
@section('content')
<section class="banner padding-top-200px padding-bottom-30px sm-ptb-0px" style="background-image: url({{asset('assets/img/banner_1.jpg')}});">
   <div id="page-title" class="margin-mob">
      <div class="container">
         <ol class="breadcrumb opacity-5">
            <li><a href="{{url('/')}}">Home</a></li>
            <li class="active">Sign Up</li>
         </ol><br>
         <h1 class="font-weight" style="color: #007cff;">
            {{$formText->title}}
         </h1><br>
         <p style="font: 14px/26px Raleway; line-height: 26px; color: #6b6c6c; line-height: 24px;"><em>{{$formText->note}}</em></p>
         @if ($errors->any())
         <div class="alert alert-danger">
            <ul>
               @foreach ($errors->all() as $error)
               <li>{{ $error }}</li>
               @endforeach
            </ul>
         </div>
         @endif
      </div>
   </div>
   <hr>
   <div class="">
      <div class="container margin-bottom-0px">
         <!--======= log_in_page =======-->
         <div id="log-in" class=" .padding-top-10 site-form border-radius-10">
            <div class="form-output">
               <form method="post" action="{{route('become_members.store')}}" enctype="multipart/form-data">
                  @csrf
                  <input type="hidden" name="category" value="{{$category}}">
                  <div class="row">
                     <div class="col-md-6 col-sm-12">
                        <div class="form-group label-floating">
                           <label class="control-label">{{$formText->email}}</label>
                           <input class="form-control" placeholder="" name="email" type="email" value="{{old('email')}}">
                        </div>
                     </div>
                     <div class="col-md-6 col-sm-12">
                        <div class="form-group label-floating">
                           <label class="control-label">{{$formText->name}}</label>
                           <input class="form-control" name="name" placeholder="" type="text" value="{{old('name')}}">
                        </div>
                     </div>
                  </div>
                   <div class="row">
                     <div class="col-md-12 col-sm-12">
                     <div class="form-group label-floating">
                       <label class="control-label">{{$formText->birthday}}</label>
                       <input class="form-control" name="birthdate" placeholder="{{$formText->birthday}}" id="date_of_birth" type="text" value="{{old('birthdate')}}">
                     </div>
                     </div>
                     
                     </div>
                  <div class="row">
                    <div class="col-md-6 col-sm-12">
                        <div class="form-group label-floating is-select">
                           <label class="control-label">{{$formText->country}}</label>
                           <select class="selectpicker form-control" name="country">
                              @foreach($countries as $country)
                              <option value="{{$country->country_name}}">{{$country->country_name}}</option>
                              @endforeach
                           </select>
                        </div>
                    </div>
                     <div class="col-md-6 col-sm-12">
                        <div class="form-group label-floating">
                           <label class="control-label">{{$formText->address}}</label>
                           <input class="form-control" name="address" placeholder="{{$formText->address}}" type="text" value="{{old('address')}}">
                        </div>
                     </div>
                    
                  </div>
                  <div class="row">
                     
                      <div class="col-md-6 col-sm-12">
                        <div class="form-group label-floating">
                           <label class="control-label">{{$formText->phone}}</label>
                           <input class="form-control" name="phone" placeholder="{{$formText->phone}}" type="text" value="{{old('phone')}}">
                        </div>
                     </div>
                     <div class="col-md-6 col-sm-12">
                        <div class="form-group label-floating is-select">
                           <label class="control-label">{{$formText->race}}</label>
                           <select class="selectpicker form-control" name="race">
                              <option value="">Please Select {{$formText->race}}</option>
                              @foreach(explode(',',$formText->race_data) as $key => $race_data)
                              <option value="{{$race_data}}">{{$race_data}}</option>
                              @endforeach
                           </select>
                        </div>
                     </div>
                   </div>
                   <div class="row">
                     <div class="col-md-6 col-sm-12">
                        <div class="form-group label-floating is-select">
                           <label class="control-label">{{$formText->religion}}</label>
                           <select class="selectpicker form-control" name="religion">
                              <option value="">Please Select {{$formText->religion}}</option>
                               @foreach(explode(',',$formText->religion_data) as $key => $religion_data)
                              <option value="{{$religion_data}}">{{$religion_data}}</option>
                              @endforeach
                           </select>
                        </div>
                     </div>
                     <div class="col-md-6 col-sm-12">
                        <div class="form-group label-floating is-select">
                           <label class="control-label">{{$formText->education}}</label>
                           <select class="selectpicker form-control" name="education">
                              <option value="">Please Select {{$formText->education}}</option>
                              @foreach(explode(',',$formText->education_data) as $key => $education)
                              <option value="{{$education}}">{{$education}}</option>
                              @endforeach
                           </select>
                        </div>
                     </div>
                   </div>
                  <div class="row">
                     
                     <div class="col-md-6 col-sm-12">
                        <div class="form-group label-floating is-select">
                           <label class="control-label">{{$formText->health_status}}</label>
                           <select class="selectpicker form-control" name="health_status">
                              <option value="">Please Select {{$formText->health_status}}</option>
                               @foreach(explode(',',$formText->health_status_data) as $key => $health_status_data)
                              <option value="{{$health_status_data}}">{{$health_status_data}}</option>
                              @endforeach
                           </select>
                        </div>
                     </div>
                     <div class="col-md-6 col-sm-12">
                        <div class="form-group label-floating is-select">
                           <label class="control-label">{{$formText->smoker}}</label>
                           <select class="selectpicker form-control" name="smoker">
                            <option value="">Please Select {{$formText->smoker}}</option>
                               @foreach(explode(',',$formText->smoker_data) as $key => $smoker_data)
                              <option value="{{$smoker_data}}">{{$smoker_data}}</option>
                              @endforeach
                           </select>
                        </div>
                     </div>
                   </div>
                   <div class="row">
                     <div class="col-md-6 col-sm-12">
                        <div class="form-group label-floating is-select">
                           <label class="control-label">{{$formText->eye_color}}</label>
                           <select class="selectpicker form-control" name="eye_color">
                              <option value="">Please Select {{$formText->eye_color}}</option>
                               @foreach(explode(',',$formText->eye_color_data) as $key => $eye_color_data)
                              <option value="{{$eye_color_data}}">{{$eye_color_data}}</option>
                              @endforeach
                           </select>
                        </div>
                     </div>
                     <div class="col-md-6 col-sm-12">
                        <div class="form-group label-floating is-select">
                           <label class="control-label">{{$formText->hair_color}}</label>
                           <select class="selectpicker form-control" name="hair_color">
                              <option value="">Please Select {{$formText->hair_color}}</option>
                              @foreach(explode(',',$formText->hair_color_data) as $key => $hair_color_data)
                              <option value="{{$hair_color_data}}">{{$hair_color_data}}</option>
                              @endforeach
                           </select>
                        </div>
                     </div>
                  </div>
                  <div class="row">
                     
                     <div class="col-md-6 col-sm-12">
                        <div class="form-group label-floating is-select">
                           <label class="control-label">{{$formText->height}}</label>
                           <select class="selectpicker form-control" name="height">
                              <option value="">Please Select {{$formText->height}}</option>
                                @foreach(explode(',',$formText->height_data) as $key => $height_data)
                              <option value="{{$height_data}}">{{$height_data}}</option>
                              @endforeach
                           </select>
                        </div>
                     </div>
                     <div class="col-md-6 col-sm-12">
                        <div class="form-group label-floating is-select">
                           <label class="control-label">{{$formText->weight}}</label>
                           <select class="selectpicker form-control" name="weight">
                              <option value="">Please Select {{$formText->weight}}</option>
                              @foreach(explode(',',$formText->weight_data) as $key => $weight_data)
                              <option value="{{$weight_data}}">{{$weight_data}}</option>
                              @endforeach
                           </select>
                        </div>
                     </div>
                   </div>
                   <div class="row">
                     <div class="col-md-6 col-sm-12">
                        <div class="form-group label-floating is-select">
                           <label class="control-label">{{$formText->blood_type}}</label>
                           <select class="selectpicker form-control" name="blood_type">
                              <option value="">Please Select {{$formText->bood_type}}</option>
                               @foreach(explode(',',$formText->blood_type_data) as $key => $blood_type_data)
                              <option value="{{$blood_type_data}}">{{$blood_type_data}}</option>
                              @endforeach
                           </select>
                        </div>
                     </div>
                     <div class="col-md-6 col-sm-12">
                        <div class="form-group label-floating is-select">
                           <label class="control-label">{{$formText->have_passport}}</label>
                           <select class="selectpicker form-control" name="have_passport">
                              <option value="">Please Select {{$formText->have_passport}}</option>
                               @foreach(explode(',',$formText->have_passport_data) as $key => $have_passport_data)
                              <option value="{{$have_passport_data}}">{{$have_passport_data}}</option>
                              @endforeach
                           </select>
                        </div>
                     </div>
                  </div>
                  <div class="row">
                     
                     <div class="col-md-6 col-sm-12">
                        <div class="form-group label-floating is-select">
                           <label class="control-label">{{$formText->willing_to_travel}}</label>
                           <select class="selectpicker form-control" name="willing_to_travel">
                              <option value="">Please Select {{$formText->willing_to_travel}}</option>
                               @foreach(explode(',',$formText->willing_to_travel_data) as $key => $willing_to_travel_data)
                              <option value="{{$willing_to_travel_data}}">{{$willing_to_travel_data}}</option>
                              @endforeach
                           </select>
                        </div>
                     </div>
                     <div class="col-md-6 col-sm-12">
                        <div class="form-group label-floating is-select">
                           <label class="control-label">{{$formText->number_of_children}}</label>
                           <select class="selectpicker form-control" name="number_of_children">
                              <option value="">Please Select {{$formText->number_of_children}}</option>
                               @foreach(explode(',',$formText->number_of_children_data) as $key => $number_of_children_data)
                              <option value="{{$number_of_children_data}}">{{$number_of_children_data}}</option>
                              @endforeach
                           </select>
                        </div>
                     </div>
                  </div>
                  <div class="row">
                     <div class="col-md-6 col-sm-12">
                     <div class="form-group label-floating is-select">
                           <label class="control-label">{{$formText->number_of_surrogate_children}}</label>
                           <select class="selectpicker form-control" name="number_of_surrogate_children">
                              <option value="">Please Select {{$formText->number_of_surrogate_children}}</option>
                              @foreach(explode(',',$formText->number_of_surrogate_children_data) as $key => $number_of_surrogate_children_data)
                              <option value="{{$number_of_surrogate_children_data}}">{{$number_of_surrogate_children_data}}</option>
                              @endforeach
                           </select>
                        </div>
                     </div>
                     <div class="col-md-6 col-sm-12">
                        <div class="form-group label-floating is-select">
                           <label class="control-label">{{$formText->surrogate_type}}</label>
                           <select class="selectpicker form-control jq_multiselect" name="surrogate_type[]" multiple="multiple">
                               @foreach(explode(',',$formText->surrogate_type_data) as $key => $surrogate_type_data)
                              <option value="{{$surrogate_type_data}}">{{$surrogate_type_data}}</option>
                              @endforeach
                           </select>
                        </div>
                     </div>
                  </div>
                  <div class="row">
                     <div class="col-md-6 col-sm-12">
                        <div class="form-group label-floating is-select">
                           <label class="control-label">{{$formText->given_service_before}}</label>
                           <select class="selectpicker form-control" name="given_service_before">
                              <option value="">Please Select</option>
                               @foreach(explode(',',$formText->given_service_before_data) as $key => $given_service_before_data)
                              <option value="{{$given_service_before_data}}">{{$given_service_before_data}}</option>
                              @endforeach
                           </select>
                        </div>
                     </div>
                     <div class="col-md-6 col-sm-12">
                        <div class="form-group label-floating is-select">
                           <label class="control-label">{{$formText->marital_status}}</label>
                           <select class="selectpicker form-control" name="marital_status">
                              <option value="">Please Select {{$formText->marital_status}}</option>
                               @foreach(explode(',',$formText->marital_status_data) as $key => $marital_status_data)
                              <option value="{{$marital_status_data}}">{{$marital_status_data}}</option>
                              @endforeach
                           </select>
                        </div>
                     </div>
                   </div>
                   <div class="row">
                     <div class="col-md-6 col-sm-12">
                        <div class="form-group label-floating is-select">
                           <label class="control-label">{{$formText->spoken_language}}</label>
                           <div class="input-group">
                            <select class="form-control jq_multiselect" id="spoken_languages" enable_select_all="false" name="spoken_language[]" multiple="multiple" >
                               @foreach(explode(',',$formText->spoken_language_data) as $key => $spoken_language_data)
                              <option value="{{$spoken_language_data}}">{{$spoken_language_data}}</option>
                              @endforeach
                            </select>
                        </div>
                     </div>
                   </div>
                     <div class="col-md-6 col-sm-12">
                        <div class="form-group label-floating is-select">
                           <label class="control-label">{{$formText->willing_to_assist}}</label>
                           <div class="input-group">
                           <select class="selectpicker form-control spoken_language jq_multiselect" name="willing_to_assist[]" multiple="multiple">
                               @foreach(explode(',',$formText->willing_to_assist_data) as $key => $willing_to_assist_data)
                              <option value="{{$willing_to_assist_data}}">{{$willing_to_assist_data}}</option>
                              @endforeach
                           </select>
                         </div>
                        </div>
                     </div>
                   </div>
                   <div class="row">
                     <div class="col-md-6 col-sm-12">
                        <div class="form-group label-floating">
                           <label class="control-label">{{$formText->describe_yourself}}</label>
                           <textarea class="form-control" name="describe_yourself" placeholder="" rows="6" style="height: 100px;">{{old('describe_yourself')}}</textarea>
                        </div>
                     </div>
                       <div class="col-md-6 col-sm-12">
                        <div class="form-group label-floating">
                           <label class="control-label">{{$formText->why_become_member}}</label>
                           <textarea class="form-control" name="why_become_member" placeholder="" rows="10" style="height: 100px;">{{old('why_become_member')}}</textarea>
                        </div>
                     </div>
                    </div>
                  <div class="row">
                     <div class="col-md-6 col-sm-12">
                        <div class="form-group label-floating">
                           <label class="control-label">{{$formText->communication_letter}}</label>
                           <textarea class="form-control" name="communication_letter" placeholder="" rows="10" style="height: 100px;">{{old('communication_letter')}}</textarea>
                        </div>
                     </div>
                     <div class="col-md-6 col-sm-12">
                       <div class="form-group label-floating">
                           <label class="control-label">{{$formText->video_url}}</label>
                           <textarea class="form-control" style="height: 100px;" name="video_url" placeholder="{{$formText->video_url_hint}}">{{old('video_url')}}</textarea>
                        </div>
                     </div>
                   </div>
                   <div class="row">
                     <div class="col-md-6 col-sm-12">
                         <div class="form-group label-floating">
                           <label class="control-label">{{$formText->add_photo}}</label><small>  {{$formText->first_photo}}</small><br>
                           <!-- <input class="form-control" type="file" multiple="multiple" name="pictures[]" placeholder=""> -->
                           <span class="btn btn-success btn-success2 fileinput-button">
                              <i class="fa fa-plus"></i>
                              <span>{{$formText->add_file}}</span>
                              <input type="file" name="picture[]" multiple="multiple">
                           </span>
                        </div>
                     </div>
                  </div>
                   <div class="row">
                     <div class="col-md-6 col-sm-12">
                        <div class="g-recaptcha"
                           data-sitekey="{{env('GOOGLE_RECAPTCHA_KEY','6LeP3eMUAAAAAE1Nk-Z2v-JGxUUwwOfRzPwzRifL')}}">
                        </div>
                     </div>
                  </div>
                  <div class="row" style="margin-top: 10px;">
                    <div class="col-md-12">
                  <button type="submit" class="search_btn btn btn-block">{{$formText->complete_signup}}</button>
                </div>
              </div>
               </form>
            </div>
         </div>
         <!--======= // log_in_page =======-->
      </div>
      <!-- <div class="container col-md-6">
         <img src="{{asset('assets/img/banner-11.jpg')}}">
         
         </div> -->
   </div>
</section>
@endsection
@section('additional-js')

<script type="text/javascript" src="{{asset('assets/assets/bootstrap-datepicker.js')}}"></script>
<script type="text/javascript" src="{{asset('assets/assets/bootstrap-datepicker.init.js')}}"></script>
<script type="text/javascript" src="{{asset('assets/assets/signup.js')}}"></script>
<script type="text/javascript" src="{{asset('assets/assets/bootstrap-multiselect.js')}}"></script>
<link rel="stylesheet" type="text/css" href="{{asset('assets/assets/datepicker3.css')}}">
<link rel="stylesheet" type="text/css" href="{{asset('assets/assets/bootstrap-multiselect.css')}}">
<link rel="stylesheet" type="text/css" href="{{asset('assets/assets/member_generic.css')}}">
<script src='https://www.google.com/recaptcha/api.js'></script>
@endsection

