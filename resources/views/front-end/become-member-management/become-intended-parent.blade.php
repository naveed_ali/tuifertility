@extends('front-end.layouts.master')
@section('content')
<section class="banner padding-top-170px padding-bottom-30px sm-ptb-0px" style="background-image: url({{asset('assets/img/banner_1.jpg')}});">
   <div id="page-title" class="margin-mob">
      <div class="container">
         <ol class="breadcrumb opacity-5">
            <li><a href="{{url('/')}}">Home</a></li>
            <li class="active">Sing Up</li>
         </ol><br>
         <h1 class="font-weight" style="color: #007cff;">
            Intended Parent Registration
         </h1><br>
         <p style="font: 14px/26px Raleway; line-height: 26px; color: #6b6c6c; line-height: 24px;"><em>Your information stays confidential ( photos, name, contacts won't be displayed in search). For this program, you can gain USD 80 - 150</em></p>
         @if ($errors->any())
         <div class="alert alert-danger">
            <ul>
               @foreach ($errors->all() as $error)
               <li>{{ $error }}</li>
               @endforeach
            </ul>
         </div>
         @endif
      </div>
   </div>
   <hr>
   <div class="">
      <div class="container margin-bottom-100">
         <!--======= log_in_page =======-->
         <div id="log-in" class=" .padding-top-10 site-form border-radius-10">
            <div class="form-output">
               <form method="post" action="{{route('intended_parents.store')}}" enctype="multipart/form-data">
                  @csrf
                  <input type="hidden" name="category" value="{{$category}}">
                  <div class="row">
                     <div class="col-md-6 col-sm-12">
                        <div class="form-group label-floating">
                           <label class="control-label">Your Email*</label>
                           <input class="form-control" placeholder="" name="email" type="email" value="{{old('email')}}">
                        </div>
                     </div>
                     <div class="col-md-6 col-sm-12">
                        <div class="form-group label-floating">
                           <label class="control-label">Full Name*</label>
                           <input class="form-control" name="name" placeholder="" type="text" value="{{old('name')}}">
                        </div>
                     </div>
                  </div>
                   <div class="row">
                     <div class="col-md-12 col-sm-12">
                     <div class="form-group label-floating">
                       <label class="control-label">Birthday (mo/day/yr)*</label>
                       <input class="form-control" name="birthdate" placeholder="Your Birthday" id="date_of_birth" type="text" value="{{old('birthdate')}}">
                     </div>
                     </div>
                     
                     </div>
                  <div class="row">
                    <div class="col-md-6 col-sm-12">
                        <div class="form-group label-floating is-select">
                           <label class="control-label">Country</label>
                           <select class="selectpicker form-control" name="country">
                              @foreach($countries as $country)
                              <option value="{{$country->country_name}}">{{$country->country_name}}</option>
                              @endforeach
                           </select>
                        </div>
                    </div>
                     <div class="col-md-6 col-sm-12">
                        <div class="form-group label-floating">
                           <label class="control-label">Address</label>
                           <input class="form-control" name="address" placeholder="Address" type="text" value="{{old('address')}}">
                        </div>
                     </div>
                     
                  </div>
                  <div class="row">
                     
                     <div class="col-md-6 col-sm-12">
                        <div class="form-group label-floating">
                           <label class="control-label">Phone/WhatsApp/Viber/Telegram</label>
                           <input class="form-control" name="phone" placeholder="Phone/WhatsApp/Viber/Telegram" type="text" value="{{old('phone')}}">
                        </div>
                     </div>
                     <div class="col-md-6 col-sm-12">
                        <div class="form-group label-floating is-select">
                           <label class="control-label">Race*</label>
                           <select class="selectpicker form-control" name="race">
                              <option value="">Please Select</option>
                              <option value="African">African</option>
                              <option value="American-Indian">American Indian</option>
                              <option value="Asian (Chinese)">Asian (Chinese)</option>
                              <option value="Asian (Japanese)">Asian (Japanese)</option>
                              <option value="Asian (other)">Asian (other)</option>
                              <option value="Caucasian">Caucasian</option>
                              <option value="Hispanic">Hispanic</option>
                              <option value="Middle Eastern">Middle Eastern</option>
                              <option value="Pacific Islander">Pacific Islander</option>
                              <option value="Other">Other</option>
                           </select>
                        </div>
                     </div>
                   </div>
                   <div class="row">
                     <div class="col-md-6 col-sm-12">
                        <div class="form-group label-floating is-select">
                           <label class="control-label">Religion</label>
                           <select class="selectpicker form-control" name="religion">
                              <option value="">Please Select</option>
                              <option value="African Traditional &amp; Diasporic">African Traditional &amp; Diasporic</option>
                              <option value="Agnostic">Agnostic</option>
                              <option value="Atheist">Atheist</option>
                              <option value="Baha'i">Baha'i</option>
                              <option value="Buddhism">Buddhism</option>
                              <option value="Cao Dai">Cao Dai</option>
                              <option value="Chinese traditional religion">Chinese traditional religion</option>
                              <option value="Christianity">Christianity</option>
                              <option value="Hinduism">Hinduism</option>
                              <option value="Islam">Islam</option>
                              <option value="Jainism">Jainism</option>
                              <option value="Juche">Juche</option>
                              <option value="Judaism">Judaism</option>
                              <option value="Neo-Paganism">Neo-Paganism</option>
                              <option value="Nonreligious">Nonreligious</option>
                              <option value="Other">Other</option>
                              <option value="primal-indigenous">primal-indigenous</option>
                              <option value="Rastafarianism">Rastafarianism</option>
                              <option value="Secular">Secular</option>
                              <option value="Shinto">Shinto</option>
                              <option value="Sikhism">Sikhism</option>
                              <option value="Spiritism">Spiritism</option>
                              <option value="Tenrikyo">Tenrikyo</option>
                              <option value="Unitarian-Universalism">Unitarian-Universalism</option>
                              <option value="Zoroastrianism">Zoroastrianism</option>
                           </select>
                        </div>
                     </div>
                     <div class="col-md-6 col-sm-12">
                        <div class="form-group label-floating is-select">
                           <label class="control-label">Education</label>
                           <select class="selectpicker form-control" name="education">
                              <option value="">Please Select</option>
                              <option value="Never Finished High School">Never Finished High School</option>
                              <option value="Completed High School">Completed High School</option>
                              <option value="Completed College or University">Completed College or University</option>
                              <option value="Completed College or University">Completed Master Degree</option>
                           </select>
                        </div>
                     </div>
                   </div>
                  <div class="row">
                    <div class="col-md-6 col-sm-12">
                        <div class="form-group label-floating is-select">
                           <label class="control-label">Spoken Languages</label>
                           <div class="input-group">
                            <select class="form-control jq_multiselect" id="spoken_languages" enable_select_all="false" name="spoken_language[]" multiple="multiple" >
                              <option value="Abkhaz">Abkhaz</option>
                               <option value="Afrikaans">Afrikaans</option>
                               <option value="Albanian">Albanian</option>
                               <option value="Amharic">Amharic</option>
                               <option value="Arabic">Arabic</option>
                               <option value="Armenian">Armenian</option>
                               <option value="Assamese">Assamese</option>
                               <option value="Aymara">Aymara</option>
                               <option value="Bajan">Bajan</option>
                               <option value="Basque">Basque</option>
                               <option value="Belarusian">Belarusian</option>
                               <option value="Bengali">Bengali</option>
                               <option value="Bhojpuri">Bhojpuri</option>
                               <option value="Bislama">Bislama</option>
                               <option value="Bosnian">Bosnian</option>
                               <option value="Bulgarian">Bulgarian</option>
                               <option value="Burmese">Burmese</option>
                               <option value="Catalan">Catalan</option>
                               <option value="Chinese">Chinese</option>
                               <option value="Croatian">Croatian</option>
                               <option value="Czech">Czech</option>
                               <option value="Danish">Danish</option>
                               <option value="Dhivehi">Dhivehi</option>
                               <option value="Dutch">Dutch</option>
                               <option value="Dzongkha">Dzongkha</option>
                               <option value="English">English</option>
                               <option value="Estonian">Estonian</option>
                               <option value="Fijian">Fijian</option>
                               <option value="Filipino">Filipino</option>
                               <option value="Finnish">Finnish</option>
                               <option value="French">French</option>
                               <option value="Georgian">Georgian</option>
                               <option value="German">German</option>
                               <option value="Greek">Greek</option>
                               <option value="Guarani">Guarani</option>
                               <option value="Gujarati">Gujarati</option>
                               <option value="Haitian Creole">Haitian Creole</option>
                               <option value="Hebrew">Hebrew</option>
                               <option value="Hindi">Hindi</option>
                               <option value="Hiri Motu">Hiri Motu</option>
                               <option value="Hungarian">Hungarian</option>
                               <option value="Icelandic">Icelandic</option>
                               <option value="Indonesian">Indonesian</option>
                               <option value="Inuktitut">Inuktitut</option>
                               <option value="Italian">Italian</option>
                               <option value="Japanese">Japanese</option>
                               <option value="Kannada">Kannada</option>
                               <option value="Kashmiri">Kashmiri</option>
                               <option value="Kazakh">Kazakh</option>
                               <option value="Khmer">Khmer</option>
                               <option value="Korean">Korean</option>
                               <option value="Kurdish">Kurdish</option>
                               <option value="Lao">Lao</option>
                               <option value="Latvian">Latvian</option>
                               <option value="Lithuanian">Lithuanian</option>
                               <option value="Luxembourgish">Luxembourgish</option>
                               <option value="Macedonian">Macedonian</option>
                               <option value="Malagasy">Malagasy</option>
                               <option value="Malay">Malay</option>
                               <option value="Malayalam">Malayalam</option>
                               <option value="Maltese">Maltese</option>
                               <option value="Manx Gaelic">Manx Gaelic</option>
                               <option value="Maori">Maori</option>
                               <option value="Marathi">Marathi</option>
                               <option value="Mayan">Mayan</option>
                               <option value="Mongolian">Mongolian</option>
                               <option value="Nepali">Nepali</option>
                               <option value="Norwegian">Norwegian</option>
                               <option value="Occitan">Occitan</option>
                               <option value="Oriya">Oriya</option>
                               <option value="Ossetian">Ossetian</option>
                               <option value="Papiamento">Papiamento</option>
                               <option value="Pashto">Pashto</option>
                               <option value="Persian">Persian</option>
                               <option value="Polish">Polish</option>
                               <option value="Portuguese">Portuguese</option>
                               <option value="Punjabi">Punjabi</option>
                               <option value="Quechua">Quechua</option>
                               <option value="Rhaeto-Romansh">Rhaeto-Romansh</option>
                               <option value="Romanian">Romanian</option>
                               <option value="Russian">Russian</option>
                               <option value="Sanskrit">Sanskrit</option>
                               <option value="Serbian">Serbian</option>
                               <option value="Sindhi">Sindhi</option>
                               <option value="Sinhala">Sinhala</option>
                               <option value="Slovak">Slovak</option>
                               <option value="Slovene">Slovene</option>
                               <option value="Somali">Somali</option>
                               <option value="Sotho">Sotho</option>
                               <option value="Spanish">Spanish</option>
                               <option value="Swahili">Swahili</option>
                               <option value="Swedish">Swedish</option>
                               <option value="Tajik">Tajik</option>
                               <option value="Tamil">Tamil</option>
                               <option value="Telugu">Telugu</option>
                               <option value="Thai">Thai</option>
                               <option value="Tswana">Tswana</option>
                               <option value="Turkish">Turkish</option>
                               <option value="Turkmen">Turkmen</option>
                               <option value="Ukrainian">Ukrainian</option>
                               <option value="Urdu">Urdu</option>
                               <option value="Uzbek">Uzbek</option>
                               <option value="Venda">Venda</option>
                               <option value="Vietnamese">Vietnamese</option>
                               <option value="Welsh">Welsh</option>
                               <option value="Yiddish">Yiddish</option>
                               <option value="Zulu">Zulu</option>
                            </select>
                        </div>
                     </div>
                   </div>
                     <div class="col-md-6 col-sm-12">
                        <div class="form-group label-floating is-select">
                           <label class="control-label">Smoker</label>
                           <select class="selectpicker form-control" name="smoker">
                              <option value="0">Yes</option>
                              <option value="1">No</option>
                           </select>
                        </div>
                     </div>
                   </div>
                  <div class="row">
                     <div class="col-md-6 col-sm-12">
                        <div class="form-group label-floating is-select">
                           <label class="control-label">Willing To Travel</label>
                           <select class="selectpicker form-control" name="willing_to_travel">
                              <option value="">Please Select</option>
                              <option value="Yes">Yes</option>
                              <option value="No">No</option>
                           </select>
                        </div>
                     </div>
                     <div class="col-md-6 col-sm-12">
                        <div class="form-group label-floating is-select">
                           <label class="control-label">Number of Children(non-donor)</label>
                           <select class="selectpicker form-control" name="number_of_children">
                              <option value="">Please Select</option>
                              <option value="0">0</option>
                              <option value="1">1</option>
                              <option value="2">2</option>
                              <option value="3">3</option>
                              <option value="4">4 or more</option>
                           </select>
                        </div>
                     </div>
                  </div>
                
                  <div class="row">
                     <div class="col-md-6 col-sm-12">
                        <div class="form-group label-floating is-select">
                           <label class="control-label">Explain Your Couple Type*</label>
                           <div class="input-group">
                           <select class="selectpicker form-control" name="your_couple_type">
                              <option value="Gay Couple">Gay Couple</option>
                              <option value="Heterosexual Couple">Heterosexual Couple</option>
                              <option value="Lesbian Couple">Lesbian Couple</option>
                              <option value="Single Man">Single Man</option>
                              <option value="Single Woman">Single Woman</option>
                           </select>
                         </div>
                        </div>
                     </div>
                     <div class="col-md-6 col-sm-12">
                        <div class="form-group label-floating is-select">
                           <label class="control-label">Marital Status</label>
                           <select class="selectpicker form-control" name="marital_status">
                              <option value="">Please Select</option>
                              <option value="Divorced">Divorced</option>
                              <option value="Married">Married</option>
                              <option value="Separated">Separated</option>
                              <option value="Single (No partner)">Single (No partner)</option>
                              <option value="Single (With partner)">Single (With partner)</option>
                           </select>
                        </div>
                     </div>
                   </div>
                   <div class="row">
                     <div class="col-md-6 col-sm-12">
                        <div class="form-group label-floating is-select">
                           <label class="control-label">Number of Surrogate Children</label>
                           <select class="selectpicker form-control" name="number_of_surrogate_children">
                              <option value="">Please Select</option>
                              <option value="0">0</option>
                              <option value="1">1</option>
                              <option value="2">2</option>
                              <option value="3">3</option>
                              <option value="4">4 or more</option>
                           </select>
                        </div>
                     </div>
                     <div class="col-md-6 col-sm-12">
                        <div class="form-group label-floating is-select">
                           <label class="control-label">Have you had a Surrogate/Donor Before?</label>
                           <div class="input-group">
                           <select class="selectpicker form-control" name="had_surrogate_before" >
                              <option value="Yes">Yes</option>
                              <option value="No">No</option>
                           </select>
                         </div>
                        </div>
                     </div>
                   </div>
                   <div class="row">
                     <div class="col-md-6 col-sm-12">
                        <div class="form-group label-floating is-select">
                           <label class="control-label">What is your budget for surrogacy/donor expenses?</label>
                           <select class="selectpicker form-control" name="budget">
                              <option value="">Please Select</option>
                              <option value="Dont't Know">Don't Know</option>
                              <option value="Upto 5,000 USD">Upto 5,000 USD</option>
                              <option value="5,000 - 10,000 USD">5,000 - 10,000 USD</option>
                              <option value="10,0000 - 20,000 USD">10,0000 - 20,000 USD</option>
                           </select>
                        </div>
                     </div>
                     <div class="col-md-6 col-sm-12">
                        <div class="form-group label-floating is-select">
                           <label class="control-label">You are Looking to connect with:</label>
                           <div class="input-group">
                           <select class="selectpicker form-control jq_multiselect" multiple="multiple" name="looking_to_connect_with[]" >
                              <option value="Surrogate Mothers">Surrogate Mothers</option>
                              <option value="Sperm Donors">Sperm Donors</option>
                              <option value="Egg Donors">Egg Donors</option>
                           </select>
                         </div>
                        </div>
                     </div>
                   </div>
                   <div class="row">
                     <div class="col-md-6 col-sm-12">
                        <div class="form-group label-floating is-select">
                           <label class="control-label">You prefer Surrogates/Donors between the ages of?</label>
                           <select class="selectpicker form-control" name="prefer_surrogate_age">
                              <option value="">Please Select</option>
                              <option value="18-25">18-25</option>
                              <option value="25-35">25-35</option>
                              <option value="35-45">35-45</option>
                              <option value="45+">45+</option>
                           </select>
                        </div>
                     </div>
                     <div class="col-md-6 col-sm-12">
                        <div class="form-group label-floating is-select">
                           <label class="control-label">You prefer Surrogates/Donors from which country?*</label>
                           <div class="input-group">
                           <select class="selectpicker form-control jq_multiselect" multiple="multiple" name="prefer_surrogate_country[]" >
                              @foreach($countries as $country)
                              <option value="{{$country->country_name}}">{{$country->country_name}}</option>
                              @endforeach
                           </select>
                         </div>
                        </div>
                     </div>
                   </div>

                   <div class="row">
                    <div class="col-md-6 col-sm-12">
                        <div class="form-group label-floating is-select">
                           <label class="control-label">You prefer Surrogates/Donors of which nationality?</label>
                           <select class="selectpicker form-control jq_multiselect" name="prefer_surrogate_nationality[]" multiple="multiple">
                              @foreach($countries as $country)
                              <option value="{{$country->nationality}}">{{$country->nationality}}</option>
                              @endforeach
                           </select>
                        </div>
                     </div>
                     <div class="col-md-6 col-sm-12">
                        <div class="form-group label-floating">
                           <label class="control-label">How would you best describe yourself?</label>
                           <textarea class="form-control" name="describe_yourself" placeholder="" rows="6" style="height: 100px;">{{old('describe_yourself')}}</textarea>
                        </div>
                     </div>
                    </div>

                  <div class="row">
                    <div class="col-md-6 col-sm-12">
                        <div class="form-group label-floating">
                           <label class="control-label">Explain your reasons for wanting a surrogate/donor:</label>
                           <textarea class="form-control" name="reason_for_wanting_surrogate" placeholder="" rows="10" style="height: 100px;">{{old('reason_for_wanting_surrogate')}}</textarea>
                        </div>
                     </div>
                     <div class="col-md-6 col-sm-12">
                        <div class="form-group label-floating">
                           <label class="control-label">Use this space to communicate a letter to potential surrogate/Donors.</label>
                           <textarea class="form-control" name="communication_letter" placeholder="" rows="10" style="height: 100px;">{{old('communication_letter')}}</textarea>
                        </div>
                     </div>
                     
                   </div>
                   <div class="row">
                     <div class="col-md-6 col-sm-12">
                        <div class="form-group label-floating">
                           <label class="control-label">Add Photo</label><br>
                           <!-- <input class="form-control" type="file" multiple="multiple" name="pictures[]" placeholder=""> -->
                           <span class="btn btn-success btn-success2 fileinput-button">
                              <i class="fa fa-plus"></i>
                              <span>Add file...</span>
                              <input type="file" name="picture">
                           </span>
                        </div>
                     </div>
                  </div>
                   <div class="row">
                     <div class="col-md-6 col-sm-12">
                        <div class="g-recaptcha"
                           data-sitekey="{{env('GOOGLE_RECAPTCHA_KEY','6LeP3eMUAAAAAE1Nk-Z2v-JGxUUwwOfRzPwzRifL')}}">
                        </div>
                     </div>
                  </div>
                  <div class="row" style="margin-top: 10px;">
                    <div class="col-md-12">
                  <button type="submit" class="search_btn btn btn-block">Complete sign up !</button>
                </div>
              </div>
               </form>
            </div>
         </div>
         <!--======= // log_in_page =======-->
      </div>
      <!-- <div class="container col-md-6">
         <img src="{{asset('assets/img/banner-11.jpg')}}">
         
         </div> -->
   </div>
</section>
@endsection
@section('additional-js')

<script type="text/javascript" src="{{asset('assets/assets/bootstrap-datepicker.js')}}"></script>
<script type="text/javascript" src="{{asset('assets/assets/bootstrap-datepicker.init.js')}}"></script>
<script type="text/javascript" src="{{asset('assets/assets/signup.js')}}"></script>
<script type="text/javascript" src="{{asset('assets/assets/bootstrap-multiselect.js')}}"></script>
<link rel="stylesheet" type="text/css" href="{{asset('assets/assets/datepicker3.css')}}">
<link rel="stylesheet" type="text/css" href="{{asset('assets/assets/bootstrap-multiselect.css')}}">
<link rel="stylesheet" type="text/css" href="{{asset('assets/assets/member_generic.css')}}">
<script src='https://www.google.com/recaptcha/api.js'></script>
@endsection

