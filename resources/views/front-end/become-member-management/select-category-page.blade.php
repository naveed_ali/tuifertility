@extends('front-end.layouts.member-master')
@section('additional-head')
<style>
    .sliderthumbs {

        background: #007cff;
        -webkit-border-radius: 9px;
        -moz-border-radius: 9px;
        border-radius: 9px;
        transition: all .1s;
        -webkit-box-shadow: 0 5px 0 #e5e2e2;
        -moz-box-shadow: 0 5px 0 #e5e2e2;
        box-shadow: 0 5px 0 #e5e2e2;
        cursor: pointer;
        padding: 20px 10px;
        font: 21px Raleway;
        text-align: center;
        color: white;

    }

    #homebanner,
    .sliderthumbs {

        -webkit-transition: all .1s;

    }

    .mynavbar,
    .sliderthumbs {

        -moz-transition: all .1s;

    }

    .sliderthumbs span {

        font-size: 13px;
        display: block;

    }

    button,
    input[type="submit"],
    input[type="reset"] {
        background: none;
        color: inherit;
        border: none;
        padding: 0;
        font: inherit;
        cursor: pointer;
        outline: inherit;
        font-size: 22px;
    }
</style>
@endsection
@section('content')
<section class="banner padding-tb-190px sm-ptb-80px background-overlay"
    style="background-image: url({{asset('assets/img/banner_1.jpg')}});">
    <div class="container  z-index-2 position-relative" style="margin-top: 23px;">
        <div class="row">
            <div class="col-6 col-sm-6 col-md-6 col-lg-6 padding-tb-23px hvr-float" style="margin: 0 auto;">
                <form method="get" action="{{route('become.member','surrogate-mothers')}}">
                <button type="submit" class="search_btn btn btn-block">
                    {{$button->surrogate_mother}}
                </button>
            </form>
            </div>
            <!-- </div> -->
        </div>
        <div class="row">
            <div class="col-6 col-sm-6 col-md-6 col-lg-6 padding-tb-10px hvr-float" style="margin: 0 auto;">
                 <form method="get" action="{{route('become.member','egg-donors')}}">
                <button type="submit" class="search_btn btn btn-block">
                    {{$button->egg_donor}}
                </button>
            </form>
                
            </div>
        </div>
        <div class="row">
            <div class="col-6 col-sm-6 col-md-6 col-lg-6 padding-tb-10px hvr-float" style="margin: 0 auto;">
                 <form method="get" action="{{route('become.member','sperm-donors')}}">
                <button type="submit" class="search_btn btn btn-block">
                    {{$button->sperm_donor}} 
                </button>
            </form>
                
            </div>
            <!-- </div> -->
        </div>
        <div class="row">
            <div class="col-6 col-sm-6 col-md-6 col-lg-6 padding-tb-10px hvr-float" style="margin: 0 auto;">
                 <form method="get" action="{{route('become.member','fertility-clinics')}}">
                <button type="submit" class="search_btn btn btn-block">
                    {{$button->fertility_clinic}}
                </button>
            </form>
                
            </div>
        </div>
    </div>
</section>
@endsection