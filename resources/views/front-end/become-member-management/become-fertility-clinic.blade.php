

@extends('front-end.layouts.member-master')
@section('content')
<section class="banner padding-top-200px padding-bottom-30px sm-ptb-0px" style="background-image: url({{asset('assets/img/banner_1.jpg')}});">
   <div id="page-title" class="margin-mob">
      <div class="container">
         <ol class="breadcrumb opacity-5">
            <li><a href="#">Home</a></li>
            <li class="active">Sing Up</li>
         </ol><br>
         <h1 class="font-weight" style="color: #007cff;">
            {{$formText->title}}
         </h1><br>
         <p style="font: 14px/26px Raleway; line-height: 26px; color: #6b6c6c; line-height: 24px;"><em>{{$formText->note}}</em></p>
         @if ($errors->any())
         <div class="alert alert-danger">
            <ul>
               @foreach ($errors->all() as $error)
               <li>{{ $error }}</li>
               @endforeach
            </ul>
         </div>
         @endif
      </div>
   </div>
   <hr>
   <div class="">
      <div class="container margin-bottom-0px">
         <!--======= log_in_page =======-->
         <div id="log-in" class=" .padding-top-10 site-form border-radius-10">
            <div class="form-output">
               <form method="post" action="{{route('fertility_clinic_members.store')}}" enctype="multipart/form-data">
                  @csrf
                  <input type="hidden" name="category" value="{{$category}}">
                  <div class="row">
                     <div class="col-md-6 col-sm-12">
                        <div class="form-group label-floating">
                           <label class="control-label">{{$formText->clinic_name}}</label>
                           <input class="form-control" placeholder="" name="clinic_name" type="text" value="{{old('clinic_name')}}">
                        </div>
                     </div>
                     <div class="col-md-6 col-sm-12">
                        <div class="form-group label-floating">
                           <label class="control-label">{{$formText->email}}</label>
                           <input class="form-control" name="email" placeholder="" type="email" value="{{old('email')}}">
                        </div>
                     </div>
                  </div>
                  <div class="row">
                     <div class="col-md-6 col-sm-12">
                        <div class="form-group label-floating">
                           <label class="control-label">{{$formText->name}}</label>
                           <input class="form-control" name="name" placeholder="" type="text" value="{{old('name')}}">
                        </div>
                     </div>
                     <div class="col-md-6 col-sm-12">
                        <div class="form-group label-floating is-select">
                           <label class="control-label">{{$formText->country}}</label>
                           <select class="selectpicker form-control" name="country">
                              <option value="">Please Select {{$formText->country}}</option>
                              @foreach($countries as $country)
                              <option value="{{$country->country_name}}">{{$country->country_name}}</option>
                              @endforeach
                           </select>
                        </div>
                     </div>
                  </div>
                  <div class="row">
                     <div class="col-md-6 col-sm-12">
                        <div class="form-group label-floating">
                           <label class="control-label">{{$formText->address}}</label>
                           <input class="form-control" name="address" placeholder="{{$formText->address}}" type="text" value="{{old('address')}}">
                        </div>
                     </div>
                     <div class="col-md-6 col-sm-12">
                        <div class="form-group label-floating">
                           <label class="control-label">{{$formText->phone}}</label>
                           <input class="form-control" name="phone" placeholder="{{$formText->phone}}" type="text" value="{{old('phone')}}">
                        </div>
                     </div>
                  </div>
                  <div class="row">
                     <div class="col-md-6 col-sm-12">
                        <div class="form-group label-floating">
                           <label class="control-label">{{$formText->website}}</label>
                           <input class="form-control" name="website" placeholder="" type="text" value="{{old('website')}}">
                        </div>
                     </div>
                     <div class="col-md-6 col-sm-12">
                        <div class="form-group label-floating is-select">
                           <label class="control-label">{{$formText->provide_services_for}}</label>
                           <div class="input-group">
                              <select class="selectpicker form-control jq_multiselect" enable_select_all="false" multiple="multiple" name="provide_services_for[]" required>
                                 @foreach(explode(',',$formText->provide_services_for_data) as $key => $provide_services_for_data)
                              <option value="{{$provide_services_for_data}}">{{$provide_services_for_data}}</option>
                              @endforeach
                              </select>
                           </div>
                        </div>
                     </div>
                  </div>
                  <div class="row">
                     <div class="col-md-6 col-sm-12">
                        <div class="form-group label-floating is-select">
                           <label class="control-label">{{$formText->treatments}}</label>
                           <div class="input-group">
                              <select class="selectpicker form-control jq_multiselect" multiple="multiple" enable_select_all="false" required  name="treatments[]">
                                 @foreach(explode(',',$formText->treatments_data) as $key => $treatments_data)
                              <option value="{{$treatments_data}}">{{$treatments_data}}</option>
                              @endforeach
                              </select>
                           </div>
                        </div>
                     </div>
                     <div class="col-md-6 col-sm-12">
                        <div class="form-group label-floating is-select">
                           <label class="control-label">{{$formText->willing_to_assist}}</label>
                           <div class="input-group">
                              <select class="selectpicker form-control spoken_language jq_multiselect" name="willing_to_assist[]" required multiple="multiple">
                                 @foreach(explode(',',$formText->willing_to_assist_data) as $key => $willing_to_assist_data)
                              <option value="{{$willing_to_assist_data}}">{{$willing_to_assist_data}}</option>
                              @endforeach
                              </select>
                           </div>
                        </div>
                     </div>
                  </div>
                  <div class="row">
                     <div class="col-md-6 col-sm-12">
                        <div class="form-group label-floating is-select">
                           <label class="control-label">{{$formText->providing_service}}</label>
                           <div class="input-group">
                              <select class="selectpicker form-control spoken_language jq_multiselect" name="providing_service[]" multiple="multiple">
                                @foreach(explode(',',$formText->providing_service_data) as $key => $providing_service_data)
                              <option value="{{$providing_service_data}}">{{$providing_service_data}}</option>
                              @endforeach
                              </select>
                           </div>
                        </div>
                     </div>
                     <div class="col-md-6 col-sm-12">
                        <div class="form-group label-floating is-select">
                           <label class="control-label">{{$formText->willing_to_assist_countries}}</label>
                           <div class="input-group">
                              <select class="selectpicker form-control spoken_language jq_multiselect" name="willing_to_assist_countries[]" multiple="multiple">
                                 @foreach($countries as $country)
                                 <option value="{{$country->country_name}}">{{$country->country_name}}</option>
                                 @endforeach
                              </select>
                           </div>
                        </div>
                     </div>
                  </div>
                  <div class="row">
                     <div class="col-md-6 col-sm-12">
                        <div class="form-group label-floating">
                           <label class="control-label">{{$formText->describe_clinic}}</label>
                           <textarea class="form-control" name="describe_clinic" placeholder="" rows="6" style="height: 100px;">{{old('describe_clinic')}}</textarea>
                        </div>
                     </div>
                     <div class="col-md-6 col-sm-12">
                        <div class="form-group label-floating">
                           <label class="control-label">{{$formText->video_url}}</label>
                           <input class="form-control" placeholder="" name="video_url" type="text" style="height: 100px;" value="{{old('video_url')}}">
                        </div>
                     </div>
                  </div>
                  <div class="row">
                     <div class="col-md-6 col-sm-12">
                        <div class="form-group label-floating">
                           <label class="control-label">{{$formText->pictures}}</label><br>
                           <!-- <input class="form-control" type="file" multiple="multiple" name="pictures[]" placeholder=""> -->
                           <span class="btn btn-success btn-success2 fileinput-button">
                              <i class="fa fa-plus"></i>
                              <span>{{$formText->add_file}}</span>
                              <input type="file" name="pictures[]" multiple="multiple">
                           </span>
                        </div>
                     </div>
                  </div>
                  <div class="row">
                     <div class="col-md-6 col-sm-12">
                        <div class="g-recaptcha"
                           data-sitekey="{{env('GOOGLE_RECAPTCHA_KEY','6LeP3eMUAAAAAE1Nk-Z2v-JGxUUwwOfRzPwzRifL')}}">
                        </div>
                     </div>
                  </div>
                  <div class="row" style="margin-top: 10px;">
                     <div class="col-md-12">
                        <button type="submit" class="search_btn btn btn-block">{{$formText->complete_signup}}</button>
                     </div>
                  </div>
               </form>
            </div>
         </div>
         <!--======= // log_in_page =======-->
      </div>
      <!-- <div class="container col-md-6">
         <img src="{{asset('assets/img/banner-11.jpg')}}">
         
         </div> -->
   </div>
</section>
@endsection
@section('additional-js')
<script type="text/javascript" src="{{asset('assets/assets/bootstrap-datepicker.js')}}"></script>
<script type="text/javascript" src="{{asset('assets/assets/bootstrap-datepicker.init.js')}}"></script>
<script type="text/javascript" src="{{asset('assets/assets/signup.js')}}"></script>
<script type="text/javascript" src="{{asset('assets/assets/bootstrap-multiselect.js')}}"></script>
<link rel="stylesheet" type="text/css" href="{{asset('assets/assets/datepicker3.css')}}">
<link rel="stylesheet" type="text/css" href="{{asset('assets/assets/bootstrap-multiselect.css')}}">
<link rel="stylesheet" type="text/css" href="{{asset('assets/assets/member_generic.css')}}">
<script src='https://www.google.com/recaptcha/api.js'></script>
@endsection

