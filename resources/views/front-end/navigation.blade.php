<header>
   <div id="fixed-header-dark" class="header-output fixed-header box-shadow">
      <div class="container"> <!-- header-in -->
         <!-- <div class="container-fluid"> -->
         <div class="row">
               <div class="col-sm-12 col-md-6 col-lg-6 order2">
                  @if(url()->current()!==url('/'))
                  <a id="logo2" href="{{url('/')}}" class="d-inline-block align-text">
                     <img src="{{asset('assets/img/logo-tui.png')}}" alt="logo-tuilogo-tuilogo-tui" class="sm-left">
                  </a>
                  <a id="logo2" href="{{url('/')}}" class="d-inline-block align-text">
                     <img src="{{asset('assets/img/tui.png')}}" alt="logo-tuilogo-tui" class="mobile-logo">
                  </a>
                  <!-- mobile-logo -->
                  <button type="button" class="btn btn-primary search-blue-btn">
                     <div class="row">
                        <div class="col-sm-6 col-xs-6 looking-for">
                           I'm looking for <span class="fa fa-angle-down"></span> 
                        </div>
                        <div class="col-sm-6 col-xs-6 looking-for looking-for-search custom-dropdown">
                           <form method="get" action="{{route('search.form')}}">
                              <input type="hidden" name="category" id="category" value="surrogate-mothers">
                              <select class="customize-select" id="looking_for_select">
                                 <option value="surrogate-mothers" selected>SURROGATE MOTHERS</option>
                                 <option value="egg-donors">EGG DONOR</option>
                                 <option value="sperm-donors">SPERM DONOR</option>
                                 <option value="fertility-clinics">FERTILITY CLINIC</option>
                                 <option value="famous-sperm-donors">FAMOUS SPERM DONOR</option>
                                 <option value="famous-egg-donors">FAMOUS EGG DONOR</option>
                              </select>

                        </div>
                     </div>
                  </button>
                  <button type="submit" class="btn btn-light serch-white-btn btn-light2"> SEARCH NOW</button>
                  </form>
                  @endif
               </div>
               
               <div class="col-sm-12 col-md-6 col-lg-6 order1">
                  <div class="top-right" style="float: right;">
                     <!-- <ul class="clearfix" style="padding:0px;"> -->
                     @foreach($scouts as $scout)
                     <div class="display-flex">
                        <div class="align-top" style="font-weight: 400; line-height: 2;">
                           <div class="d-inline-block" style="line-height: 38px;">
                              <img src="{{asset('storage/'.$scout->picture)}}"
                                 class="contact-detail contact-photo ml-1 ml-sm-2 ml-lg-1" width="25" height="25"
                                 style="border-radius: 20px; ">
                              <span class="contact-detail contact-name ml-1 ml-sm-2 ml-lg-1">{{$scout->name}}</span>
                              <span class="contact-detail ml-1 ml-sm-2 contact-phone d-none d-lg-inline-block">
                                 ({{$scout->phone}})
                              </span>
                           </div>
                           <div class="d-inline-block" style="margin-left: 0 !important; float: right;">
                              @if($scout->telegram)
                                 <a href="{{$scout->telegram}}" target="_blank" class="d-inline-block ml-0 ml-lg-3">
                                    <span class="fab fa-telegram social-contact"></span>
                                 </a>
                              @endif
                              @if($scout->messenger)
                                 <a href="{{$scout->messenger}}" target="_blank" class="d-inline-block ml-1 ml-sm-2 ml-lg-2">
                                    <span class="fab fa-facebook-messenger social-contact"></span>
                                 </a>
                              @endif
                              @if($scout->whatsapp)
                                 <a href="{{$scout->whatsapp}}" target="_blank" class="d-inline-block ml-1 ml-sm-2 ml-lg-2">
                                    <span class="fab fa-whatsapp social-contact"></span>
                                 </a>
                              @endif
                              @if($scout->viber)
                                 <a href="{{$scout->viber}}" target="_blank" class="d-inline-block ml-1 ml-sm-2 ml-lg-2">
                                    <span class="fab fa-viber social-contact"></span>
                                 </a>
                              @endif
                           </div>
                        </div>
                     </div>
                     @endforeach
                  </div>
               </div>
         </div>
         <!-- </div> -->

         <div class="row apply-margin">
            <div class="col-lg-2 col-md-3 apply-margin-bottom">
               {{-- <a class="mobile-toggle padding-13px background-main-color" href="#"><i class="fas fa-bars"></i> Menu</a> --}}
               <a class="padding-13px background-main-color" id="navToggle" href="#"><i class="fas fa-bars"></i> Menu</a>

               <a id="logo2" href="{{url('/')}}" class="d-inline-block align-text" style="margin-top: 12px;"> 
                  <img src="{{asset('assets/img/logo-tui.png')}}" alt="logo-tui" class="smallhide">
               </a>
            </div>

            <div class="col-lg-10 col-md-9 no-gutters m-width">
               <nav style="position: relative;">
                  <ul id="menu-main" class="nav-menu link-padding-tb-20px" role="menu">
                     @foreach($menus as $menuitem)
                     <li class="has-dropdown" style="margin-right: 5px;">
                        <a href="#">{{$menuitem->name}}</a>
                        @if($menuitem->pages->count()>0)
                        <ul class="sub-menu" style="margin-right: 20px;right: 0; left: none;">
                           @foreach($menuitem->pages as $page)
                           <li style="position: relative;">
                              <a href="{{url('/'.$menuitem->slug.'/'.$page->slug)}}">{{$page->name}}</a>
                              @if($page->subPages->count()>0)
                              <ul class="sub-menu sub-menu2" style="position: absolute;">
                                 @foreach($page->subPages as $subpage)
                                 <li><a
                                       href="{{url('/'.$menuitem->slug.'/'.$page->slug.'/'.$subpage->slug)}}">{{$subpage->name}}</a>
                                 </li>
                                 @endforeach
                              </ul>
                              @endif
                           </li>
                           @endforeach
                        </ul>
                        @endif
                     </li>
                     @endforeach
                  </ul>
               </nav>
            </div>
            
         </div>
         <!--  START Mobile NAVBAR   -->
            <nav class="navbar navbar-expand-lg fixed-top navbar-dark navbar-offcanvas">
                <div class="navbar-collapse offcanvas-collapse" style="background-color: #0664b2;">


                    <ul class="navbar-nav mr-auto">
                     
                        <a id="navToggle" class="nav-toggle-a" href="#" style="" class="pt-1"> 
                           <i class="fas fa-times"></i>
                        </a>
                        <li class="nav-item">
                            <a class="nav-link" href="#">Switch account</a>
                        </li>
                        <li class="nav-item dropdown">
                            <a class="nav-link dropdown-toggle" href="https://example.com" id="dropdown01" data-toggle="dropdown"
                                aria-haspopup="true" aria-expanded="false">Main</a>
                            <div class="dropdown-menu" aria-labelledby="dropdown01">
                                <a class="dropdown-item" href="#">Single Page</a>
                                <a class="nav-link dropdown-toggle" href="https://example.com" id="dropdown01"
                                    data-toggle="dropdown" aria-haspopup="true" aria-expanded="false" style="color:#343a40;padding: .25rem 1.5rem;"> Dropdown Sub pages </a>

                                <div class="dropdown-menu" aria-labelledby="dropdown01">
                                    <a class="dropdown-item" href="#">Action</a>
                                    <a class="dropdown-item" href="#">Another action</a>
                                </div>
                                <a class="nav-link dropdown-toggle" href="https://example.com" id="dropdown01"
                                    data-toggle="dropdown" aria-haspopup="true" aria-expanded="false" style="color:#343a40;padding: .25rem 1.5rem;"> Dropdown Sub pages </a>

                                <div class="dropdown-menu" aria-labelledby="dropdown01">
                                    <a class="dropdown-item" href="#">Action</a>
                                    <a class="dropdown-item" href="#">Another action</a>
                                </div>
                            </div>
                        </li>
                    </ul>
                </div>
            </nav>
         <!--  END NAVBAR   -->
      </div>
   </div>
</header>