<!DOCTYPE html>
<html>
@include('front-end.partials.head')

<body>
   @include('front-end.partials.navigation')
   @include('front-end.partials.main-banner')
   @include('front-end.partials.footer')
   @include('front-end.partials.js-libraries')
</body>

</html>