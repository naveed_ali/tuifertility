@extends('front-end.layouts.master')

@section('content')
<section class="banner padding-tb-70px sm-ptb-80px" style="background-image: url({{asset('assets/img/banner_1.jpg')}});">
<div id="page-title" class="padding-tb-170px">
        <div class="container">
            <ol class="breadcrumb opacity-5">
                <li><a href="#">Home</a></li>
                <li class="active">Sing Up</li>
                <li class="active">Completed</li>
            </ol>
           
              @if ($errors->any())
    <div class="alert alert-danger">
        <ul>
            @foreach ($errors->all() as $error)
                <li>{{ $error }}</li>
            @endforeach
        </ul>
    </div>
@endif
        </div>
    </div>
    <div class="row">
    <div class="container margin-bottom-70px col-md-12" >
        <!--======= log_in_page =======-->
        <div>

          <h1 class="text-center">Please check your email now. Thank you</h1>
        </div>
        <!--======= // log_in_page =======-->

    </div>
    
</div>
</section>
@endsection