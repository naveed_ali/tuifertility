<section class="padding-tb-100px background-grey-1">
        <div class="container">
            <!-- Title -->
            <div class="row justify-content-center margin-bottom-45px">
                <div class="col-lg-10">
                    <div class="row">
                        <div class="col-md-3 wow fadeInUp">
                            <h1 class="text-second-color font-weight-300 text-sm-center text-lg-right margin-tb-15px">Our Blog</h1>
                        </div>
                        <div class="col-md-7 wow fadeInUp" data-wow-delay="0.2s">
                            <p class="text-grey-2">Lorem Ipsum is simply dummy text of the printing and typesetting industry. Lorem Ipsum has been the industry's standard dummy text ever since the 1500s</p>
                        </div>
                        <div class="col-md-2 wow fadeInUp" data-wow-delay="0.4s">
                            <a href="#" class="text-main-color margin-tb-15px d-inline-block"><span class="d-block float-left margin-right-10px margin-top-5px">Show All</span> <i class="far fa-arrow-alt-circle-right text-large margin-top-7px"></i></a>
                        </div>
                    </div>
                </div>
            </div>
            <!-- // Title -->

            <div class="row">
                <div class="col-lg-6 sm-mb-45px">
                    <div class="background-white thum-hover box-shadow hvr-float full-width wow fadeInUp">
                        <div class="float-md-left margin-right-30px thum-xs">
                            <img src="{{asset('assets/img/blog-1.jpg')}}" alt="">
                        </div>
                        <div class="padding-25px">
                            <i class="far fa-folder-open text-main-color"></i>
                            <a href="#" class="text-main-color">News </a> ,
                            <a href="#" class="text-main-color">Articles </a>
                            <h3><a href="#" class="d-block text-dark text-capitalize text-medium margin-tb-15px">Long Don’t Spend Time Beating On a Wall, Hoping To Trans ... </a></h3>
                            <span class="margin-right-20px text-extra-small"><i class="far fa-user text-grey-2"></i> By : <a href="#"> Rabie Elkheir</a></span>
                            <span class="text-extra-small d-block d-sm-none"><i class="far fa-clock text-grey-2"></i> Date :  <a href="#"> July 15, 2016</a></span>
                        </div>
                        <div class="clearfix"></div>
                    </div>
                </div>
                <div class="col-lg-6 sm-mb-45px">
                    <div class="background-white thum-hover box-shadow hvr-float full-width wow fadeInUp">
                        <div class="float-md-left margin-right-30px thum-xs">
                            <img src="{{asset('assets/img/blog-2.jpg')}}" alt="">
                        </div>
                        <div class="padding-25px">
                            <i class="far fa-folder-open text-main-color"></i>
                            <a href="#" class="text-main-color">News </a> ,
                            <a href="#" class="text-main-color">Articles </a>
                            <h3><a href="#" class="d-block text-dark text-capitalize text-medium margin-tb-15px">Long Don’t Spend Time Beating On a Wall, Hoping To Trans ... </a></h3>
                            <span class="margin-right-20px text-extra-small"><i class="far fa-user text-grey-2"></i> By : <a href="#"> Rabie Elkheir</a></span>
                            <span class="text-extra-small d-block d-sm-none"><i class="far fa-clock text-grey-2"></i> Date :  <a href="#"> July 15, 2016</a></span>
                        </div>
                        <div class="clearfix"></div>
                    </div>
                </div>
            </div>

        </div>
    </section>