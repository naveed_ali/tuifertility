 <section class="padding-tb-100px">
        <div class="container">
            <!-- Title -->
            <div class="row justify-content-center margin-bottom-45px">
                <div class="col-lg-10">
                    <div class="row">
                        <div class="col-md-4 wow fadeInUp">
                            <h1 class="text-second-color font-weight-300 text-sm-center text-lg-right margin-tb-15px">Popular Clinic</h1>
                        </div>
                        <div class="col-md-6 wow fadeInUp" data-wow-delay="0.2s">
                            <p class="text-grey-2">Lorem Ipsum is simply dummy text of the printing and typesetting industry. Lorem Ipsum has been the industry's standard dummy text ever since the 1500s</p>
                        </div>
                        <div class="col-md-2 wow fadeInUp" data-wow-delay="0.4s">
                            <a href="#" class="text-main-color margin-tb-15px d-inline-block"><span class="d-block float-left margin-right-10px margin-top-5px">Show All</span> <i class="far fa-arrow-alt-circle-right text-large margin-top-7px"></i></a>
                        </div>
                    </div>
                </div>
            </div>
            <!-- // Title -->


            <div class="row">

                <!-- clinic -->
                <div class="col-lg-4 col-md-6 sm-mb-45px">
                    <div class="background-white full-width thum-hover box-shadow hvr-float wow fadeInUp" data-wow-delay="0.2s">
                        <div class="item-thumbnail thum background-white">
                            <a href="#"><img src="{{asset('assets/img/clinic-1.jpg')}}" alt=""></a>
                        </div>
                        <div class="padding-30px">
                            <h5 class="margin-bottom-20px"><a class="text-dark" href="#">Alrayan Eye Clinic</a></h5>
                            <div class="rating clearfix">
                                <span class="float-left text-grey-2"><i class="far fa-map"></i>  California</span>
                                <ul class="float-right">
                                    <li class="active"></li>
                                    <li class="active"></li>
                                    <li class="active"></li>
                                    <li class="active"></li>
                                    <li></li>
                                </ul>
                            </div>
                        </div>
                        <div class="padding-lr-30px padding-tb-15px background-light-grey">
                            <div class="row no-gutters">
                                <div class="col-4"><a href="#" class="text-lime"><i class="far fa-bookmark"></i> Open Now!</a></div>
                                <div class="col-4 text-center"><a href="#" class="text-red"><i class="far fa-heart"></i> Save</a></div>
                                <div class="col-4 text-right"><a href="#" class="text-blue"><i class="far fa-hospital"></i> Hospital</a></div>
                            </div>
                        </div>
                    </div>
                </div>
                <!-- // clinic -->


                <!-- clinic -->
                <div class="col-lg-4 col-md-6 sm-mb-45px">
                    <div class="background-white full-width thum-hover box-shadow hvr-float wow fadeInUp" data-wow-delay="0.4s">
                        <div class="item-thumbnail thum background-white">
                            <a href="#"><img src="{{asset('assets/img/clinic-2.jpg')}}" alt=""></a>
                        </div>
                        <div class="padding-30px">
                            <h5 class="margin-bottom-20px"><a class="text-dark" href="#">Alrayan Eye Clinic</a></h5>
                            <div class="rating clearfix">
                                <span class="float-left text-grey-2"><i class="far fa-map"></i>  California</span>
                                <ul class="float-right">
                                    <li class="active"></li>
                                    <li class="active"></li>
                                    <li class="active"></li>
                                    <li class="active"></li>
                                    <li></li>
                                </ul>
                            </div>
                        </div>
                        <div class="padding-lr-30px padding-tb-15px background-light-grey">
                            <div class="row no-gutters">
                                <div class="col-4"><a href="#" class="text-lime"><i class="far fa-bookmark"></i> Open Now!</a></div>
                                <div class="col-4 text-center"><a href="#" class="text-red"><i class="far fa-heart"></i> Save</a></div>
                                <div class="col-4 text-right"><a href="#" class="text-blue"><i class="far fa-hospital"></i> Hospital</a></div>
                            </div>
                        </div>
                    </div>
                </div>
                <!-- // clinic -->


                <!-- clinic -->
                <div class="col-lg-4 col-md-6 sm-mb-45px">
                    <div class="background-white full-width thum-hover box-shadow hvr-float wow fadeInUp" data-wow-delay="0.6s">
                        <div class="item-thumbnail thum background-white">
                            <a href="#"><img src="{{asset('assets/img/clinic-3.jpg')}}" alt=""></a>
                        </div>
                        <div class="padding-30px">
                            <h5 class="margin-bottom-20px"><a class="text-dark" href="#">Alrayan Eye Clinic</a></h5>
                            <div class="rating clearfix">
                                <span class="float-left text-grey-2"><i class="far fa-map"></i>  California</span>
                                <ul class="float-right">
                                    <li class="active"></li>
                                    <li class="active"></li>
                                    <li class="active"></li>
                                    <li class="active"></li>
                                    <li></li>
                                </ul>
                            </div>
                        </div>
                        <div class="padding-lr-30px padding-tb-15px background-light-grey">
                            <div class="row no-gutters">
                                <div class="col-4"><a href="#" class="text-lime"><i class="far fa-bookmark"></i> Open Now!</a></div>
                                <div class="col-4 text-center"><a href="#" class="text-red"><i class="far fa-heart"></i> Save</a></div>
                                <div class="col-4 text-right"><a href="#" class="text-blue"><i class="far fa-hospital"></i> Hospital</a></div>
                            </div>
                        </div>
                    </div>
                </div>
                <!-- // clinic -->

            </div>

        </div>
    </section>