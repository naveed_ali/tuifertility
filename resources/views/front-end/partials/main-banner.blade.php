<section class="homepage_bg banner padding-top-200px padding-bottom-30px sm-ptb-0px background-overlay"
    style="background-image: url({{asset('assets/img/banner_111.jpg')}}); ">
    <div class="container z-index-2 position-relative">

        <div class="row">
            <div class="col-12 col-sm-7 col-md-6 col-lg-5 offset-lg-1 pt-m-65px padding-bottom-10px padding-top-50px">
                <form method="get" action="{{route('search.form','egg-donors')}}">
                    <input type="hidden" name="category" value="egg-donors">
                    <button type="submit" class="search_btn btn btn-block font_size">SEARCH NOW for egg donors</button>
                </form>
            </div>
        </div>
        <div class="row">
            <div class="col-12 col-sm-7 col-md-6 col-lg-5 offset-lg-1 padding-tb-10px">
                <form method="get" action="{{route('search.form','sperm-donors')}}">
                    <input type="hidden" name="category" value="sperm-donors">
                    <button type="submit" class="search_btn btn btn-block font_size">SEARCH NOW for sperm donors</button>
                </form>
            </div>
        </div>
        <div class="row">
            <div class="col-12 col-sm-7 col-md-6 col-lg-5 offset-lg-1 padding-tb-10px">
                <form method="get" action="{{route('search.form','fertility-clinics')}}">
                     <input type="hidden" name="category" value="fertility-clinics">
                    <button type="submit" class="search_btn btn btn-block font_size">SEARCH NOW for fertility clinics</button>
                </form>
            </div>
        </div>
        <div class="row">
            <div class="col-12 col-sm-8 col-md-6 col-lg-5 offset-lg-1 padding-tb-10px">
                <form method="get" action="{{route('search.form','surrogate-mothers')}}">
                    <input type="hidden" name="category" value="surrogate-mothers">
                    <button type="submit" class="search_btn btn btn-block font_size">SEARCH NOW for surrogate mothers</button>
                </form>
            </div>
        </div>
        <div class="row">
            <div class="col-12 col-sm-8 col-md-6 col-lg-5 offset-lg-1 padding-tb-10px">
                <form method="get" action="{{route('search.form','famous-egg-donors')}}">
                    <input type="hidden" name="category" value="famous-egg-donors">
                    <button type="submit" class="search_btn btn btn-block font_size">SEARCH NOW for famous egg donors</button>
                </form>
            </div>
        </div>
        <div class="row">
            <div class="col-12 col-sm-8 col-md-6 col-lg-5 offset-lg-1 padding-bottom-20px padding-top-10px">
                <form method="get" action="{{route('search.form','famous-sperm-donors')}}">
                    <input type="hidden" name="category" value="famous-sperm-donors">
                    <button type="submit" class="search_btn btn btn-block font_size">SEARCH NOW for famous sperm donors</button>
                </form>
            </div>
        </div>
        <!--<div class="row" style="margin-left: 11px;">
            <div class="col-lg-12 offset-lg-1 col-md-12 padding-top-50px padding-bottom-10px">
                 <form method="get" action="{{route('search.form','surrogate-mothers')}}">
                    <input type="hidden" name="category" value="surrogate-mothers">
                <button type="submit" class="btn btn-primary blue-btn font-style">
                    <span class="s_mother">I'm looking for SURROGATE MOTHERS</span>
                    <button type="submit" class="btn btn-light white-btn btn-light2 search-btn-style"> SEARCH NOW </button>
                </button>
            </form>
            </div>
        </div>
        <div class="row" style="margin-left: 11px;">
            <div class="col-lg-12 offset-lg-1 col-md-12 padding-tb-10px">
                 <form method="get" action="{{route('search.form','egg-donors')}}">
                    <input type="hidden" name="category" value="sperm-donors">
                <button type="submit" class="btn btn-primary blue-btn font-style">
                    <span class="egg_donor">I'm looking for EGGS DONOR</span>
                    <button type="submit" class="btn btn-light white-btn btn-light2 search-btn-style"> SEARCH NOW </button>
                </button>
            </form>
            </div>
        </div>
         <div class="row" style="margin-left: 11px;">
            <div class="col-lg-12 offset-lg-1 col-md-12 padding-tb-10px">
                <form method="get" action="{{route('search.form','egg-donors')}}">
                    <input type="hidden" name="category" value="egg-donors">
                <button type="button" class="btn btn-primary blue-btn font-style">
                    <span class="sperm_donor">I'm looking for SPERM DONOR</span>
                    <button type="submit" class="btn btn-light white-btn btn-light2 search-btn-style "> SEARCH NOW </button>
                </button>
            </form>
            </div>
        </div>
        <div class="row" style="margin-left: 11px;">
            <div class="col-lg-12 offset-lg-1 col-md-12 padding-tb-10px">
                 <form method="get" action="{{route('search.form','fertility-clinics')}}">
                     <input type="hidden" name="category" value="fertility-clinics">
                <button type="button" class="btn btn-primary blue-btn font-style">
                    <span class="fertility_clinics">I'm looking for FERTILITY CLINICS</span>
                    <button type="submit" class="btn btn-light white-btn btn-light2 search-btn-style"> SEARCH NOW </button>
                </button>
            </form>
            </div>
        </div>
        <div class="row" style="margin-left: 11px;">
            <div class="col-lg-12 col-md-12 offset-lg-1 padding-tb-10px">
                <button type="button" class="btn btn-primary blue-btn font-style">
                    <span class="f_egg_donor">I'm looking for FAMOUS EGG DONORS</span>
                    <button type="submit" class="btn btn-light white-btn btn-light2 search-btn-style"> SEARCH NOW </button>
                </button>
            </div>
        </div>
        <div class="row" style="margin-left: 11px;">
            <div class="col-lg-12 col-md-12 offset-lg-1 padding-tb-10px padding-bottom-20px">
                <button type="button" class="btn btn-primary blue-btn font-style">
                    <span class="f_sperm_donor">I'm looking for FAMOUS SPERM DONORS
                    <button type="submit" class="btn btn-light white-btn btn-light2 search-btn-style"> SEARCH NOW </button>
                </button>
                
            </div>
        </div> -->
    </div>
</section>