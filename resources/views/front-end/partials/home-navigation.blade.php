 <header>
        <div id="fixed-header-dark" class="header-output fixed-header">
            <div class="container header-in">
                <div class="row">
            <div class="col-md-6">
                        <a id="logo" style="width: 84%" href="index.html" class="d-inline-block"><img src="{{asset('assets/img/logo-tui.png')}}" alt=""></a>
            </div>
            <div class="col-md-6">
            <div class="top-right">
                    <!-- <ul class="clearfix" style="padding:0px;"> -->
                        <div style="display: flex">
                        <div class="align-top" style="font-weight: 400; line-height: 2; margin-bottom: 10px;">
                            <img src="https://pacojobs.com/en-content/images/texts/contact_scouts_en_15756766945708.jpg" class="contact-detail contact-photo ml-1 ml-sm-2 ml-lg-1" width="25" height="25">
                            <span class="contact-detail contact-name ml-1 ml-sm-2 ml-lg-1" >PACO artists booker</span>
                            <span class="contact-detail ml-1 ml-sm-2 contact-phone d-none d-lg-inline-block" >(+ 447418429076)</span>
                        </div>
                        <div class=" contact-links ml-1 ml-sm-2 ml-lg-0" style="font-size: 1.2rem; text-align: right;">
                            <a href="https://t.me/pacojobs" target="_blank" class="d-inline-block ml-0 ml-lg-3"><span class="fab fa-telegram" style="color: #4e9cd3;"></span></a>
                            <a href="https://wa.me/447418429076" target="_blank" class="d-inline-block ml-1 ml-sm-2 ml-lg-2"><span class="fab fa-whatsapp" style="color: #7fbe73;"></span></a>
                            <a href="viber://chat?number=447418429076" target="_blank" class="d-inline-block ml-1 ml-sm-2 ml-lg-2"><span class="fab fa-viber" style="color: #8162ac;"></span></a>
                            <a href="https://m.me/realpacoarmando" target="_blank" class="d-inline-block ml-1 ml-sm-2 ml-lg-2"><span class="fab fa-facebook-messenger" style="color: #4c6ef5;"></span></a>
                        </div>
                    </div>
                    <div style="display: flex">
                        <div class="align-top" style="font-weight: 400; line-height: 2; margin-bottom: 10px;">
                            <img src="https://pacojobs.com/en-content/images/texts/contact_scouts_en_15756766945708.jpg" class="contact-detail contact-photo ml-1 ml-sm-2 ml-lg-1" width="25" height="25">
                            <span class="contact-detail contact-name ml-1 ml-sm-2 ml-lg-1" >PACO artists booker</span>
                            <span class="contact-detail ml-1 ml-sm-2 contact-phone d-none d-lg-inline-block" >(+ 447418429076)</span>
                        </div>
                        <div class=" contact-links ml-1 ml-sm-2 ml-lg-0" style="font-size: 1.2rem; text-align: right;">
                            <a href="https://t.me/pacojobs" target="_blank" class="d-inline-block ml-0 ml-lg-3"><span class="fab fa-telegram" style="color: #4e9cd3;"></span></a>
                            <a href="https://wa.me/447418429076" target="_blank" class="d-inline-block ml-1 ml-sm-2 ml-lg-2"><span class="fab fa-whatsapp" style="color: #7fbe73;"></span></a>
                            <a href="viber://chat?number=447418429076" target="_blank" class="d-inline-block ml-1 ml-sm-2 ml-lg-2"><span class="fab fa-viber" style="color: #8162ac;"></span></a>
                            <a href="https://m.me/realpacoarmando" target="_blank" class="d-inline-block ml-1 ml-sm-2 ml-lg-2"><span class="fab fa-facebook-messenger" style="color: #4c6ef5;"></span></a>
                        </div>
                    </div>
                    <div style="display: flex">
                        <div class="align-top" style="font-weight: 400; line-height: 2; margin-bottom: 10px;">
                            <img src="https://pacojobs.com/en-content/images/texts/contact_scouts_en_15756766945708.jpg" class="contact-detail contact-photo ml-1 ml-sm-2 ml-lg-1" width="25" height="25">
                            <span class="contact-detail contact-name ml-1 ml-sm-2 ml-lg-1" >PACO artists booker</span>
                            <span class="contact-detail ml-1 ml-sm-2 contact-phone d-none d-lg-inline-block" >(+ 447418429076)</span>
                        </div>
                        <div class=" contact-links ml-1 ml-sm-2 ml-lg-0" style="font-size: 1.2rem; text-align: right;">
                            <a href="https://t.me/pacojobs" target="_blank" class="d-inline-block ml-0 ml-lg-3"><span class="fab fa-telegram" style="color: #4e9cd3;"></span></a>
                            <a href="https://wa.me/447418429076" target="_blank" class="d-inline-block ml-1 ml-sm-2 ml-lg-2"><span class="fab fa-whatsapp" style="color: #7fbe73;"></span></a>
                            <a href="viber://chat?number=447418429076" target="_blank" class="d-inline-block ml-1 ml-sm-2 ml-lg-2"><span class="fab fa-viber" style="color: #8162ac;"></span></a>
                            <a href="https://m.me/realpacoarmando" target="_blank" class="d-inline-block ml-1 ml-sm-2 ml-lg-2"><span class="fab fa-facebook-messenger" style="color: #4c6ef5;"></span></a>
                        </div>
                    </div>
                </div>
            </div>
        </div>
                <div class="row" style="align-items: center;">
                    <div class="col-lg-2 col-md-12">
                        <a class="mobile-toggle padding-13px background-main-color" href="#"><i class="fas fa-bars"></i></a>
                        <a id="logo" href="index.html" class="d-inline-block margin-tb-15px align-text"><img src="{{asset('assets/img/logo-tui.png')}}" alt=""></a>
                        
                    </div>
                    <div class="col-lg-7 col-md-12">
                        <nav style="position: relative;">
                            <ul id="menu-main" class="nav-menu float-lg-right link-padding-tb-20px" role="menu">
                                @foreach($menus as $menuitem)
                                <li class="has-dropdown">
                                    <a href="{{url('/'.$menuitem->slug)}}">{{$menuitem->name}}</a>
                                    @if($menuitem->pages->count()>0)
                                        <ul class="sub-menu">
                                            @foreach($menuitem->pages as $page)
                                            <li style="position: relative;"><a href="{{url('/'.$menuitem->slug.'/'.$page->slug)}}">{{$page->name}}</a>
                                                @if($page->subPages->count()>0)
                                               <ul class="sub-menu" style="position: absolute;">
                                                    @foreach($page->subPages as $subpage)
                                                    <li><a href="{{url('/'.$menuitem->slug.'/'.$page->slug.'/'.$subpage->slug)}}">{{$subpage->name}}</a>   
                                                    </li>
                                                    @endforeach
                                                </ul>
                                                @endif 
                                            </li>
                                            @endforeach
                                        </ul>
                                    @endif
                                </li>
                                @endforeach
                            </ul>
                        </nav>
                    </div>
                    <div class="col-lg-3 col-md-12">
                        <hr class="margin-bottom-0px d-block d-sm-none">
                        
                        <!-- <a href="page-login.html" class="margin-tb-20px d-inline-block text-up-small float-left float-lg-right"><i class="far fa-user"></i>  Login</a> -->
                    </div>
                </div>

            </div>
        </div>
    </header>