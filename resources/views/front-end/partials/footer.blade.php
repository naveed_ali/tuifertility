 <footer class="padding-top-100px background-main-color wow fadeInUp">
        <div class="container">
            <div class="row">
                <div class="col-lg-5 col-md-5 sm-mb-30px">
                    <div class="row">
                        <div class="col-6 col-md-5 col-lg-5 sm-mb-15px" style="margin-top: -57px;">
                            <a class="d-inline-block margin-tb-15px">
                                <img style="background-color: white;" src="{{asset('assets/img/logo-tui.png')}}" alt="Tuifertility">
                            </a>
                        </div>
                        <div class="col-md-12 col-lg-12">
                            <h2 class="footer-heading f-sm-mb-0px" style="line-height: 15px;">
                                CONNECT WITH US
                            </h2>
                            <ul class="social-icon style-2">
                                <li class="list-inline-item" style="margin-left: -10px;"><a class="facebook" href="#"><i class="fab fa-facebook-f"></i></a></li>
                                <li class="list-inline-item"><a class="youtube" href="#"><i class="fab fa-youtube"></i></a></li>
                                <li class="list-inline-item"><a class="linkedin" href="#"><i class="fab fa-linkedin"></i></a></li>
                                <li class="list-inline-item"><a class="google" href="#"><i class="fab fa-google-plus"></i></a></li>
                                <li class="list-inline-item"><a class="twitter" href="#"><i class="fab fa-twitter"></i></a></li>
                                <li class="list-inline-item"><a class="rss" href="#"><i class="fa fa-rss" aria-hidden="true"></i></a></li>
                            </ul>
                            <br>
                            <p class="footer-p-address sm-mt-5px">
                                <a href="tuifertility.com">TUIFertility.com</a><br>
                                427 N Tatnall St, STE 96120<br>
                                Wilmington, DE 19801<br>
                                Email: <a href="mailto:office@tuifertility.com">office@tuifertility.com</a><br>
                            </p>
                            
                        </div>
                    </div>
                    
                </div>
                <div class="col-sm-6 col-md-3 col-lg-3 sm-mb-30px">
                    <h2 class="footer-heading">
                        LEARN MORE
                    </h2>
                    <ul class="footer-menu margin-tb-15px margin-lr-0px padding-0px list-unstyled" style="font-size: 12px;">
                        <li><a href="#" class="text-white">Privacy Policy</a></li>
                        <li><a href="#" class="text-white">Terms of Use</a></li>
                        <li><a href="#" class="text-white">About Us</a></li>
                    </ul>
                </div>
                <div class="col-sm-6 col-md-4 col-lg-4 ">
                    <h2 class="footer-heading">
                        FIND
                    </h2>
                    <ul class="footer-menu margin-tb-15px margin-lr-0px padding-0px list-unstyled" style="font-size: 12px;">
                        <li><a href="#" class="text-white">Surrogate Mothers</a></li>
                        <li><a href="#" class="text-white">Egg Donors</a></li>
                        <li><a href="#" class="text-white">Sperm Donors</a></li>
                        <li><a href="#" class="text-white">Fertility Clinics</a></li>
                        <li><a href="#" class="text-white">Famous Individuals Egg Donors</a></li>
                        <li><a href="#" class="text-white">Famous Individuals Sperm Donors</a></li>
                    </ul>
                </div>
            </div>
            <hr class="border-white opacity-4 margin-tb-45px">
            <div class="row">
                <div class="col-lg-12 text-center margin-top-5px margin-bottom-10px">
                    <p style="font-size: 12px;" class="margin-0px text-white opacity-7 sm-mb-15px">Copyright © TUIFertility.com . All rights reserved. </p>
                </div>
                <!-- <div class="col-lg-6">
                    <ul class="social-icon style-2 float-lg-right">
                        <li class="list-inline-item"><a class="facebook" href="#"><i class="fab fa-facebook-f"></i></a></li>
                        <li class="list-inline-item"><a class="youtube" href="#"><i class="fab fa-youtube"></i></a></li>
                        <li class="list-inline-item"><a class="linkedin" href="#"><i class="fab fa-linkedin"></i></a></li>
                        <li class="list-inline-item"><a class="google" href="#"><i class="fab fa-google-plus"></i></a></li>
                        <li class="list-inline-item"><a class="twitter" href="#"><i class="fab fa-twitter"></i></a></li>
                        <li class="list-inline-item"><a class="rss" href="#"><i class="fa fa-rss" aria-hidden="true"></i></a></li>
                    </ul>
                </div> -->
            </div>
        </div>
    </footer>