 <section class="padding-tb-100px background-grey-1">
        <div class="container">
            <!-- Title -->
            <div class="row justify-content-center margin-bottom-45px">
                <div class="col-lg-10">
                    <div class="row">
                        <div class="col-md-4 wow fadeInUp">
                            <h1 class="text-second-color font-weight-300 text-sm-center text-lg-right margin-tb-15px">Famous Doctors</h1>
                        </div>
                        <div class="col-md-6 wow fadeInUp" data-wow-delay="0.2s">
                            <p class="text-grey-2">Lorem Ipsum is simply dummy text of the printing and typesetting industry. Lorem Ipsum has been the industry's standard dummy text ever since the 1500s</p>
                        </div>
                        <div class="col-md-2 wow fadeInUp" data-wow-delay="0.4s">
                            <a href="#" class="text-main-color margin-tb-15px d-inline-block"><span class="d-block float-left margin-right-10px margin-top-5px">Show All</span> <i class="far fa-arrow-alt-circle-right text-large margin-top-7px"></i></a>
                        </div>
                    </div>
                </div>
            </div>
            <!-- // Title -->

            <div class="row">

                <!-- Doctor -->
                <div class="col-lg-3 col-md-6 hvr-bob sm-mb-45px">
                    <div class="background-white box-shadow wow fadeInUp" data-wow-delay="0.2s">
                        <div class="thum">
                            <a href="#"><img src="{{asset('assets/img/doctor-1.jpg')}}" alt=""></a>
                        </div>
                        <div class="padding-30px">
                            <span class="text-grey-2">Internal</span>
                            <h5 class="margin-tb-15px"><a class="text-dark" href="#">Dr. Shahrzat Moh</a></h5>
                            <div class="rating clearfix">
                                <ul class="float-left">
                                    <li class="active"></li>
                                    <li class="active"></li>
                                    <li class="active"></li>
                                    <li class="active"></li>
                                    <li></li>
                                </ul>
                                <small class="float-right text-grey-2">(17 reviews)</small>
                            </div>
                        </div>
                    </div>
                </div>
                <!-- // Doctor -->

                <!-- Doctor -->
                <div class="col-lg-3 col-md-6 hvr-bob sm-mb-45px">
                    <div class="background-white box-shadow wow fadeInUp" data-wow-delay="0.4s">
                        <div class="thum">
                            <a href="#"><img src="{{asset('assets/img/doctor-2.jpg')}}" alt=""></a>
                        </div>
                        <div class="padding-30px">
                            <span class="text-grey-2">Internal</span>
                            <h5 class="margin-tb-15px"><a class="text-dark" href="#">Dr. Adwa Ali</a></h5>
                            <div class="rating clearfix">
                                <ul class="float-left">
                                    <li class="active"></li>
                                    <li class="active"></li>
                                    <li class="active"></li>
                                    <li class="active"></li>
                                    <li></li>
                                </ul>
                                <small class="float-right text-grey-2">(17 reviews)</small>
                            </div>
                        </div>
                    </div>
                </div>
                <!-- // Doctor -->

                <!-- Doctor -->
                <div class="col-lg-3 col-md-6 hvr-bob sm-mb-45px">
                    <div class="background-white box-shadow wow fadeInUp" data-wow-delay="0.6s">
                        <div class="thum">
                            <a href="#"><img src="{{asset('assets/img/doctor-3.jpg')}}" alt=""></a>
                        </div>
                        <div class="padding-30px">
                            <span class="text-grey-2">Internal</span>
                            <h5 class="margin-tb-15px"><a class="text-dark" href="#">Dr. Salma Elkheir</a></h5>
                            <div class="rating clearfix">
                                <ul class="float-left">
                                    <li class="active"></li>
                                    <li class="active"></li>
                                    <li class="active"></li>
                                    <li class="active"></li>
                                    <li></li>
                                </ul>
                                <small class="float-right text-grey-2">(17 reviews)</small>
                            </div>
                        </div>
                    </div>
                </div>
                <!-- // Doctor -->

                <!-- Doctor -->
                <div class="col-lg-3 col-md-6 hvr-bob sm-mb-45px">
                    <div class="background-white box-shadow wow fadeInUp" data-wow-delay="0.8s">
                        <div class="thum">
                            <a href="#"><img src="{{asset('assets/img/doctor-4.jpg')}}" alt=""></a>
                        </div>
                        <div class="padding-30px">
                            <span class="text-grey-2">Internal</span>
                            <h5 class="margin-tb-15px"><a class="text-dark" href="#">Dr. Salim Qasim</a></h5>
                            <div class="rating clearfix">
                                <ul class="float-left">
                                    <li class="active"></li>
                                    <li class="active"></li>
                                    <li class="active"></li>
                                    <li class="active"></li>
                                    <li></li>
                                </ul>
                                <small class="float-right text-grey-2">(17 reviews)</small>
                            </div>
                        </div>
                    </div>
                </div>
                <!-- // Doctor -->

            </div>
        </div>
    </section>