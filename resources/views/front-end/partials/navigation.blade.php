<header>
   <div id="fixed-header-dark" class="header-output fixed-header box-shadow">
      <div class="container"> <!-- header-in -->
         <!-- <div class="container-fluid"> -->
         <div class="row">
               <div class="col-10 col-sm-8 col-md-6 col-lg-4 order2">
                  @if(url()->current()!==url('/'))
                    <a id="logo2" href="{{url('/')}}" class="d-inline-block align-text">
                       <img src="{{asset('assets/img/logo-tui.png')}}" alt="logo-tuilogo-tuilogo-tui" class="sm-left">
                    </a>
                    <!-- mobile-logo -->
                    <a id="logo2" href="{{url('/')}}" class="d-inline-block align-text">
                       <img src="{{asset('assets/img/tui.png')}}" alt="logo-tuilogo-tui" class="mobile-logo">
                    </a>
                    <!--Search Button-->
                    <button type="button" class="search_btn search_btn_header btn btn-block">
                      SEARCH NOW&nbsp;&nbsp;<!--customize-select --><span class="fa fa-angle-down"></span>&nbsp;
                      
                      <select class="custom-dropdown" name="category" id="search_btn_select">
                        <option value="" selected>Please Select</option>
                         <option value="surrogate-mothers">surrogate mothers</option>
                         <option value="egg-donors">egg donor</option>
                         <option value="sperm-donors">sperm donor</option>
                         <option value="fertility-clinics">ferility clinics</option>
                         <option value="famous-sperm-donors">famous sperm donor</option>
                         <option value="famous-egg-donors">famous egg donor</option>
                      </select>
                       <form method="get" action="{{route('search.form')}}" style="display: none;" id="submit-form">
                      <input type="hidden" id="category" name="category">
                    </form>
                    </button>
                  @endif
               </div>
               
               <div class="col-12 col-sm-12 col-md-6 col-lg-8 order1">
                  <div class="top-right" style="float: right;">
                    @foreach($scouts as $scout)
                      <div class="display-flex">
                          <div class="align-top" style="font-weight: 400; line-height: 2;">
                             <div class="d-inline-block" style="line-height: 38px;">
                                <img src="{{asset('storage/'.$scout->picture)}}"
                                   class="contact-detail contact-photo ml-1 ml-sm-2 ml-lg-1"
                                   style="">
                                <span class="contact-detail contact-name ml-1 ml-sm-2 ml-lg-1">{{$scout->name}}</span>
                                <span class="contact-detail ml-1 ml-sm-2 contact-phone d-none d-lg-inline-block">
                                   ({{$scout->phone}})
                                </span>
                             </div>
                             <div class="d-inline-block" style="margin-left: 0 !important; float: right;">
                                @if($scout->telegram)
                                   <a href="{{$scout->telegram}}" target="_blank" class="d-inline-block scout_social_icons ml-1 ml-lg-3">
                                      <span class="fab fa-telegram social-contact"></span>
                                   </a>
                                @endif
                                @if($scout->messenger)
                                   <a href="{{$scout->messenger}}" target="_blank" class="d-inline-block scout_social_icons ml-1 ml-sm-2 ml-lg-2">
                                      <span class="fab fa-facebook-messenger social-contact"></span>
                                   </a>
                                @endif
                                @if($scout->whatsapp)
                                   <a href="{{$scout->whatsapp}}" target="_blank" class="d-inline-block scout_social_icons ml-1 ml-sm-2 ml-lg-2">
                                      <span class="fab fa-whatsapp social-contact"></span>
                                   </a>
                                @endif
                                @if($scout->viber)
                                   <a href="{{$scout->viber}}" target="_blank" class="d-inline-block scout_social_icons ml-1 ml-sm-2 ml-lg-2">
                                      <span class="fab fa-viber social-contact"></span>
                                   </a>
                                @endif
                             </div>
                          </div>
                      </div>
                    @endforeach
                  </div>
               </div>
         </div>
         <!-- </div>align-text -->

         <div class="row apply-margin">
            <div class="col-6 col-sm-4 col-md-3 col-lg-2 apply-margin-bottom logo-margin-left">
              <a id="logo2" href="{{url('/')}}" class="d-inline-block " style="margin-top: 10px; margin-bottom: 10px;"> 
                <img src="{{asset('assets/img/logo-tui.png')}}" alt="logo-tui" class="smallhide">
              </a>

              <a class="padding-13px background-main-color" id="navToggle" href="#"><i class="fas fa-bars"></i> Menu</a>
            </div>

            <div class="col-lg-9 col-md-9 no-gutters m-width" style="margin-top: 10px;">
               <nav style="position: relative;">
                  <ul id="menu-main" class="nav-menu link-padding-tb-20px" role="menu">
                     @foreach($menus as $menuitem)
                     <li class="has-dropdown" style="margin-right: 5px;">
                        <a style="padding: 10px 20px 10px 20px;" href="#">{{$menuitem->name}}</a>
                        @if($menuitem->pages->count()>0)
                        <ul class="sub-menu" style="margin-right: 20px; right: 0; left: none; box-shadow: 0px 1px 2px 1px #888;">
                           @foreach($menuitem->pages as $page)
                           <li style="position: relative;">
                              <a href="{{url('/'.$menuitem->slug.'/'.$page->slug)}}">{{$page->name}}</a>
                              @if($page->subPages->count()>0)
                              <ul class="sub-menu sub-menu2" style="position: absolute; box-shadow: 0px 1px 2px 1px #888;">
                                 @foreach($page->subPages as $subpage)
                                 <li><a
                                       href="{{url('/'.$menuitem->slug.'/'.$page->slug.'/'.$subpage->slug)}}">{{$subpage->name}}</a>
                                 </li>
                                 @endforeach
                              </ul>
                              @endif
                           </li>
                           @endforeach
                        </ul>
                        @endif
                     </li>
                     @endforeach
                  </ul>
               </nav>
            </div>
            
         </div>
         <!--  START Mobile NAVBAR   -->
            <nav class="navbar navbar-expand-lg fixed-top navbar-dark navbar-offcanvas">
                <div class="navbar-collapse offcanvas-collapse" style="background-color: #0664b2;">


                    <ul class="navbar-nav mr-auto">
                     
                        <a id="navToggle" class="nav-toggle-a" href="#" style="" class="pt-1"> 
                           <i class="fas fa-times"></i>
                        </a>
                        @foreach($menus as $menuitem)
                        <li class="nav-item dropdown">
                          <a class="nav-link dropdown-toggle" href="https://example.com" id="m-{{$menuitem->id}}" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false">{{$menuitem->name}}</a>
                          @if($menuitem->pages->count()>0)
                            <div class="dropdown-menu" aria-labelledby="m-{{$menuitem->id}}">
                              @foreach($menuitem->pages as $page)
                                @if($page->subPages->count()>0)
                                <a class="nav-link dropdown-toggle" href="{{url('/'.$menuitem->slug.'/'.$page->slug)}}" id="m-{{$menuitem->id}}"
                                    data-toggle="dropdown" aria-haspopup="true" aria-expanded="false" style="color:#343a40;padding: .25rem 1.5rem;"> {{$page->name}} </a>
                                    <div class="dropdown-menu" aria-labelledby="m-{{$menuitem->id}}">
                                        @foreach($page->subPages as $subpage)
                                        <a class="nav-link" href="{{url('/'.$menuitem->slug.'/'.$page->slug.'/'.$subpage->slug)}}" style="color:#343a40;padding: .25rem 1.5rem;">{{$subpage->name}}</a>
                                          @endforeach
                                    </div>
                                  @else
                                  <a class="nav-link" href="{{url('/'.$menuitem->slug.'/'.$page->slug)}}"style="color:#343a40;padding: .25rem 1.5rem;"> {{$page->name}} </a>  
                                  @endif
                              @endforeach
                            </div>
                          @endif
                        </li>
                        @endforeach
                    </ul>
                </div>
            </nav>
         <!--  END NAVBAR   -->
      </div>
   </div>
</header>