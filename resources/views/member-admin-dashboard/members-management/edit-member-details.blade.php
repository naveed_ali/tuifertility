@extends('member-admin-dashboard.layouts.master')
@section('additional-head')
<!-- <link href="https://stackpath.bootstrapcdn.com/bootstrap/3.3.7/css/bootstrap.min.css" rel="stylesheet"> -->
<link href="https://cdn.jsdelivr.net/npm/summernote@0.8.16/dist/summernote.min.css" rel="stylesheet">
@endsection
@section('content')
<!-- Page Title -->
                <div id="page-title" class="padding-30px background-white full-width">
                    <div class="container">
                        <ol class="breadcrumb opacity-5">
                            <li><a href="#">Home</a></li>
                            <li><a href="#">Edit informationPage</a></li>
                        </ol>
                        <h1 class="font-weight-300">Edit information Page</h1>
                    </div>
                </div>
                <!-- // Page Title -->

                <div class="margin-tb-45px full-width">
                     @if ($errors->any())
                        <div class="alert alert-danger">
                            <ul>
                                @foreach ($errors->all() as $error)
                                    <li>{{ $error }}</li>
                                @endforeach
                            </ul>
                        </div>
                    @endif
                    <div class="padding-30px background-white border-radius-20 box-shadow">
                        <h3><i class="far fa-list-alt margin-right-10px text-main-color"></i>{{$becomeMember->name}}'s Edit Details Page </h3>
                        <hr>
                        <form method="post" action="{{route('become_members.update',$becomeMember->id)}}" enctype="multipart/form-data">
                            @method('put')
                            @csrf
                    <input type="hidden" name="category" value="{{$becomeMember->category}}">
                    <div class="form-group label-floating">
                        <label class="control-label">Your Email*</label>
                        <input class="form-control" placeholder="" value="{{$becomeMember->email}}" name="email" type="email">
                    </div>
                    <div class="form-group label-floating">
                        <label class="control-label">Name</label>
                        <input class="form-control" value="{{$becomeMember->name}}" name="name" placeholder="" type="text">
                    </div>
                    <div class="form-group label-floating">
                        <label class="control-label">Birthday (mo/day/yr)*</label>
                        <input class="form-control" value="{{$becomeMember->birthday}}" name="birthdate" placeholder="Your Birthday" type="date">
                    </div>
                    <div class="form-group label-floating is-select">
                        <label class="control-label">Country</label>
                        <select class="selectpicker form-control" name="country">
                          @foreach($countries as $country)
                          <option value="{{$country->country_name}}" {{$becomeMember->country==$country->country_name?'selected':''}}>{{$country->country_name}}</option>
                          @endforeach
                       </select>
                    </div>
                    <div class="form-group label-floating">
                        <label class="control-label">Address</label>
                        <input class="form-control" value="{{$becomeMember->address}}" name="address" placeholder="Address" type="text">
                    </div>
                    <div class="form-group label-floating">
                        <label class="control-label">Phone/WhatsApp/Viber/Telegram</label>
                        <input class="form-control" value="{{$becomeMember->phone}}" name="phone" placeholder="Phone/WhatsApp/Viber/Telegram" type="text">
                    </div>
                    <div class="form-group label-floating is-select">
                        <label class="control-label">Race*</label>
                        <select class="selectpicker form-control" name="race">
                            @foreach(explode(',',$formText->race_data) as $key => $race_data)
                              <option value="{{$race_data}}" {{$race_data==$becomeMember->race?'selected': ''}}>{{$race_data}}</option>
                              @endforeach
                       </select>
                    </div>

                    <div class="form-group label-floating is-select">
                        <label class="control-label">Religion</label>
                        <select class="selectpicker form-control" name="religion">
                           <option value="">Please Select</option>
                           
                           @foreach(explode(',',$formText->religion_data) as $key => $religion_data)
                              <option value="{{$religion_data}}" {{$religion_data==$becomeMember->religion?'selected': ''}}>{{$religion_data}}</option>
                              @endforeach
                       </select>
                    </div>
                    <div class="form-group label-floating is-select">
                        <label class="control-label">Education</label>
                        <select class="selectpicker form-control" name="education">
                           @foreach(explode(',',$formText->education_data) as $key => $education_data)
                              <option value="{{$education_data}}" {{$education_data==$becomeMember->education ? 'selected': ''}}>{{$education_data}}</option>
                              @endforeach
                       </select>
                    </div>
                    <div class="form-group label-floating is-select">
                        <label class="control-label">Health Status</label>
                        <select class="selectpicker form-control" name="health_status">
                            @foreach(explode(',',$formText->health_status_data) as $key => $health_status_data)
                              <option value="{{$health_status_data}}" {{$health_status_data==$becomeMember->health_status ? 'selected': ''}}>{{$health_status_data}}</option>
                              @endforeach
                       </select>
                    </div>
                    <div class="form-group label-floating is-select">
                        <label class="control-label">Smoker</label>
                        <select class="selectpicker form-control" name="smoker">
                           @foreach(explode(',',$formText->smoker_data) as $key => $smoker_data)
                              <option value="{{$smoker_data}}" {{$smoker_data==$becomeMember->smoker ? 'selected': ''}}>{{$smoker_data}}</option>
                              @endforeach
                       </select>
                    </div>
                    <div class="form-group label-floating is-select">
                        <label class="control-label">Eye Color*</label>
                        <select class="selectpicker form-control" name="eye_color">
                           @foreach(explode(',',$formText->eye_color_data) as $key => $eye_color_data)
                              <option value="{{$eye_color_data}}" {{$eye_color_data==$becomeMember->eye_color ? 'selected': ''}}>{{$eye_color_data}}</option>
                              @endforeach
                       </select>
                    </div>
                    <div class="form-group label-floating is-select">
                        <label class="control-label">Hair Color*</label>
                        <select class="selectpicker form-control" name="hair_color">
                           @foreach(explode(',',$formText->hair_color_data) as $key => $hair_color_data)
                              <option value="{{$hair_color_data}}" {{$hair_color_data==$becomeMember->hair_color ? 'selected': ''}}>{{$hair_color_data}}</option>
                              @endforeach
                       </select>
                    </div>
                    <div class="form-group label-floating is-select">
                        <label class="control-label">Height*</label>
                        <select class="selectpicker form-control" name="height">
                            @foreach(explode(',',$formText->height_data) as $key => $height_data)
                              <option value="{{$height_data}}" {{$height_data==$becomeMember->height ? 'selected': ''}}>{{$height_data}}</option>
                              @endforeach
                       </select>
                    </div>
                    <div class="form-group label-floating is-select">
                        <label class="control-label">Weight*</label>
                        <select class="selectpicker form-control" name="weight">
                            @foreach(explode(',',$formText->weight_data) as $key => $weight_data)
                              <option value="{{$weight_data}}" {{$weight_data==$becomeMember->weight ? 'selected': ''}}>{{$weight_data}}</option>
                              @endforeach
                       </select>
                    </div>
                    <div class="form-group label-floating is-select">
                        <label class="control-label">Blood Type</label>
                        <select class="selectpicker form-control" name="blood_type">
                            @foreach(explode(',',$formText->blood_type_data) as $key => $blood_type_data)
                              <option value="{{$blood_type_data}}" {{$blood_type_data==$becomeMember->blood_type ? 'selected': ''}}>{{$blood_type_data}}</option>
                              @endforeach
                       </select>
                    </div>
                    <div class="form-group label-floating is-select">
                        <label class="control-label">Have Passport</label>
                        <select class="selectpicker form-control" name="have_passport">
                        @foreach(explode(',',$formText->have_passport_data) as $key => $have_passport_data)
                              <option value="{{$have_passport_data}}" {{$have_passport_data==$becomeMember->have_passport ? 'selected': ''}}>{{$have_passport_data}}</option>
                              @endforeach
                    </select>
                    </div>
                    <div class="form-group label-floating is-select">
                        <label class="control-label">Willing To Travel</label>
                        <select class="selectpicker form-control" name="willing_to_travel">
                            @foreach(explode(',',$formText->willing_to_travel_data) as $key => $willing_to_travel_data)
                              <option value="{{$willing_to_travel_data}}" {{$willing_to_travel_data==$becomeMember->willing_to_travel ? 'selected': ''}}>{{$willing_to_travel_data}}</option>
                              @endforeach
                       </select>
                    </div>
                    <div class="form-group label-floating is-select">
                        <label class="control-label">Number of Children(non-donor)</label>
                        <select class="selectpicker form-control" name="number_of_children">
                            @foreach(explode(',',$formText->number_of_children_data) as $key => $number_of_children_data)
                              <option value="{{$number_of_children_data}}" {{$number_of_children_data==$becomeMember->number_of_children ? 'selected': ''}}>{{$number_of_children_data}}</option>
                              @endforeach
                       </select>
                    </div>
                    <div class="form-group label-floating is-select">
                        <label class="control-label">Number of Surrogate Children</label>
                        <select class="selectpicker form-control" name="number_of_surrogate_children">
                            @foreach(explode(',',$formText->number_of_surrogate_children_data) as $key => $number_of_surrogate_children_data)
                              <option value="{{$number_of_surrogate_children_data}}" {{$number_of_surrogate_children_data==$becomeMember->number_of_surrogate_children ? 'selected': ''}}>{{$number_of_surrogate_children_data}}</option>
                              @endforeach
                       </select>
                    </div>
                     
                    <div class="form-group label-floating is-select">
                        <label class="control-label">Surrogate Mother Types That You Agree To:*</label>
                        <div class="row">
                      <div class="col-md-6">
                        <div class="input-group">
                        <select class="selectpicker form-control jq_multiselect" multiple="multiple" name="surrogate_type[]">
                            @foreach(explode(',',$formText->surrogate_type_data) as $key => $surrogate_type_data)
                              <option value="{{$surrogate_type_data}}" {{$surrogate_type_data==$becomeMember->surrogate_type ? 'selected': ''}}>{{$surrogate_type_data}}</option>
                              @endforeach
                       </select>
                     </div>
                   </div>
                   <div class="col-md-6">
               <div class="form-group">
                  <label class="control-label">Selected By User: {{$becomeMember->surrogate_type}}</label>
               </div>
            </div>
                    </div>
                  </div>
                    <div class="form-group label-floating is-select">
                        <label class="control-label">Have you been a @if($becomeMember->category=='surrogate-mothers')
                            surrogate mother
                        @endif
                          @if($becomeMember->category=='sperm-donors')
                            sperm donor
                        @endif
                          @if($becomeMember->category=='egg-donors')
                            egg donor
                        @endif
                         before?</label>
                        <select class="selectpicker form-control" name="given_service_before">
                            @foreach(explode(',',$formText->given_service_before_data) as $key => $given_service_before_data)
                              <option value="{{$given_service_before_data}}" {{$given_service_before_data==$becomeMember->given_service_before ? 'selected': ''}}>{{$given_service_before_data}}</option>
                              @endforeach
                       </select>
                    </div>
                    <div class="form-group label-floating is-select">
                        <label class="control-label">Marital Status</label>
                        <select class="selectpicker form-control" name="marital_status">
                            @foreach(explode(',',$formText->marital_status_data) as $key => $marital_status_data)
                              <option value="{{$marital_status_data}}" {{$marital_status_data==$becomeMember->marital_status ? 'selected': ''}}>{{$marital_status_data}}</option>
                              @endforeach
                       </select>
                    </div>
                    <div class="form-group label-floating is-select">
                        <label class="control-label">Spoken Languages</label>
                        <div class="row">
                      <div class="col-md-6">
                        <div class="input-group">
                        <select class="form-control jq_multiselect" id="spoken_languages" enable_select_all="false" name="spoken_language[]" multiple="multiple" >
                              @foreach(explode(',',$formText->spoken_language_data) as $key => $spoken_language_data)
                              <option value="{{$spoken_language_data}}" {{$spoken_language_data==$becomeMember->spoken_language ? 'selected': ''}}>{{$spoken_language_data}}</option>
                              @endforeach
                            </select>
                  </div>
                </div>
                <div class="col-md-6">
               <div class="form-group">
                  <label class="control-label">Selected By User: {{$becomeMember->spoken_language}}</label>
               </div>
            </div>
                    </div>
                  </div>
                    <div class="form-group label-floating is-select">
                        <label class="control-label">Willing To Assist Couple Types:*</label>
                         <div class="row">
                      <div class="col-md-6">
                        <div class="input-group">
                         <select class="selectpicker form-control spoken_language jq_multiselect" name="willing_to_assist[]" multiple="multiple">
                              @foreach(explode(',',$formText->willing_to_assist_data) as $key => $willing_to_assist_data)
                              <option value="{{$willing_to_assist_data}}" {{$willing_to_assist_data==$becomeMember->willing_to_assist ? 'selected': ''}}>{{$willing_to_assist_data}}</option>
                              @endforeach
                           </select>
                      </div>
                    </div>
                     <div class="col-md-6">
               <div class="form-group">
                  <label class="control-label">Selected By User: {{$becomeMember->willing_to_assist}}</label>
               </div>
                  </div>
                    </div>
                    <div class="form-group label-floating">
                        <label class="control-label">How would you best describe yourself?</label>
                        <textarea class="form-control" name="describe_yourself" >{{$becomeMember->describe_yourself}}</textarea>
                    </div>
                    <div class="form-group label-floating">
                        <label class="control-label">Why do you want to be a 
                        @if($becomeMember->category=='surrogate-mothers')
                            surrogate mother
                        @endif
                          @if($becomeMember->category=='sperm-donors')
                            sperm donor
                        @endif
                          @if($becomeMember->category=='egg-donors')
                            egg donor
                        @endif
                        ?*</label>
                        <textarea class="form-control" name="why_become_member" >{{$becomeMember->why_become_member}}</textarea>
                    </div>
                    <div class="form-group label-floating">
                        <label class="control-label">Use this space to communicate a letter to potential intended parents.</label>
                        <textarea class="form-control" name="communication_letter">{{$becomeMember->communication_letter}}</textarea>
                    </div>
                    <div class="form-group label-floating">
                     <label class="control-label">Video URL</label>
                     <input class="form-control" value="{{$becomeMember->video_url}}" name="video_url" type="text">
                  </div>
                    <div class="form-group label-floating">
                        
                        <img src="{{asset('storage/'.$becomeMember->picture)}}"   height="100">
                    </div>
                    <div class="form-group label-floating">
                        <label class="control-label">Select Another Photos to Update</label><br>
                        <span class="btn btn-success btn-success2 fileinput-button">
                              <i class="fa fa-plus"></i>
                              <span>Add files...</span>
                              <input type="file" name="picture[]" multiple="multiple">
                           </span>
                    </div>
                       <div class="form-group label-floating is-select">
                        <label class="control-label">Stars</label>
                        <div class="row">
                      <div class="col-md-6">
                        <div class="input-group">
                        <select class="selectpicker form-control" name="stars">
                            <option value="">Please Select</option>
                           @if($becomeMember->review)
                            <option value="0" {{$becomeMember->review->stars==0?'selected':''}}>0</option>
                            <option value="1"  {{$becomeMember->review->stars==1?'selected':''}}>1</option>
                            <option value="2"  {{$becomeMember->review->stars==2?'selected':''}}>2</option>
                            <option value="3"  {{$becomeMember->review->stars==3?'selected':''}}>3</option>
                            <option value="4"  {{$becomeMember->review->stars==4?'selected':''}}>4</option>
                            <option value="5"  {{$becomeMember->review->stars==5?'selected':''}}>5</option>
                            @else
                            <option value="0">0</option>
                            <option value="1">1</option>
                            <option value="2">2</option>
                            <option value="3">3</option>
                            <option value="4">4</option>
                            <option value="5">5</option>
                            @endif
                       </select>
                     </div>
                   </div>
                    </div>
                  </div> 
                   <div class="form-group label-floating">
                        <label class="control-label">Your Review</label>
                        @if($becomeMember->review)
                        <textarea class="form-control" id="summernote" name="review"> {{$becomeMember->review->review}}</textarea>
                        @else
                        <textarea class="form-control" id="summernote" name="review"></textarea>
                        @endif
                    </div>
                    </div>
                <button type="submit" class="btn btn-lg border-2  btn-primary btn-block border-radius-15 padding-15px box-shadow">Update</button>
            </form>
                
@endsection
@section('additional-js')
<script src="https://cdn.jsdelivr.net/npm/summernote@0.8.16/dist/summernote.min.js"></script>
 <script>
    $(document).ready(function() {
        $('#summernote').summernote();
    });
  </script>
<script type="text/javascript" src="{{asset('assets/assets/bootstrap-datepicker.js')}}"></script>
<script type="text/javascript" src="{{asset('assets/assets/bootstrap-datepicker.init.js')}}"></script>
<script type="text/javascript" src="{{asset('assets/assets/signup.js')}}"></script>
<script type="text/javascript" src="{{asset('assets/assets/bootstrap-multiselect.js')}}"></script>
<link rel="stylesheet" type="text/css" href="{{asset('assets/assets/datepicker3.css')}}">
<link rel="stylesheet" type="text/css" href="{{asset('assets/assets/bootstrap-multiselect.css')}}">
<link rel="stylesheet" type="text/css" href="{{asset('assets/assets/member_generic.css')}}">
<script src='https://www.google.com/recaptcha/api.js'></script>
@endsection