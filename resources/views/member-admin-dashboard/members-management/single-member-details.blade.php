@extends('member-admin-dashboard.layouts.master')
@section('additional-head')
<!-- <link href="https://stackpath.bootstrapcdn.com/bootstrap/3.3.7/css/bootstrap.min.css" rel="stylesheet"> -->
@endsection
@section('content')
<!-- Page Title -->
                <div id="page-title" class="padding-30px background-white full-width">
                    <div class="container">
                        <ol class="breadcrumb opacity-5">
                            <li><a href="#">Home</a></li>
                            <li><a href="#">Member Details Page</a></li>
                        </ol>
                        <h1 class="font-weight-300">Member Details Page</h1>
                    </div>
                </div>
                <!-- // Page Title -->

                <div class="margin-tb-45px full-width">
                    <div class="padding-30px background-white border-radius-20 box-shadow">
                        <h3><i class="far fa-list-alt margin-right-10px text-main-color"></i> {{$becomeMember->name}}'s Detailed Information </h3>
                        <hr>
                        <form method="post" action="{{route('become_members.store')}}" enctype="multipart/form-data">
                    @csrf
                    <input type="hidden" name="category" value="{{$becomeMember->category}}">
                     <div class="form-group label-floating">
                        
                        <img src="{{asset('storage/'.$becomeMember->picture)}}"   height="100">
                    </div>
                    <div class="form-group label-floating">
                        <label class="control-label">Your Email</label>
                        <input class="form-control" selected value="{{$becomeMember->email}}" placeholder="" name="email" type="email">
                    </div>
                    <div class="form-group label-floating">
                        <label class="control-label">Full Name</label>
                        <input class="form-control" selected value="{{$becomeMember->name}}" name="name" placeholder="" type="text">
                    </div>
                    <div class="form-group label-floating">
                        <label class="control-label">Birthday (mo/day/yr)</label>
                        <input class="form-control" selected value="{{$becomeMember->birthday}}" type="date">
                    </div>
                    <div class="form-group label-floating is-select">
                        <label class="control-label">Country</label>
                        <select class="selectpicker form-control" name="country">
                          
                          <option value="{{$becomeMember->country}}" selected>{{$becomeMember->country}}</option>
                       </select>
                    </div>
                    <div class="form-group label-floating">
                        <label class="control-label">Address</label>
                        <input class="form-control" selected value="{{$becomeMember->address}}"  type="text">
                    </div>
                    <div class="form-group label-floating">
                        <label class="control-label">Phone/WhatsApp/Viber/Telegram</label>
                        <input class="form-control" selected value="{{$becomeMember->phone}}" type="text">
                    </div>
                    <div class="form-group label-floating is-select">
                        <label class="control-label">Race*</label>
                        <select class="selectpicker form-control" name="race">
                           <option selected value="{{$becomeMember->race}}">{{$becomeMember->race}}</option>
                       </select>
                    </div>

                    <div class="form-group label-floating is-select">
                        <label class="control-label">Religion</label>
                        <select class="selectpicker form-control" name="religion">
                           <option selected value="{{$becomeMember->religion}}">{{$becomeMember->religion}}</option>
                       </select>
                    </div>
                    <div class="form-group label-floating is-select">
                        <label class="control-label">Education</label>
                        <select class="selectpicker form-control" name="education">
                           <option selected value="{{$becomeMember->education}}">{{$becomeMember->education}}</option>
                       </select>
                    </div>
                    <div class="form-group label-floating is-select">
                        <label class="control-label">Health Status</label>
                        <select class="selectpicker form-control" name="health_status">
                            <option selected value="{{$becomeMember->health_status}}">{{$becomeMember->health_status}}</option>
                       </select>
                    </div>
                    <div class="form-group label-floating is-select">
                        <label class="control-label">Smoker</label>
                        <select class="selectpicker form-control" name="smoker">
                            <option >{{$becomeMember->smoker==1?'Yes':'No'}}</option>
                       </select>
                    </div>
                    <div class="form-group label-floating is-select">
                        <label class="control-label">Eye Color*</label>
                        <select class="selectpicker form-control" name="eye_color">
                           <option selected value="{{$becomeMember->eye_color}}">{{$becomeMember->eye_color}}</option>
                       </select>
                    </div>
                    <div class="form-group label-floating is-select">
                        <label class="control-label">Hair Color*</label>
                        <select class="selectpicker form-control" name="hair_color">
                           <option selected value="{{$becomeMember->hair_color}}">{{$becomeMember->hair_color}}</option>
                       </select>
                    </div>
                    <div class="form-group label-floating is-select">
                        <label class="control-label">Height*</label>
                        <select class="selectpicker form-control" name="height">
                            <option selected value="{{$becomeMember->height}}">{{$becomeMember->height}}</option>
                       </select>
                    </div>
                    <div class="form-group label-floating is-select">
                        <label class="control-label">Weight*</label>
                        <select class="selectpicker form-control" name="weight">
                           <option selected value="{{$becomeMember->weight}}">{{$becomeMember->weight}}</option>
                       </select>
                    </div>
                    <div class="form-group label-floating is-select">
                        <label class="control-label">Blood Type</label>
                        <select class="selectpicker form-control" name="blood_type">
                            <option selected value="{{$becomeMember->blood_type}}">{{$becomeMember->blood_type}}</option>
                       </select>
                    </div>
                    <div class="form-group label-floating is-select">
                        <label class="control-label">Have Passport</label>
                        <select class="selectpicker form-control" name="have_passport">
                        <option selected value="{{$becomeMember->have_passport}}">{{$becomeMember->have_passport}}</option>
                    </select>
                    </div>
                    <div class="form-group label-floating is-select">
                        <label class="control-label">Willing To Travel</label>
                        <select class="selectpicker form-control" name="willing_to_travel">
                            <option selected value="{{$becomeMember->willing_to_travel}}">{{$becomeMember->willing_to_travel}}</option>
                       </select>
                    </div>
                    <div class="form-group label-floating is-select">
                        <label class="control-label">Number of Children(non-donor)</label>
                        <select class="selectpicker form-control" name="number_of_children">
                            <option selected value="{{$becomeMember->number_of_children}}">{{$becomeMember->number_of_children}}</option>
                       </select>
                    </div>
                    <div class="form-group label-floating is-select">
                        <label class="control-label">Number of Surrogate Children</label>
                        <select class="selectpicker form-control" name="number_of_surrgate_children">
                            <option selected value="{{$becomeMember->number_of_surrogate_children}}">{{$becomeMember->number_of_surrogate_children}}</option>
                       </select>
                    </div>
                    <div class="form-group label-floating is-select">
                        <label class="control-label">Surrogate Type</label>
                        <select class="selectpicker form-control" name="surrgate_type">
                            <option selected value="{{$becomeMember->surrogate_type}}">{{$becomeMember->surrogate_type}}</option>
                       </select>
                    </div>
                    <div class="form-group label-floating is-select">
                        <label class="control-label">Have you been a {{$becomeMember->category}}
                         before?</label>
                        <select class="selectpicker form-control" name="given_service_before">
                            <option selected value="{{$becomeMember->given_service_before}}">{{$becomeMember->given_service_before}}</option>
                       </select>
                    </div>
                    <div class="form-group label-floating is-select">
                        <label class="control-label">Marital Status</label>
                        <select class="selectpicker form-control" name="marital_status">
                            <option selected value="{{$becomeMember->marital_status}}">{{$becomeMember->marital_status}}</option>
                       </select>
                    </div>
                    <div class="form-group label-floating is-select">
                        <label class="control-label">Spoken Languages</label>
                        <select class="selectpicker form-control"  name="spoken_language">
                        <option selected value="{{$becomeMember->spoken_language}}">{{$becomeMember->spoken_language}}</option>
                    </select>
                    </div>
                    <div class="form-group label-floating is-select">
                        <label class="control-label">Willing To Assist Couple Types:*</label>
                        <select class="selectpicker form-control" name="willing_to_assist">
                          <option selected value="{{$becomeMember->willing_to_assist}}">{{$becomeMember->willing_to_assist}}</option>
                        </select>
                    </div>
                    <div class="form-group label-floating">
                        <label class="control-label">How would you best describe yourself?</label>
                        <textarea class="form-control" name="describe_yourself" >{{$becomeMember->describe_yourself}}</textarea>
                    </div>
                    <div class="form-group label-floating">
                        <label class="control-label">Why do you want to be a 
                        {{$becomeMember->category}}
                        ?*</label>
                        <textarea class="form-control" name="why_become_member" >{{$becomeMember->why_become_member}}</textarea>
                    </div>
                    <div class="form-group label-floating">
                        <label class="control-label">Use this space to communicate a letter to potential intended parents.</label>
                        <textarea class="form-control" name="communication_letter">{{$becomeMember->communication_letter}}</textarea>
                    </div>
                    <div class="form-group label-floating">
                     <label class="control-label">Video URL</label>
                     <input class="form-control" value="{{$becomeMember->video_url}}" name="video_url" type="text">
                  </div>
                   
                </div>
                <a href="{{route('become_members.edit',$becomeMember->id)}}" class="btn btn-lg border-2  btn-primary btn-block border-radius-15 padding-15px box-shadow">Edit Member Details</a>
            </form>
                
@endsection
@section('additional-js')
<!-- <script src="https://code.jquery.com/jquery-3.4.1.min.js"></script>
  <script src="https://stackpath.bootstrapcdn.com/bootstrap/3.3.7/js/bootstrap.min.js"></script> -->
<script src="https://cdn.jsdelivr.net/npm/summernote@0.8.16/dist/summernote.min.js"></script>
 <script>
    $(document).ready(function() {
        $('#summernote').summernote();
    });
  </script>
@endsection