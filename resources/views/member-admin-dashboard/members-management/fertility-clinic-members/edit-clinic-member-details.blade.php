@extends('member-admin-dashboard.layouts.master')
@section('additional-head')
<!-- <link href="https://stackpath.bootstrapcdn.com/bootstrap/3.3.7/css/bootstrap.min.css" rel="stylesheet"> -->
<link href="https://cdn.jsdelivr.net/npm/summernote@0.8.16/dist/summernote.min.css" rel="stylesheet">
@endsection
@section('content')
<!-- Page Title -->
<div id="page-title" class="padding-30px background-white full-width">
   <div class="container">
      <ol class="breadcrumb opacity-5">
         <li><a href="#">Home</a></li>
         <li><a href="#">Clinic Member Details Page</a></li>
      </ol>
      <h1 class="font-weight-300">Member Details Page</h1>
   </div>
</div>
<!-- // Page Title -->
<div class="margin-tb-45px full-width">
<div class="padding-30px background-white border-radius-20 box-shadow">
   <h3><i class="far fa-list-alt margin-right-10px text-main-color"></i> {{$fertilityClinicMember->name}}'s Edit Page</h3>
   <hr>
   <form method="post" action="{{route('fertility_clinic_members.update',$fertilityClinicMember->id)}}" enctype="multipart/form-data">
    @method('put')
      @csrf
      <div class="form-group label-floating">
         @foreach(explode(',',$fertilityClinicMember->pictures) as $key =>$clinicMember)
         <img src="{{asset('storage/'.$clinicMember)}}" height="100">
         @endforeach
      </div>
      <div class="form-group label-floating">
         <label class="control-label">Clinic Name</label>
         <input class="form-control" value="{{$fertilityClinicMember->clinic_name}}" name="clinic_name" type="text">
      </div>
      <div class="form-group label-floating">
         <label class="control-label">Email*</label>
         <input class="form-control" value="{{$fertilityClinicMember->email}}" name="email" type="email">
      </div>
      <div class="form-group label-floating">
         <label class="control-label">Full Name</label>
         <input class="form-control" value="{{$fertilityClinicMember->name}}" name="name" type="text">
      </div>
      <div class="form-group label-floating is-select">
         <label class="control-label">Country</label>
         <select class="selectpicker form-control" name="country">
         @foreach($countries as $country)
         <option value="{{$country->country_name}}" {{$fertilityClinicMember->country==$country->country_name?'selected': ''}}>{{$country->country_name}}</option>
         @endforeach
         </select>
      </div>
      <div class="form-group label-floating">
         <label class="control-label">Address</label>
         <input class="form-control" value="{{$fertilityClinicMember->address}}" name="address"  type="text">
      </div>
      <div class="form-group label-floating">
         <label class="control-label">Phone/WhatsApp/Viber/Telegram</label>
         <input class="form-control" value="{{$fertilityClinicMember->phone}}" name="phone" type="text">
      </div>
      <div class="form-group label-floating">
         <label class="control-label">Website/Blog</label>
         <input class="form-control" name="website" type="text" value="{{$fertilityClinicMember->website}}">
      </div>
      <div class="form-group label-floating is-select">
         <label class="control-label">Groups that you provide services for</label>
         <div class="row">
            <div class="col-md-6">
               <div class="input-group">
                  <select class="selectpicker form-control jq_multiselect" enable_select_all="false" multiple="multiple" name="provide_services_for[]">
                     @foreach(explode(',',$formText->provide_services_for_data) as $key => $provide_services_for_data)
                              <option value="{{$provide_services_for_data}}" {{$provide_services_for_data==$fertilityClinicMember->provide_services_for ? 'selected': ''}}>{{$provide_services_for_data}}</option>
                              @endforeach
                  </select>
               </div>
            </div>
            <div class="col-md-6">
               <div class="form-group">
                  <label class="control-label">Selected By User: {{$fertilityClinicMember->provide_services_for}}</label>
               </div>
            </div>
         </div>
         <hr>
      </div>
      <div class="form-group label-floating is-select">
         <label class="control-label">Treatments</label>
         <div class="row">
            <div class="col-md-6">
               <div class="input-group">
                  <select class="selectpicker form-control jq_multiselect" multiple="multiple" enable_select_all="false"  name="treatments[]">
                     @foreach(explode(',',$formText->treatments_data) as $key => $treatments_data)
                              <option value="{{$treatments_data}}" {{$treatments_data==$fertilityClinicMember->treatments ? 'selected': ''}}>{{$treatments_data}}</option>
                              @endforeach
                  </select>
               </div>
            </div>
            <div class="col-md-6">
               <div class="form-group">
                  <label class="control-label">Selected By User: {{$fertilityClinicMember->treatments}}</label>
               </div>
            </div>
         </div>
         <hr>
      </div>
      <div class="form-group label-floating is-select">
         <label class="control-label">Services that you provide</label>
         <div class="row">
            <div class="col-md-6">
               <div class="input-group">
                  <select class="selectpicker form-control spoken_language jq_multiselect" name="providing_service[]" multiple="multiple">
                     @foreach(explode(',',$formText->providing_service_data) as $key => $providing_service_data)
                              <option value="{{$providing_service_data}}" {{$providing_service_data==$fertilityClinicMember->providing_service ? 'selected': ''}}>{{$providing_service_data}}</option>
                              @endforeach
                  </select>
               </div>
            </div>
            <div class="col-md-6">
               <div class="form-group">
                  <label class="control-label">Selected By User: {{$fertilityClinicMember->providing_service}}</label>
               </div>
            </div>
         </div>
         <hr>
      </div>
      <div class="form-group label-floating is-select">
         <label class="control-label">Willing to assist intended parents/surrogates/donors for the countries:</label>
         <div class="row">
            <div class="col-md-6">
               <div class="input-group">
                  <select class="selectpicker form-control jq_multiselect" name="willing_to_assist_countries[]" multiple="multiple">
                  @foreach($countries as $country)
                  <option value="{{$country->country_name}}" {{$fertilityClinicMember->willing_to_assist_countries==$country->country_name?'selected' : ''}}>{{$country->country_name}}</option>
                  @endforeach
                  </select>
               </div>
            </div>
            <div class="col-md-6">
               <div class="form-group">
                  <label class="control-label">Selected By User: {{$fertilityClinicMember->willing_to_assist_countries}}</label>
               </div>
            </div>
         </div>
         <hr>
      </div>
      <div class="form-group label-floating is-select">
         <label class="control-label">Willing To Assist Couple Types</label>
         <div class="row">
            <div class="col-md-6">
               <div class="input-group">
                  <select class="selectpicker form-control spoken_language jq_multiselect" name="willing_to_assist[]" multiple="multiple">
                     @foreach(explode(',',$formText->willing_to_assist_data) as $key => $willing_to_assist_data)
                              <option value="{{$willing_to_assist_data}}" {{$willing_to_assist_data==$fertilityClinicMember->willing_to_assist ? 'selected': ''}}>{{$willing_to_assist_data}}</option>
                              @endforeach
                  </select>
               </div>
            </div>
            <div class="col-md-6">
               <div class="form-group">
                  <label class="control-label">Selected By User: {{$fertilityClinicMember->willing_to_assist}}</label>
               </div>
            </div>
         </div>
         <hr>
      </div>
      <div class="form-group label-floating">
         <label class="control-label">Description of practise/clinic?</label>
         <textarea class="form-control" name="describe_clinic" >{{$fertilityClinicMember->describe_clinic}}</textarea>
      </div>
      <div class="form-group label-floating">
         <label class="control-label">Video URL</label>
         <input class="form-control" value="{{$fertilityClinicMember->video_url}}" name="video_url" type="text">
      </div>
      <div class="form-group label-floating is-select">
                        <label class="control-label">Stars</label>
                        <div class="row">
                      <div class="col-md-6">
                        <div class="input-group">
                        <select class="selectpicker form-control" name="stars">
                            <option value="">Please Select</option>
                            @if($fertilityClinicMember->clinic_review)
                            <option value="0" {{$fertilityClinicMember->clinic_review->stars==0?'selected':''}}>0</option>
                            <option value="1"  {{$fertilityClinicMember->clinic_review->stars==1?'selected':''}}>1</option>
                            <option value="2"  {{$fertilityClinicMember->clinic_review->stars==2?'selected':''}}>2</option>
                            <option value="3"  {{$fertilityClinicMember->clinic_review->stars==3?'selected':''}}>3</option>
                            <option value="4"  {{$fertilityClinicMember->clinic_review->stars==4?'selected':''}}>4</option>
                            <option value="5"  {{$fertilityClinicMember->clinic_review->stars==5?'selected':''}}>5</option>
                            @else
                            <option value="0">0</option>
                            <option value="1">1</option>
                            <option value="2">2</option>
                            <option value="3">3</option>
                            <option value="4">4</option>
                            <option value="5">5</option>
                            @endif
                       </select>
                     </div>
                   </div>
                    </div>
                  </div> 
                   <div class="form-group label-floating">
                        <label class="control-label">Your Review</label>
                        @if($fertilityClinicMember->clinic_review)
                        <textarea class="form-control" id="summernote" name="review"> {{$fertilityClinicMember->clinic_review->review}}</textarea>
                        @else
                        <textarea class="form-control" id="summernote" name="review"></textarea>
                        @endif

                    </div>
       <div class="form-group label-floating">
         <label class="control-label">Upload New Images(old images would be deleted)</label><br>
         <span class="btn btn-success btn-success2 fileinput-button">
            <i class="fa fa-plus"></i>
            <span>Add files...</span>
            <input type="file" name="pictures[]" multiple="multiple">
         </span>
      </div>
</div>
<button type="submit" class="btn btn-lg border-2  btn-primary btn-block border-radius-15 padding-15px box-shadow">Update Information</button>
</form>
@endsection
@section('additional-js')
<script src="https://cdn.jsdelivr.net/npm/summernote@0.8.16/dist/summernote.min.js"></script>
 <script>
    $(document).ready(function() {
        $('#summernote').summernote();
    });
  </script>
<script type="text/javascript" src="{{asset('assets/assets/bootstrap-datepicker.js')}}"></script>
<script type="text/javascript" src="{{asset('assets/assets/bootstrap-datepicker.init.js')}}"></script>
<script type="text/javascript" src="{{asset('assets/assets/signup.js')}}"></script>
<script type="text/javascript" src="{{asset('assets/assets/bootstrap-multiselect.js')}}"></script>
<link rel="stylesheet" type="text/css" href="{{asset('assets/assets/datepicker3.css')}}">
<link rel="stylesheet" type="text/css" href="{{asset('assets/assets/bootstrap-multiselect.css')}}">
<link rel="stylesheet" type="text/css" href="{{asset('assets/assets/member_generic.css')}}">
<script src='https://www.google.com/recaptcha/api.js'></script>
@endsection

