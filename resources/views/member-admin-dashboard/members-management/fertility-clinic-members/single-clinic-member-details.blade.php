@extends('member-admin-dashboard.layouts.master')
@section('additional-head')
<!-- <link href="https://stackpath.bootstrapcdn.com/bootstrap/3.3.7/css/bootstrap.min.css" rel="stylesheet"> -->
@endsection
@section('content')
<!-- Page Title -->
                <div id="page-title" class="padding-30px background-white full-width">
                    <div class="container">
                        <ol class="breadcrumb opacity-5">
                            <li><a href="#">Home</a></li>
                            <li><a href="#">Clinic Member Details Page</a></li>
                        </ol>
                        <h1 class="font-weight-300">Member Details Page</h1>
                    </div>
                </div>
                <!-- // Page Title -->

                <div class="margin-tb-45px full-width">
                    <div class="padding-30px background-white border-radius-20 box-shadow">
                        <h3><i class="far fa-list-alt margin-right-10px text-main-color"></i> {{$fertilityClinicMember->name}}'s Detailed Information </h3>
                        <hr>
                        <form method="post" action="{{route('fertility_clinic_members.store')}}" enctype="multipart/form-data">
                    @csrf
                     <div class="form-group label-floating">
                        @foreach(explode(',',$fertilityClinicMember->pictures) as $key =>$clinicMember)
                        <img src="{{asset('storage/'.$clinicMember)}}" height="100">
                        @endforeach
                    </div>
                    <div class="form-group label-floating">
                        <label class="control-label">Clinic Name</label>
                        <input class="form-control" selected value="{{$fertilityClinicMember->clinic_name}}" name="clinic_name" type="text" disabled>
                    </div>
                    <div class="form-group label-floating">
                        <label class="control-label">Your Email*</label>
                        <input class="form-control" disabled value="{{$fertilityClinicMember->email}}" name="email" type="email">
                    </div>
                    <div class="form-group label-floating">
                        <label class="control-label">Name</label>
                        <input class="form-control" disabled value="{{$fertilityClinicMember->name}}" name="name" type="text">
                    </div>
                    <div class="form-group label-floating is-select">
                        <label class="control-label">Country</label>
                        <select class="selectpicker form-control" name="country">
                          
                          <option value="{{$fertilityClinicMember->country}}" selected>{{$fertilityClinicMember->country}}</option>
                       </select>
                    </div>
                    <div class="form-group label-floating">
                        <label class="control-label">Address</label>
                        <input class="form-control" disabled value="{{$fertilityClinicMember->address}}"  type="text">
                    </div>
                    <div class="form-group label-floating">
                        <label class="control-label">Phone/WhatsApp/Viber/Telegram</label>
                        <input class="form-control" selected value="{{$fertilityClinicMember->phone}}" disabled type="text">
                    </div>
                    <div class="form-group label-floating">
                           <label class="control-label">Website/Blog</label>
                           <input class="form-control" name="website" type="text" disabled value="{{$fertilityClinicMember->website}}">
                        </div>
                    <div class="form-group label-floating is-select">
                        <label class="control-label">Groups that you provide services for</label>
                        <select class="selectpicker form-control" name="willing_to_assist">
                          <option selected value="{{$fertilityClinicMember->provide_services_for}}">{{$fertilityClinicMember->provide_services_for}}</option>
                        </select>
                    </div>
                    <div class="form-group label-floating is-select">
                        <label class="control-label">Treatments</label>
                        <select class="selectpicker form-control">
                          <option selected value="{{$fertilityClinicMember->treatments}}">{{$fertilityClinicMember->treatments}}</option>
                        </select>
                    </div>
                    <div class="form-group label-floating is-select">
                        <label class="control-label">Services that you provide</label>
                        <select class="selectpicker form-control">
                          <option selected value="{{$fertilityClinicMember->providing_service}}">{{$fertilityClinicMember->providing_service}}</option>
                        </select>
                    </div>
                    <div class="form-group label-floating is-select">
                        <label class="control-label">Willing to assist intended parents/surrogates/donors for the countries:</label>
                        <select class="selectpicker form-control">
                          <option selected value="{{$fertilityClinicMember->willing_to_assist_countries}}">{{$fertilityClinicMember->willing_to_assist_countries}}</option>
                        </select>
                    </div>
                    <div class="form-group label-floating is-select">
                        <label class="control-label">Willing To Assist Couple Types</label>
                        <select class="selectpicker form-control">
                          <option selected value="{{$fertilityClinicMember->willing_to_assist}}">{{$fertilityClinicMember->willing_to_assist}}</option>
                        </select>
                    </div>
                   <div class="form-group label-floating">
                        <label class="control-label">Description of practise/clinic?</label>
                        <textarea class="form-control" name="describe_yourself" >{{$fertilityClinicMember->describe_clinic}}</textarea>
                    </div>
                   <div class="form-group label-floating">
                        <label class="control-label">Video URL</label>
                        <input class="form-control" selected value="{{$fertilityClinicMember->video_url}}" name="clinic_name" type="text" disabled>
                    </div> 
                   
                </div>
                <a href="{{route('fertility_clinic_members.edit',$fertilityClinicMember->id)}}" class="btn btn-lg border-2  btn-primary btn-block border-radius-15 padding-15px box-shadow">Edit Information</a>
            </form>
                
@endsection
@section('additional-js')
<!-- <script src="https://code.jquery.com/jquery-3.4.1.min.js"></script>
  <script src="https://stackpath.bootstrapcdn.com/bootstrap/3.3.7/js/bootstrap.min.js"></script> -->
<script src="https://cdn.jsdelivr.net/npm/summernote@0.8.16/dist/summernote.min.js"></script>
 <script>
    $(document).ready(function() {
        $('#summernote').summernote();
    });
  </script>
@endsection