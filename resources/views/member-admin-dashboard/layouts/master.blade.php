<!DOCTYPE html>
<html lang="en-US">

@include('member-admin-dashboard.partials.head')
<body>

   @include('member-admin-dashboard.partials.navigation')
    <!-- // Header  -->


    <!-- Navigation-->
    @include('member-admin-dashboard.partials.side-bar')
    <div class="content-wrapper">
        <div class="container-fluid overflow-hidden">
            <div class="row margin-tb-90px margin-lr-10px sm-mrl-0px">

                @include('member-admin-dashboard.partials.analytics')

                <div class="col-12">
                    @if($message = Session::get('success'))
                      <div class="alert alert-success alert-block">
                      <button type="button" class="close" data-dismiss="alert">×</button> 
                      <strong>{{ $message }}</strong>
                    </div>
                    @endif
                    @if ($message = Session::get('danger'))
                    <div class="alert alert-danger alert-block">
                    <button type="button" class="close" data-dismiss="alert">×</button> 
                    <strong>{{ $message }}</strong>
                    </div>
                    @endif
                	<!-- <div class="row"> -->
                    @yield('content')
                    <!-- </div> -->
                </div>
            </div>


        </div>
        <!-- /.container-fluid-->
        <!-- /.content-wrapper-->
       @include('member-admin-dashboard.partials.footer')
        <!-- Scroll to Top Button-->
        <a class="scroll-to-top rounded" href="#page-top">
          <i class="fa fa-angle-up"></i>
        </a>
        <!-- Logout Modal-->
        <div class="modal fade" id="exampleModal" tabindex="-1" role="dialog" aria-labelledby="exampleModalLabel" aria-hidden="true">
            <div class="modal-dialog" role="document">
                <div class="modal-content">
                    <div class="modal-header">
                        <h5 class="modal-title" id="exampleModalLabel">Ready to Leave?</h5>
                        <button class="close" type="button" data-dismiss="modal" aria-label="Close">
                  <span aria-hidden="true">×</span>
                </button>
                    </div>
                    <div class="modal-body">Select "Logout" below if you are ready to end your current session.</div>
                    <div class="modal-footer">
                        <button class="btn btn-secondary" type="button" data-dismiss="modal">Cancel</button>
                        <a class="btn btn-primary" href="page-login.html">Logout</a>
                    </div>
                </div>
            </div>
        </div>
    </div>

    @include('member-admin-dashboard.partials.js-libraries')
    @yield('modal')
    @yield('additional-js')

</body>
</html>
