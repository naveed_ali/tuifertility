@extends('member-admin-dashboard.layouts.master')
@section('additional-head')
<!-- <link href="https://stackpath.bootstrapcdn.com/bootstrap/3.3.7/css/bootstrap.min.css" rel="stylesheet"> -->
<link href="https://cdn.jsdelivr.net/npm/summernote@0.8.16/dist/summernote.min.css" rel="stylesheet">
@endsection
@section('content')
<!-- Page Title -->
                <div id="page-title" class="padding-30px background-white full-width">
                    <div class="container">
                        <ol class="breadcrumb opacity-5">
                            <li><a href="#">Home</a></li>
                            <li><a href="#">Edit Page</a></li>
                        </ol>
                        <h1 class="font-weight-300">Edit Page</h1>
                    </div>
                </div>
                <!-- // Page Title -->

                <div class="margin-tb-45px full-width">
                    <div class="padding-30px background-white border-radius-20 box-shadow">
                        <h3><i class="far fa-list-alt margin-right-10px text-main-color"></i> Page Information </h3>
                        <hr>
                        <form method="post" action="{{route('member.pages.update',$page->slug)}}" enctype="multipart/form-data">
                            @method('put')
                        	@csrf
                        	<div class="form-group margin-bottom-20px">
                                <div class="row">
                                    <div class="col-md-12">
                                        <label><i class="far fa-folder-open margin-right-10px"></i> Select Menu</label>
                                        <select class="form-control form-control-sm" name="menu_id">
                                          <option disabled="">Update Menu for the page</option>
                                          @foreach($member_menus as $menu)
                                          <option value="{{$menu->id}}" {{$page->menu_id==$menu->id?'selected':''}}>{{$menu->name}}</option>
                                          @endforeach
                                        </select>
                                    </div>
                                </div>
                            </div>
                            <div class="form-group margin-bottom-20px">
                                <label><i class="far fa-file margin-right-10px"></i> Page Name</label>
                                <input type="text" class="form-control form-control-sm" value="{{$page->name}}" name="name" id="ListingTitle" placeholder="Page Name">
                            </div>
                            <div class="form-group margin-bottom-20px">
                                <label><i class="far fa fa-link margin-right-10px"></i> URL ({{url('/')}})</label>
                                <input type="text" class="form-control form-control-sm" name="slug" id="ListingKeywords" value="{{$page->slug}}">
                            </div>
                            <div class="row">
                                <div class="col-md-12 margin-bottom-20px">
                                    <label><i class="fas fa fa-info-circle margin-right-10px"></i> Title</label>
                                    <input type="text" name="title" class="form-control form-control-sm" value="{{$page->title}}">
                                </div>
                                
                            </div>
                            <div class="row">
                                <div class="col-md-12 margin-bottom-20px">
                                    <label><i class="fas fa fa-check margin-right-10px"></i> Status</label><br>
                                   	<label class="radio-inline">
							      	<input type="radio" value="1" name="status"
                                    {{$page->status==1?'checked':''}}> Published
							    	</label>
							    	<label class="radio-inline">
							      	<input type="radio" value="0" name="status"
                                    {{$page->status==0?'checked':''}}> Unpublished
							    	</label>
                                </div>
                                
                            </div>
                            <div class="margin-bottom-45px full-width">
                                <div class="padding-30px background-white border-radius-20 box-shadow">
                                    <h3><i class="far fa-list-alt margin-right-10px text-main-color"></i>Meta Description </h3>
                                    <hr>
                                    <div class="margin-bottom-20px">
                                        <textarea name="meta_data" class="form-control" rows="5">{{$page->meta_data}}</textarea>
                                    </div>  
                                </div>
                            </div>
                            <div class="margin-bottom-45px full-width">
			                    <div class="padding-30px background-white border-radius-20 box-shadow">
			                        <h3><i class="far fa-list-alt margin-right-10px text-main-color"></i>Text</h3>
			                        <hr>
			                        <div class="margin-bottom-20px">
			                            <textarea id="summernote" name="description" class="form-control" rows="3">{{$page->description}}</textarea>
			                        </div>  
			                    </div>
			                </div>
                            <div class="margin-bottom-45px full-width">
                                <div class="padding-30px background-white border-radius-20 box-shadow">
                                    <h3><i class="far fa-image margin-right-10px text-main-color"></i>Photo</h3>
                                    <hr>
                                    <div class="margin-bottom-20px">
                                        <img src="{{asset('storage/'.$page->picture)}}">
                                    </div>  
                                </div>
                            </div>
                            <div class="margin-bottom-45px full-width">
                                <div class="padding-30px background-white border-radius-20 box-shadow">
                                    <h3><i class="far fa-image margin-right-10px text-main-color"></i>Add new Photo and old photo will be replaced</h3>
                                    <hr>
                                    <div class="margin-bottom-20px">
                                        <input type="file" name="picture">
                                    </div>  
                                </div>
                            </div>
                    </div>
                </div>
                <button type="submit" class="btn btn-lg border-2  btn-primary btn-block border-radius-15 padding-15px box-shadow">Update Page</button>
                </form>
@endsection
@section('additional-js')
<!-- <script src="https://code.jquery.com/jquery-3.4.1.min.js"></script>
  <script src="https://stackpath.bootstrapcdn.com/bootstrap/3.3.7/js/bootstrap.min.js"></script> -->
<script src="https://cdn.jsdelivr.net/npm/summernote@0.8.16/dist/summernote.min.js"></script>
 <script>
    $(document).ready(function() {
        $('#summernote').summernote();
    });
  </script>
@endsection