<head>
    <title>TUI FERTILITY</title>
    <meta name="author" content="Nile-Theme">
    <meta name="robots" content="index follow">
    <meta name="googlebot" content="index follow">
    <meta http-equiv="content-type" content="text/html; charset=utf-8">
    <meta name="keywords" content="directory, doctor, doctor directory, Health directory, listing, map, medical, medical directory, professional directory, reservation, reviews">
    <meta name="description" content="Health Care & Medical Services Directory">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <!-- google fonts -->
    <link href="fonts.googleapis.com/csscfd8.css?family=Open+Sans:400,600,800%7CPoppins:300i,300,400,700,400i,500%7CDancing+Script:700%7CDancing+Script:700" rel="stylesheet">
    <!-- animate -->
    <link rel="stylesheet" href="{{asset('assets/css/animate.css')}}" />
    <!-- owl Carousel assets -->
    <link href="{{asset('assets/css/owl.carousel.css')}}" rel="stylesheet">
    <link href="{{asset('assets/css/owl.theme.css')}}" rel="stylesheet">
    <!-- bootstrap -->
    <link rel="stylesheet" href="{{asset('assets/css/bootstrap.min.css')}}">
    <!-- hover anmation -->
    <link rel="stylesheet" href="{{asset('assets/css/hover-min.css')}}">
    <!-- flag icon -->
    <link rel="stylesheet" href="{{asset('assets/css/flag-icon.min.css')}}">
    <!-- main style -->
    <link rel="stylesheet" href="{{asset('assets/css/style.css')}}">
    <link rel="stylesheet" href="{{asset('assets/css/custom.css')}}">
    <!-- colors -->
    <link rel="stylesheet" href="{{asset('assets/css/colors/main.css')}}">
    <!-- elegant icon -->
    <link rel="stylesheet" href="{{asset('assets/css/elegant_icon.css')}}">
    <!-- admin style -->
    <link rel="stylesheet" href="{{asset('assets/css/sb-admin.css')}}">
    <!-- jquery library  -->
    <script src="{{asset('assets/js/jquery-3.2.1.min.js')}}"></script>
    <!-- fontawesome  -->
    <script defer src="https://use.fontawesome.com/releases/v5.0.6/js/all.js"></script>
    <style type="text/css">
    .header-top {
    position: relative;
    background: #ededed;
    color: #555555;
    z-index: 5;
    }
    .auto-container {
    position: static;
    max-width: 1140px;
    padding: 0px 15px;
    margin: 0 auto;}
    .header-top .top-left {
    position: relative;
    float: left;
    padding: 5px 0px 0px 0px;
    color: #c9c9c9;
    font-size: 12px;}
    .header-top .top-right {
    position: relative;
    padding: 5px 0px 0px 0px;
    float: right;}
    .top-right ul li {
    display: inline-flex; list-style: none;
    }
    .header-top ul li {
        position: relative;
        float: left;}
</style>
@yield('additional-head')
</head>