<nav class="navbar navbar-expand-lg navbar-dark z-index-9  fixed-top" id="mainNav">

        <div class="collapse navbar-collapse" id="navbarResponsive">

            <ul class="navbar-nav navbar-sidenav background-main-color admin-nav" id="admin-nav">
                <li class="nav-item">
                    <span class="nav-title-text">Main</span>
                </li>
                <li class="nav-item" data-toggle="tooltip" data-placement="right" title="Dashboard">
                    <a class="nav-link " href="dashboard-home.html">
                        <i class="fas fa-fw fa-home"></i><span class="nav-link-text">Dashboard</span>
                    </a>
                </li>
                <li class="nav-item" data-toggle="tooltip" data-placement="right" title="My items">
                    <a class="nav-link" href="{{route('become_members.index','surrogate-mothers')}}">
                <i class="fa fa-fw fa-table"></i>
                <span class="nav-link-text">Surrogate Mothers</span>
              </a>
                </li>
                <li class="nav-item" data-toggle="tooltip" data-placement="right" title="My items">
                    <a class="nav-link" href="{{route('become_members.index','sperm-donors')}}">
                <i class="fa fa-fw fa-table"></i>
                <span class="nav-link-text">Sperm Donors</span>
              </a>
                </li>
                <li class="nav-item" data-toggle="tooltip" data-placement="right" title="My items">
                    <a class="nav-link" href="{{route('become_members.index','egg-donors')}}">
                <i class="fa fa-fw fa-table"></i>
                <span class="nav-link-text">Egg Donors</span>
              </a>
                </li>
                <li class="nav-item" data-toggle="tooltip" data-placement="right" title="My items">
                    <a class="nav-link" href="{{route('fertility_clinic_members.index')}}">
                <i class="fa fa-fw fa-table"></i>
                <span class="nav-link-text">Fertility Clinics</span>
              </a>
                </li>
               
               
                <li class="nav-item" data-toggle="tooltip" data-placement="right" title="My Profile">
                    <a class="nav-link" href="dashboard-my-profile.html">
                        <i class="fa fa-fw fa-user-circle"></i>
                        <span class="nav-link-text">My Profile</span>
                    </a>
                </li>
                <li class="nav-item">
                    <span class="nav-title-text">CMS</span>
                </li>
                <li class="nav-item" data-toggle="tooltip" data-placement="right" title="Text Management">
                    <a class="nav-link {{url()->current()==route('all.pages')? 'active': ''}}" href="{{route('all.pages')}}">
                        <i class="far fa-fw fa-bookmark"></i>
                        <span class="nav-link-text">Text Management</span>
                    </a>
                </li>
                <li class="nav-item" data-toggle="tooltip" data-placement="right" title="Menus Management">
                    <a class="nav-link" href="{{route('member.menus.index')}}">
                        <i class="far fa-fw fa-bookmark"></i>
                        <span class="nav-link-text">Menus Management</span>
                    </a>
                </li>
                <li class="nav-item" data-toggle="tooltip" data-placement="right" title="Pages Management">
                    <a class="nav-link" href="{{route('member.pages.index')}}">
                        <i class="far fa-fw fa-bookmark"></i>
                        <span class="nav-link-text">Pages Management</span>
                    </a>
                </li>
                <li class="nav-item" data-toggle="tooltip" data-placement="right" title="Sub Pages Management">
                    <a class="nav-link" href="{{route('member.subpages.index')}}">
                        <i class="far fa-fw fa-bookmark"></i>
                        <span class="nav-link-text">Sub Pages Management</span>
                    </a>
                </li>
                <li class="nav-item" data-toggle="tooltip" data-placement="right" title="Sing Out">
                    <a class="nav-link" href="#" data-toggle="modal" data-target="#exampleModal">
                        <i class="fa fa-fw fa-sign-out-alt"></i>
                        <span class="nav-link-text">Sing Out</span>
                    </a>
                </li>
            </ul>
        </div>
    </nav>