 <header class="background-white box-shadow fixed-top z-index-99">
        <nav class="container-fluid header-in">
            <div class="row">
                <div class="col-xl-2 col-lg-2">
                    <a id="logo" href="{{url('/')}}" class="d-inline-block margin-tb-15px"><img src="{{asset('assets/img/logo-tui.png')}}" alt=""></a>
                    <a class="mobile-toggle padding-13px background-main-color" href="#"><i class="fas fa-bars"></i></a>
                </div>
                <div class="col-xl-6 col-lg-8 position-inherit">
                    <ul id="menu-main" class="nav-menu float-lg-right link-padding-tb-20px">
                         @foreach($member_menus as $menuitem)
                                <li class="has-dropdown">
                                    <a href="{{url('/'.$menuitem->slug)}}">{{$menuitem->name}}</a>
                                    @if($menuitem->pages->count()>0)
                                        <ul class="sub-menu">
                                            @foreach($menuitem->pages as $page)
                                            <li style="position: relative; white-space: nowrap;"><a href="{{url('/'.$menuitem->slug.'/'.$page->slug)}}">{{$page->name}}</a>
                                                @if($page->subPages->count()>0)
                                               <ul class="sub-menu" style="position: absolute;">
                                                    @foreach($page->subPages as $subpage)
                                                    <li><a href="{{url('/'.$menuitem->slug.'/'.$page->slug.'/'.$subpage->slug)}}">{{$subpage->name}}</a>   
                                                    </li>
                                                    @endforeach
                                                </ul>
                                                @endif 
                                            </li>
                                            @endforeach
                                        </ul>
                                    @endif
                                </li>
                                @endforeach
                    </ul>

                </div>
                <div class="col-xl-4 d-none d-xl-block">

                    <div class="nav-item float-right">
                        <a href="#" class="nav-link margin-top-10px" data-toggle="modal" data-target="#exampleModal">
                            <div class="text-grey-3"><i class="fa fa-fw fa-sign-out-alt"></i>Logout</div>
                        </a>
                    </div>
                     <div class="nav-item dropdown float-right">
                        <a href="dashboard-my-profile.html" class="margin-top-15px d-inline-block text-grey-3 margin-right-15px"><img src="{{asset('assets/img/testimonia')}}l-3.png" class="height-30px border-radius-30" alt=""> Salim Aldosery</a>
                    </div>
                </div>
            </div>
        </nav>
    </header>