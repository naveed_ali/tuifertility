@extends('member-admin-dashboard.layouts.master')

@section('content')
@if ($errors->any())
    <div class="alert alert-danger">
        <ul>
            @foreach ($errors->all() as $error)
                <li>{{ $error }}</li>
            @endforeach
        </ul>
    </div>
@endif
<form method="post" action="{{route('member.menus.update',$menu->slug)}}">
	@method('put')
	@csrf
	<div class="form-group">
	<label for="name">Menu Name</label>
	<input type="text" class="form-control" name="name" value="{{$menu->name}}" id="name" aria-describedby="Menu Name" placeholder="Menu Name">
	</div>
	<div class="form-group">
	<label for="slug">Slug</label>
	<input type="text" class="form-control" name="slug" id="slug" value="{{$menu->slug}}" placeholder="Menu Slug">
	</div>
	<button type="submit" class="btn btn-primary">Update Menu</button>
</form>
@endsection