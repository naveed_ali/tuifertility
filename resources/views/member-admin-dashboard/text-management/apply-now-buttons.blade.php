@extends('member-admin-dashboard.layouts.master')
@section('content')
<!-- Page Title -->
                <div id="page-title" class="padding-30px background-white full-width">
                    <div class="container">
                        <ol class="breadcrumb opacity-5">
                            <li><a href="#">Home</a></li>
                            <li><a href="#">Apply Now Button Text Page</a></li>
                        </ol>
                        <h1 class="font-weight-300">Apply Now Button Text Page</h1>
                    </div>
                </div>
                <!-- // Page Title -->

                <div class="margin-tb-45px full-width">
                     @if ($errors->any())
                        <div class="alert alert-danger">
                            <ul>
                                @foreach ($errors->all() as $error)
                                    <li>{{ $error }}</li>
                                @endforeach
                            </ul>
                        </div>
                    @endif
                    <div class="padding-30px background-white border-radius-20 box-shadow">
                        <h3><i class="far fa-list-alt margin-right-10px text-main-color"></i>Apply Now Buttons </h3>
                        <hr>
                        <form method="post" action="{{route('applynow.button.update',$button->id)}}">
                            @method('put')
                            @csrf
                    <div class="form-group label-floating">
                        <label class="control-label">Surrogate Mother Button Text</label>
                        <input class="form-control" placeholder="" value="{{$button->surrogate_mother}}" name="surrogate_mother" type="text">
                    </div>
                    <div class="form-group label-floating">
                        <label class="control-label">Sperm Donor Button Text</label>
                        <input class="form-control" placeholder="" value="{{$button->sperm_donor}}" name="sperm_donor" type="text">
                    </div>
                    <div class="form-group label-floating">
                        <label class="control-label">Egg Donor Button Text</label>
                        <input class="form-control" placeholder="" value="{{$button->egg_donor}}" name="egg_donor" type="text">
                    </div>
                    <div class="form-group label-floating">
                        <label class="control-label">Fertility Clinic Button Text</label>
                        <input class="form-control" placeholder="" value="{{$button->fertility_clinic}}" name="fertility_clinic" type="text">
                    </div>
                <button type="submit" class="btn btn-lg border-2  btn-primary btn-block border-radius-15 padding-15px box-shadow">Update Text</button>
            </form>
                
@endsection