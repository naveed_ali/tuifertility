@extends('member-admin-dashboard.layouts.master')
@section('content')
<!-- Page Title -->
                <div id="page-title" class="padding-30px background-white full-width">
                    <div class="container">
                        <ol class="breadcrumb opacity-5">
                            <li><a href="#">Home</a></li>
                            <li><a href="#">Edit Form Text Page</a></li>
                        </ol>
                        <h1 class="font-weight-300">Fertiity Clinic Form Text Page</h1>
                    </div>
                </div>
                <!-- // Page Title -->

                <div class="margin-tb-45px full-width">
                     @if ($errors->any())
                        <div class="alert alert-danger">
                            <ul>
                                @foreach ($errors->all() as $error)
                                    <li>{{ $error }}</li>
                                @endforeach
                            </ul>
                        </div>
                    @endif
                    <div class="padding-30px background-white border-radius-20 box-shadow">
                        <h3><i class="far fa-list-alt margin-right-10px text-main-color"></i>Fertility Clinic's Edit Form Fields </h3>
                        <hr>
                        <form method="post" action="{{route('clinic.member.form.update',$clinicMember->id)}}">
                            @method('put')
                            @csrf
                    <div class="form-group label-floating">
                        <label class="control-label">Page Title Text</label>
                        <input class="form-control" placeholder="" value="{{$clinicMember->title}}" name="title" type="text">
                    </div>
                    <div class="form-group label-floating">
                        <label class="control-label">Small Note Text</label>
                        <input class="form-control" placeholder="" value="{{$clinicMember->note}}" name="note" type="text">
                    </div>
                    <div class="form-group label-floating">
                        <label class="control-label">Email Text</label>
                        <input class="form-control" placeholder="" value="{{$clinicMember->email}}" name="email" type="text">
                    </div>
                    <div class="form-group label-floating">
                        <label class="control-label">Full Name Text</label>
                        <input class="form-control" placeholder="" value="{{$clinicMember->name}}" name="name" type="text">
                    </div><div class="form-group label-floating">
                        <label class="control-label">Name of clinic Text</label>
                        <input class="form-control" placeholder="" value="{{$clinicMember->clinic_name}}" name="clinic_name" type="text">
                    </div>
                    <div class="form-group label-floating">
                        <label class="control-label">Website/Blog Text</label>
                        <input class="form-control" value="{{$clinicMember->website}}" name="website" type="text">
                    </div>
                    <div class="form-group label-floating is-select">
                        <label class="control-label">Country Text</label>
                        <input class="form-control" value="{{$clinicMember->country}}" name="country" type="text">
                    </div>
                    <div class="form-group label-floating">
                        <label class="control-label">Address Text</label>
                        <input class="form-control" value="{{$clinicMember->address}}" name="address" placeholder="Address" type="text">
                    </div>
                    <div class="form-group label-floating">
                        <label class="control-label">Phone/WhatsApp/Viber/Telegram Text</label>
                        <input class="form-control" value="{{$clinicMember->phone}}" name="phone" placeholder="Phone/WhatsApp/Viber/Telegram" type="text">
                    </div>
                    
                    <div class="form-group label-floating is-select">
                        <label class="control-label">Select the groups that you provide services for:* Text</label>
                        <input class="form-control" value="{{$clinicMember->provide_services_for}}" name="provide_services_for" type="text">
                  </div>
                  <div class="form-group label-floating is-select">
                        <label class="control-label">Groups That your provide serve Data</label>
                        <textarea class="form-control" name="provide_services_for_data">{{$clinicMember->provide_services_for_data}}</textarea>
                    </div>
                  <div class="form-group label-floating is-select">
                        <label class="control-label">Select the services that you provide:* Text</label>
                        <input class="form-control" value="{{$clinicMember->providing_service}}" name="providing_service" type="text">
                  </div>
                  <div class="form-group label-floating is-select">
                        <label class="control-label">Services you provide Data Text</label>
                        <textarea class="form-control" name="providing_service_data">{{$clinicMember->providing_service_data}}</textarea>
                    </div>
                    <div class="form-group label-floating is-select">
                        <label class="control-label">Treatments Offered: Text</label>
                        <input class="form-control" value="{{$clinicMember->treatments}}" name="treatments" type="text">
                    </div>
                    <div class="form-group label-floating is-select">
                        <label class="control-label">Treatments Data</label>
                        <textarea class="form-control" name="treatments_data">{{$clinicMember->treatments_data}}</textarea>
                    </div>
                    <div class="form-group label-floating is-select">
                        <label class="control-label">Willing To Assist Couple Types*: Text</label>
                        <input class="form-control" value="{{$clinicMember->willing_to_assist}}" name="willing_to_assist" type="text">
                    </div>
                    <div class="form-group label-floating is-select">
                        <label class="control-label">Willing To assist Couple Data </label>
                        <textarea class="form-control" name="willing_to_assist_data">{{$clinicMember->willing_to_assist_data}}</textarea>
                    </div>
                    <div class="form-group label-floating is-select">
                        <label class="control-label">Willing To Assist Couple Types from following countries: Text</label>
                        <input class="form-control" value="{{$clinicMember->willing_to_assist_countries}}" name="willing_to_assist_countries" type="text">
                    </div>
                    <div class="form-group label-floating">
                        <label class="control-label">Please describe your practise/clinic in detail:*</label>
                        <input class="form-control" value="{{$clinicMember->describe_clinic}}" name="describe_clinic" type="text">
                    </div>
                    <div class="form-group label-floating">
                     <label class="control-label">Video URL</label>
                     <input class="form-control" value="{{$clinicMember->video_url}}" name="video_url" type="text">
                  </div>
                  <div class="form-group label-floating">
                     <label class="control-label">Video URL Hint Text</label>
                     <input class="form-control" value="{{$clinicMember->video_url_hint}}" name="video_url_hint" type="text">
                  </div>
                  <div class="form-group label-floating">
                     <label class="control-label">Add Photos Label Text</label>
                     <input class="form-control" value="{{$clinicMember->pictures}}" name="pictures" type="text">
                  </div>
                  <!-- <div class="form-group label-floating">
                     <label class="control-label">Add Photo Hint</label>
                     <input class="form-control" value="{{$clinicMember->first_photo}}" name="first_photo" type="text">
                  </div> -->
                  <div class="form-group label-floating">
                     <label class="control-label">Add Files button Text</label>
                     <input class="form-control" value="{{$clinicMember->add_file}}" name="add_file" type="text">
                  </div>
                  <div class="form-group label-floating">
                     <label class="control-label">Complete Sign up button Text</label>
                     <input class="form-control" value="{{$clinicMember->complete_signup}}" name="complete_signup" type="text">
                  </div>
                    </div>
                <button type="submit" class="btn btn-lg border-2  btn-primary btn-block border-radius-15 padding-15px box-shadow">Update Text</button>
            </form>
                
@endsection