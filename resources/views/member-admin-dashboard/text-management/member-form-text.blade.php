@extends('member-admin-dashboard.layouts.master')
@section('content')
<!-- Page Title -->
                <div id="page-title" class="padding-30px background-white full-width">
                    <div class="container">
                        <ol class="breadcrumb opacity-5">
                            <li><a href="#">Home</a></li>
                            <li><a href="#">Edit Form Text Page</a></li>
                        </ol>
                        <h1 class="font-weight-300">Edit Form Text Page</h1>
                    </div>
                </div>
                <!-- // Page Title -->

                <div class="margin-tb-45px full-width">
                     @if ($errors->any())
                        <div class="alert alert-danger">
                            <ul>
                                @foreach ($errors->all() as $error)
                                    <li>{{ $error }}</li>
                                @endforeach
                            </ul>
                        </div>
                    @endif
                    <div class="padding-30px background-white border-radius-20 box-shadow">
                        <h3><i class="far fa-list-alt margin-right-10px text-main-color"></i>@if($becomeMember->category=='surrogate-mothers')
                            Surrogate Mother
                        @endif
                          @if($becomeMember->category=='sperm-donors')
                            Sperm Donor
                        @endif
                          @if($becomeMember->category=='egg-donors')
                            Egg Donor
                        @endif
                      's Edit Form Fields </h3>
                        <hr>
                        <form method="post" action="{{route('member.form.update',$becomeMember->id)}}">
                            @method('put')
                            @csrf
                    <input type="hidden" name="category" value="{{$becomeMember->category}}">
                    <div class="form-group label-floating">
                        <label class="control-label">Page Title Text</label>
                        <input class="form-control" placeholder="" value="{{$becomeMember->title}}" name="title" type="text">
                    </div>
                    <div class="form-group label-floating">
                        <label class="control-label">Small Note Text</label>
                        <input class="form-control" placeholder="" value="{{$becomeMember->note}}" name="note" type="text">
                    </div>
                    <div class="form-group label-floating">
                        <label class="control-label">Email Text</label>
                        <input class="form-control" placeholder="" value="{{$becomeMember->email}}" name="email" type="text">
                    </div>
                    <div class="form-group label-floating">
                        <label class="control-label">Full Name Text</label>
                        <input class="form-control" placeholder="" value="{{$becomeMember->name}}" name="name" type="text">
                    </div>
                    <div class="form-group label-floating">
                        <label class="control-label">Birthday (mo/day/yr)*</label>
                        <input class="form-control" value="{{$becomeMember->birthday}}" name="birthdate" type="text">
                    </div>
                    <div class="form-group label-floating is-select">
                        <label class="control-label">Country Text</label>
                        <input class="form-control" value="{{$becomeMember->country}}" name="country" type="text">
                    </div>
                    <div class="form-group label-floating">
                        <label class="control-label">Address Text</label>
                        <input class="form-control" value="{{$becomeMember->address}}" name="address" placeholder="Address" type="text">
                    </div>
                    <div class="form-group label-floating">
                        <label class="control-label">Phone/WhatsApp/Viber/Telegram Text</label>
                        <input class="form-control" value="{{$becomeMember->phone}}" name="phone" placeholder="Phone/WhatsApp/Viber/Telegram" type="text">
                    </div>
                    <div class="form-group label-floating is-select">
                        <label class="control-label">Race Text</label>
                        <input class="form-control" value="{{$becomeMember->race}}" name="race" type="text">
                    </div>
                    <div class="form-group label-floating is-select">
                        <label class="control-label">Race Data Text</label>
                        <textarea class="form-control" name="race_data">{{$becomeMember->race_data}}</textarea>
                    </div>
                    <div class="form-group label-floating is-select">
                        <label class="control-label">Religion Text</label>
                        <input class="form-control" value="{{$becomeMember->religion}}" name="religion" type="text">
                    </div>
                    <div class="form-group label-floating is-select">
                        <label class="control-label">Religion Data Text</label>
                        <textarea class="form-control" name="religion_data">{{$becomeMember->religion_data}}</textarea>
                    </div>
                    <div class="form-group label-floating is-select">
                        <label class="control-label">Education Text</label>
                        <input class="form-control" value="{{$becomeMember->education}}" name="education" type="text">
                    </div>
                    <div class="form-group label-floating is-select">
                        <label class="control-label">Education Data Text</label>
                        <textarea class="form-control" name="education_data">{{$becomeMember->education_data}}</textarea>
                    </div>
                    <div class="form-group label-floating is-select">
                        <label class="control-label">Health Status Text</label>
                        <input class="form-control" value="{{$becomeMember->health_status}}" name="health_status" type="text">
                    </div>
                    <div class="form-group label-floating is-select">
                        <label class="control-label">Health Status Data Text</label>
                        <textarea class="form-control" name="health_status_data">{{$becomeMember->health_status_data}}</textarea>
                    </div>
                    <div class="form-group label-floating is-select">
                        <label class="control-label">Smoker Text</label>
                       <input class="form-control" value="{{$becomeMember->smoker}}" name="smoker" type="text">
                    </div>
                    <div class="form-group label-floating is-select">
                        <label class="control-label">Smoker Data Text</label>
                        <textarea class="form-control" name="smoker_data">{{$becomeMember->smoker_data}}</textarea>
                    </div>
                    <div class="form-group label-floating is-select">
                        <label class="control-label">Eye Color Text</label>
                        <input class="form-control" value="{{$becomeMember->eye_color}}" name="eye_color" type="text">
                    </div>
                    <div class="form-group label-floating is-select">
                        <label class="control-label">Eye Coloar Data Text</label>
                        <textarea class="form-control" name="eye_color_data">{{$becomeMember->eye_color_data}}</textarea>
                    </div>
                    <div class="form-group label-floating is-select">
                        <label class="control-label">Hair Color Text</label>
                        <input class="form-control" value="{{$becomeMember->hair_color}}" name="hair_color" type="text">
                    </div>
                    <div class="form-group label-floating is-select">
                        <label class="control-label">Hair Color Data Text</label>
                        <textarea class="form-control" name="hair_color_data">{{$becomeMember->hair_color_data}}</textarea>
                    </div>
                    <div class="form-group label-floating is-select">
                        <label class="control-label">Height Text</label>
                        <input class="form-control" value="{{$becomeMember->height}}" name="height" type="text">
                    </div>
                    <div class="form-group label-floating is-select">
                        <label class="control-label">Height Data Text</label>
                        <textarea class="form-control" name="height_data">{{$becomeMember->height_data}}</textarea>
                    </div>
                    <div class="form-group label-floating is-select">
                        <label class="control-label">Weight Text</label>
                        <input class="form-control" value="{{$becomeMember->weight}}" name="weight" type="text">
                    </div>
                    <div class="form-group label-floating is-select">
                        <label class="control-label">Weight Data Text</label>
                        <textarea class="form-control" name="weight_data">{{$becomeMember->weight_data}}</textarea>
                    </div>
                    <div class="form-group label-floating is-select">
                        <label class="control-label">Blood Type Text</label>
                        <input class="form-control" value="{{$becomeMember->blood_type}}" name="blood_type" type="text">
                    </div>
                    <div class="form-group label-floating is-select">
                        <label class="control-label">Blood Type Data Text</label>
                        <textarea class="form-control" name="blood_type_data">{{$becomeMember->blood_type_data}}</textarea>
                    </div>
                    <div class="form-group label-floating is-select">
                        <label class="control-label">Have Passport Text</label>
                        <input class="form-control" value="{{$becomeMember->have_passport}}" name="have_passport" type="text">
                    </div>
                    <div class="form-group label-floating is-select">
                        <label class="control-label">Have Passport Data Text</label>
                        <textarea class="form-control" name="have_passport_data">{{$becomeMember->have_passport_data}}</textarea>
                    </div>
                    <div class="form-group label-floating is-select">
                        <label class="control-label">Willing To Travel Text</label>
                        <input class="form-control" value="{{$becomeMember->willing_to_travel}}" name="willing_to_travel" type="text">
                    </div>
                    <div class="form-group label-floating is-select">
                        <label class="control-label">Willing to travel Data Text</label>
                        <textarea class="form-control" name="willing_to_travel_data">{{$becomeMember->willing_to_travel_data}}</textarea>
                    </div>
                    <div class="form-group label-floating is-select">
                        <label class="control-label">Number of Children(non-donor) Text</label>
                        <input class="form-control" value="{{$becomeMember->number_of_children}}" name="number_of_children" type="text">
                    </div>
                    <div class="form-group label-floating is-select">
                        <label class="control-label">Number of Children Data Text</label>
                        <textarea class="form-control" name="number_of_children_data">{{$becomeMember->number_of_children_data}}</textarea>
                    </div>
                    <div class="form-group label-floating is-select">
                        <label class="control-label">Number of Surrogate Children Text</label>
                        <input class="form-control" value="{{$becomeMember->number_of_surrogate_children}}" name="number_of_surrogate_children" type="text">
                    </div>
                     <div class="form-group label-floating is-select">
                        <label class="control-label">Number of Surrogate Children Data Text</label>
                        <textarea class="form-control" name="number_of_surrogate_children_data">{{$becomeMember->number_of_surrogate_children_data}}</textarea>
                    </div>
                    <div class="form-group label-floating is-select">
                        <label class="control-label">Surrogate Mother Types That You Agree To: Text</label>
                        <input class="form-control" value="{{$becomeMember->surrogate_type}}" name="surrogate_type" type="text">
                  </div>
                  <div class="form-group label-floating is-select">
                       <label class="control-label">Surrogate Mother Types That You Agree To Data</label>
                        <textarea class="form-control" name="surrogate_type_data">{{$becomeMember->surrogate_type_data}}</textarea>
                    </div>
                    <div class="form-group label-floating is-select">
                        <label class="control-label">Have you been a @if($becomeMember->category=='surrogate-mothers')
                            surrogate mother
                        @endif
                          @if($becomeMember->category=='sperm-donors')
                            sperm donor
                        @endif
                          @if($becomeMember->category=='egg-donors')
                            egg donor
                        @endif
                         before? Text</label>
                        <input class="form-control" value="{{$becomeMember->given_service_before}}" name="given_service_before" type="text">
                    </div>
                    <div class="form-group label-floating is-select">
                        <label class="control-label">Have you been a @if($becomeMember->category=='surrogate-mothers')
                            surrogate mother
                        @endif
                          @if($becomeMember->category=='sperm-donors')
                            sperm donor
                        @endif
                          @if($becomeMember->category=='egg-donors')
                            egg donor
                        @endif
                         before? Data</label>
                        <textarea class="form-control" name="given_service_before_data">{{$becomeMember->given_service_before_data}}</textarea>
                    </div>
                    <div class="form-group label-floating is-select">
                        <label class="control-label">Marital Status</label>
                        <input class="form-control" value="{{$becomeMember->marital_status}}" name="marital_status" type="text">
                    </div>
                     <div class="form-group label-floating is-select">
                        <label class="control-label">Marital Status Data Text</label>
                        <textarea class="form-control" name="marital_status_data">{{$becomeMember->marital_status_data}}</textarea>
                    </div>
                    <div class="form-group label-floating is-select">
                        <label class="control-label">Spoken Languages</label>
                        <input class="form-control" value="{{$becomeMember->spoken_language}}" name="spoken_language" type="text">
                  </div>
                   <div class="form-group label-floating is-select">
                        <label class="control-label">Spoken Language Data Text</label>
                        <textarea class="form-control" name="spoken_language_data">{{$becomeMember->spoken_language_data}}</textarea>
                    </div>
                    <div class="form-group label-floating is-select">
                        <label class="control-label">Willing To Assist Couple Types:*</label>
                        <input class="form-control" value="{{$becomeMember->willing_to_assist}}" name="willing_to_assist" type="text">
                    </div>
                     <div class="form-group label-floating is-select">
                        <label class="control-label">Willing To Assist Data Text</label>
                        <textarea class="form-control" name="willing_to_assist_data">{{$becomeMember->willing_to_assist_data}}</textarea>
                    </div>
                    <div class="form-group label-floating">
                        <label class="control-label">How would you best describe yourself?</label>
                        <input class="form-control" value="{{$becomeMember->describe_yourself}}" name="describe_yourself" type="text">
                    </div>
                    <div class="form-group label-floating">
                        <label class="control-label">Why do you want to be a 
                        @if($becomeMember->category=='surrogate-mothers')
                            surrogate mother
                        @endif
                          @if($becomeMember->category=='sperm-donors')
                            sperm donor
                        @endif
                          @if($becomeMember->category=='egg-donors')
                            egg donor
                        @endif
                        ?*</label>
                        <input class="form-control" value="{{$becomeMember->why_become_member}}" name="why_become_member" type="text">
                    </div>
                    <div class="form-group label-floating">
                        <label class="control-label">Use this space to communicate a letter to potential intended parents.</label>
                        <input class="form-control" value="{{$becomeMember->communication_letter}}" name="communication_letter" type="text">
                    </div>
                    <div class="form-group label-floating">
                     <label class="control-label">Video URL</label>
                     <input class="form-control" value="{{$becomeMember->video_url}}" name="video_url" type="text">
                  </div>
                  <div class="form-group label-floating">
                     <label class="control-label">Video URL Hint Text</label>
                     <input class="form-control" value="{{$becomeMember->video_url_hint}}" name="video_url_hint" type="text">
                  </div>
                  <div class="form-group label-floating">
                     <label class="control-label">Add Photos Label Text</label>
                     <input class="form-control" value="{{$becomeMember->add_photo}}" name="add_photo" type="text">
                  </div>
                  <div class="form-group label-floating">
                     <label class="control-label">Add Photo Hint</label>
                     <input class="form-control" value="{{$becomeMember->first_photo}}" name="first_photo" type="text">
                  </div>
                  <div class="form-group label-floating">
                     <label class="control-label">Add Files button Text</label>
                     <input class="form-control" value="{{$becomeMember->add_file}}" name="add_file" type="text">
                  </div>
                  <div class="form-group label-floating">
                     <label class="control-label">Complete Sign up button Text</label>
                     <input class="form-control" value="{{$becomeMember->complete_signup}}" name="complete_signup" type="text">
                  </div>
                    </div>
                <button type="submit" class="btn btn-lg border-2  btn-primary btn-block border-radius-15 padding-15px box-shadow">Update Text</button>
            </form>
                
@endsection
@section('additional-js')
<script src="https://cdn.jsdelivr.net/npm/summernote@0.8.16/dist/summernote.min.js"></script>
 <script>
    $(document).ready(function() {
        $('#summernote').summernote();
    });
  </script>
<script type="text/javascript" src="{{asset('assets/assets/bootstrap-datepicker.js')}}"></script>
<script type="text/javascript" src="{{asset('assets/assets/bootstrap-datepicker.init.js')}}"></script>
<script type="text/javascript" src="{{asset('assets/assets/signup.js')}}"></script>
<script type="text/javascript" src="{{asset('assets/assets/bootstrap-multiselect.js')}}"></script>
<link rel="stylesheet" type="text/css" href="{{asset('assets/assets/datepicker3.css')}}">
<link rel="stylesheet" type="text/css" href="{{asset('assets/assets/bootstrap-multiselect.css')}}">
<link rel="stylesheet" type="text/css" href="{{asset('assets/assets/member_generic.css')}}">
<script src='https://www.google.com/recaptcha/api.js'></script>
@endsection