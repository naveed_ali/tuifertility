@extends('member-admin-dashboard.layouts.master')
@section('content')
<a href="{{route('member.form.edit','surrogate-mothers')}}" class="btn btn-lg btn-primary margin-tb-5px">Surrogate Mothers Form Text</a>
<a href="{{route('member.form.edit','egg-donors')}}" class="btn btn-lg btn-primary margin-tb-5px">Egg Donor Form Text</a>
<a href="{{route('member.form.edit','sperm-donors')}}" class="btn btn-lg btn-primary margin-tb-5px">Sperm Donor Form Text</a>
<a href="{{route('member.form.edit','fertility-clinics')}}" class="btn btn-lg btn-primary margin-tb-5px">Fertility Clinic Form Text</a>
<a href="{{route('apply.now.buttons')}}" class="btn btn-lg btn-primary margin-tb-5px">Apply now buttons Text</a>
@endsection
